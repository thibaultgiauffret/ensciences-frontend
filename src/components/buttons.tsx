// ----------------------------
// Button components
// ----------------------------

import React from "react";
import styles from "./css/buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Dropdown from 'react-bootstrap/Dropdown';

library.add(fas, fab)

const determineIcon = (icon: string): IconProp => {
  const brands = ['twitter', 'youtube', 'github', 'gitlab', 'mastodon'];
  const prefix = brands.includes(icon) ? 'fab' : 'fas';
  return [prefix, icon] as IconProp;
};

function BigCircleLink(props: { color: string, textColor?: string, icon: string, href: string, target?: string, className?: string, tooltip?: string, style?: React.CSSProperties }) {
  let textColor = "white";
  if (props.textColor) {
    textColor = props.textColor;
  }

  if (props.tooltip) {
    return (
      <OverlayTrigger overlay={<Tooltip>{props.tooltip}</Tooltip>}>
        <a href={props.href} className={styles.bigCircleButton + " fa-stack fa-4x" + " " + props.className} target={props.target} style={props.style}>
          <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: props.color }} />
          <FontAwesomeIcon icon={determineIcon(props.icon)}
            className="fa-stack-1x fa-inverse" size="1x" style={
              {
                color: textColor
              }
            } />;
        </a>
      </OverlayTrigger>
    );
  } else {
    return (
      <a href={props.href} className={styles.bigCircleButton + " fa-stack fa-4x" + " " + props.className} target={props.target} style={props.style}>
        <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: props.color }} />
        <FontAwesomeIcon icon={determineIcon(props.icon)}
          className="fa-stack-1x fa-inverse" size="1x" style={
            {
              color: textColor
            }
          } />;
      </a>
    );
  }
}

export { BigCircleLink };

function BigCircleButton(props: { color: string, textColor?: string, icon: string, onClick: () => void, className?: string, tooltip?: string, style?: React.CSSProperties }) {
  let textColor = "white";
  if (props.textColor) {
    textColor = props.textColor;
  }

  if (props.tooltip) {
    return (
      <OverlayTrigger overlay={<Tooltip>{props.tooltip}</Tooltip>}>
        <a onClick={props.onClick} className={styles.bigCircleButton + " fa-stack fa-4x" + " " + props.className} style={props.style}>
          <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: props.color }} />
          <FontAwesomeIcon icon={determineIcon(props.icon)}
            className="fa-stack-1x fa-inverse" size="1x" style={
              {
                color: textColor
              }
            } />;
        </a>
      </OverlayTrigger>
    );
  } else {
    return (
      <a onClick={props.onClick} className={styles.bigCircleButton + " fa-stack fa-4x" + " " + props.className} style={props.style}>
        <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: props.color }} />
        <FontAwesomeIcon icon={determineIcon(props.icon)}
          className="fa-stack-1x fa-inverse" size="1x" style={
            {
              color: textColor
            }
          } />;
      </a>
    );
  }
}

export { BigCircleButton };

function ButtonLink({ children, href, target, className, style, onClick, ...rest }: { children: React.ReactNode, href?: string, target?: string, className?: string, style?: React.CSSProperties, onClick?: Function }) {
  return (
    <a target={target} href={href}
      className={className} style={
        {
          textDecoration: "none",
          color: href ? undefined : 'var(--bs-secondary)',
          ...style
        }
      }
      onClick={(e) => {
        if (onClick) {
          e.preventDefault();
          onClick();
        }
      }}
      {...rest}
    >
      {children}
    </a>
  );
}

export { ButtonLink };

function BigCircleDropdownButton({ children, color, textColor, icon, className, tooltip }: { children: React.ReactNode, color: string, textColor?: string, icon: string, className?: string, tooltip?: string }) {
  return (
    <Dropdown>
      {tooltip ? (
        <OverlayTrigger overlay={<Tooltip>{tooltip}</Tooltip>}>
          <Dropdown.Toggle as="a" className={styles.bigCircleButton + " fa-stack fa-4x" + " " + className}>
            <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: color }} />
            <FontAwesomeIcon icon={determineIcon(icon)}
              className="fa-stack-1x fa-inverse" size="1x" style={
                {
                  color: textColor
                }
              } />;
          </Dropdown.Toggle>
        </OverlayTrigger>
      ) : (
        <Dropdown.Toggle as="a" className={styles.bigCircleButton + " fa-stack fa-4x" + " " + className}>
          <FontAwesomeIcon icon="circle" className="fa-stack-2x" style={{ color: color }} />
          <FontAwesomeIcon icon={determineIcon(icon)}
            className="fa-stack-1x fa-inverse" size="1x" style={
              {
                color: textColor
              }
            } />;
        </Dropdown.Toggle>
      )}
      <Dropdown.Menu>
        {children}
      </Dropdown.Menu>
    </Dropdown>
  );
}

export { BigCircleDropdownButton };

// Scroll to top button
function ScrollToTopButton() {
  const [display, setDisplay] = React.useState(false);

  React.useEffect(() => {
    const toggleVisibility = () => {
      if (window.pageYOffset > 300) {
        setDisplay(true);
      } else {
        setDisplay(false);
      }
    };

    window.addEventListener('scroll', toggleVisibility);

    return () => window.removeEventListener('scroll', toggleVisibility);
  }, []);

  return (
      <a href="#" className={styles.scrollToTop + (display ? ` ${styles.visible}` : ` ${styles.hidden}`)} onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}>
        <FontAwesomeIcon icon="angle-up" />
      </a>
    );
}

export { ScrollToTopButton };
