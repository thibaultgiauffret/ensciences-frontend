// Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import { Modal, Pagination, Table } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { useToasts } from 'react-bootstrap-toasts';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { Badge } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState } from 'react';
import { deleteFile } from '../../functions/files/deleteFile';
import { listFilesManager } from '../../functions/files/listFilesManager';
import { uploadFile } from '../../functions/files/uploadFile';
import { updateFile } from '../../functions/files/updateFile';
import { addFolder } from '../../functions/files/addFolder';
import { deleteFolder } from '../../functions/files/deleteFolder';
import path from 'path-browserify';

// Files manager table
function FilesManager({ folder, setModified }: { folder: string, setModified: (modified: boolean) => void }) {

    // Toasts
    const toasts = useToasts();

    // Files and other states
    const [files, setFiles] = useState<FilesManagerFiles[]>([]);
    const [currentFolder, setCurrentFolder] = useState(folder);
    const [previousFolders, setPreviousFolders] = useState<string[]>([]);
    const [page, setPage] = useState(1);
    const [numberPerPage, setNumberPerPage] = useState(10);
    const [numberOfPages, setNumberOfPages] = useState(0);
    const [search, setSearch] = useState('');
    const [sort, setSort] = useState('name');
    const [desc, setDesc] = useState(false);

    useEffect(() => {
        // Get the files on component mount
        refreshFiles();
    }, []);

    const refreshFiles = () => {
        console.log('Get files for folder ' + currentFolder);
        listFilesManager(currentFolder).then((filesData: any) => {
            if (filesData) {
                if (filesData.status === 'success') {
                    setFiles(filesData.data);
                } else {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Une erreur est survenue lors du chargement de la liste des fichiers : ' + filesData.message,
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                }
            }
        });
    }

    useEffect(() => {
        // Calculate the number of pages
        const newNumberOfPages = getNumberOfPages(files.filter((file) => file.name.toLowerCase().includes(search.toLowerCase())), numberPerPage);
        setNumberOfPages(newNumberOfPages);

        // If the page is higher than the number of pages, set it to the last page
        if (page > newNumberOfPages) {
            setPage(newNumberOfPages > 0 ? newNumberOfPages : 1);
        }

        // Sort the files according to the sort and desc states
        files.sort((a, b) => {
            const multi = desc === false ? 1 : -1;
            switch (sort) {
                case 'name':
                    return a.name.localeCompare(b.name) * multi;
                case 'folder':
                    return a.path.localeCompare(b.path) * multi;
                case 'size':
                    return (a.size - b.size) * multi;
                case 'date':
                    return (a.date - b.date) * multi;
                default:
                    return 0;
            }
        });
    }, [files, numberPerPage, page, search, sort, desc]);

    // Handle the sort of the table
    const handleSort = (sortName: string) => {
        if (sort === sortName) {
            setDesc(!desc);
        } else {
            setDesc(false);
        }
        setSort(sortName);
    }

    useEffect(() => {
        refreshFiles();
    }, [currentFolder]);

    // Change the folder
    const goToPreviousFolder = () => {
        if (previousFolders.length > 0) {
            const lastFolder = previousFolders[previousFolders.length - 1];
            setPreviousFolders(previousFolders.slice(0, -1));
            setCurrentFolder(lastFolder);
        }
    };

    const changeFolder = (newFolder: string) => {
        console.log('Change folder to ' + newFolder);
        setPreviousFolders([...previousFolders, currentFolder]);
        setCurrentFolder(path.join(currentFolder, newFolder));
    };

    // Handle the upload modal
    const [showUpload, setShowUpload] = useState(false);
    const handleCloseEditUpload = () => {
        setShowUpload(false);
        refreshFiles();
    }
    const handleShowEditUpload = () => setShowUpload(true);

    // Handle the add folder modal
    const [showAddFolder, setShowAddFolder] = useState(false);
    const handleCloseAddFolder = () => {
        setShowAddFolder(false);
        refreshFiles();
    }
    const handleShowAddFolder = () => setShowAddFolder(true);

    return (
        <>
            <h4>
                {/* Previous file button */}
                {previousFolders.length > 0 &&
                    <Button variant="primary" size="sm" className="me-3" onClick={() => {
                        goToPreviousFolder();
                    }}>
                        <FontAwesomeIcon icon="arrow-left" />
                    </Button>
                }
                <FontAwesomeIcon icon="folder" className="me-2" />
                {currentFolder}
            </h4>
            <Row>

                <Col>


                    {/* Search bar */}
                    <InputGroup className="mb-3" size="sm">
                        <InputGroup.Text><FontAwesomeIcon icon="search" /></InputGroup.Text>
                        <Form.Control type="text" placeholder="Rechercher un fichier..." value={search} onChange={(e) => {
                            setSearch(e.target.value);
                            setPage(1);
                        }} />
                        {/* Clear button when search is not empty */}
                        {search !== '' && <Button variant="secondary" onClick={() => {
                            setSearch('');
                            setPage(1);
                        }}>
                            <FontAwesomeIcon icon="times" />
                        </Button>}
                    </InputGroup>
                </Col>
                <Col>
                    {/* Select numberPerPage */}
                    <InputGroup className="mb-3" size="sm">
                        <InputGroup.Text><FontAwesomeIcon icon="list" /></InputGroup.Text>
                        <Form.Select onChange={(e) => {
                            setNumberPerPage(parseInt(e.target.value));
                            setPage(1);
                        }}>
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </Form.Select>
                    </InputGroup>
                </Col>
                <Col>
                    {/* Upload files button */}
                    <Button variant="success" size="sm" className="m-1" onClick={() => {
                        handleShowEditUpload();
                    }}>
                        <FontAwesomeIcon icon="file-upload" />
                    </Button>

                    <FileUploader currentFolder={currentFolder} folder={currentFolder} showUpload={showUpload} handleCloseEditUpload={handleCloseEditUpload} setModified={setModified} />

                    {/* Add folder */}
                    <Button variant="success" size="sm" className="m-1" onClick={() => {
                        handleShowAddFolder();
                    }}>
                        <FontAwesomeIcon icon="folder-plus" />
                    </Button>

                    <FolderAdd currentFolder={currentFolder} showAddFolder={showAddFolder} handleCloseAddFolder={handleCloseAddFolder} setModified={setModified} />
                </Col>
            </Row>
            {/* Table */}
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th>Nom

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par nom
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('name');

                                }}>
                                    {sort === "name" && desc === true ? <FontAwesomeIcon icon="sort-alpha-down" /> : <FontAwesomeIcon icon="sort-alpha-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>
                            Autorisations
                        </th>
                        <th>Taille

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par taille
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('size');
                                }}>
                                    {sort === "size" && desc === true ? <FontAwesomeIcon icon="sort-numeric-down" /> : <FontAwesomeIcon icon="sort-numeric-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Date

                            {/* Sort buttons */}
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id="sortTooltip">
                                        Trier par date
                                    </Tooltip>
                                }
                            >
                                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                                    handleSort('date');
                                }}>
                                    {sort === "date" && desc === true ? <FontAwesomeIcon icon="sort-numeric-down" /> : <FontAwesomeIcon icon="sort-numeric-up" />}
                                </Button>
                            </OverlayTrigger>
                        </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Display the files according to the search and pagination */}
                    {files
                        .sort((a, b) => {
                            if (a.type === 'folder' && b.type !== 'folder') return -1;
                            if (a.type !== 'folder' && b.type === 'folder') return 1;
                            return 0;
                        })
                        .filter((file) =>
                            file.name.toLowerCase().includes(search.toLowerCase())
                        )
                        .slice((page - 1) * numberPerPage, page * numberPerPage)
                        .map((file) => (
                            file.type === 'folder' ?
                                // Folder row
                                <tr key={file.name}>
                                    <td colSpan={4}>
                                        <Button variant="link" onClick={() => {
                                            changeFolder(file.name);
                                        }} className="d-inline-block text-start">
                                            <FontAwesomeIcon icon="folder" className="me-2" />{file.name}
                                        </Button>

                                    </td>                                <td>
                                        <Button variant="danger" size="sm" className="m-1" onClick={() => {
                                            if (window.confirm('Voulez-vous vraiment supprimer le dossier ' + file.name + ' ?')) {
                                                deleteFolder(folder, file.name).then((data: any) => {
                                                    if (data.status === 'success') {
                                                        setModified(true);
                                                        toasts.show({
                                                            headerContent: 'Succès',
                                                            bodyContent: "Dossier supprimé avec succès.",
                                                            toastProps: {
                                                                bg: 'success',
                                                                autohide: true,
                                                                delay: 5000,
                                                            },
                                                        });
                                                    } else {
                                                        toasts.show({
                                                            headerContent: 'Erreur',
                                                            bodyContent: data.message,
                                                            toastProps: {
                                                                bg: 'danger',
                                                                autohide: true,
                                                                delay: 5000,
                                                            },
                                                        });
                                                    }
                                                    // Refresh the files list
                                                    refreshFiles();
                                                });
                                            }
                                        }}>
                                            <FontAwesomeIcon icon="trash" />
                                        </Button>
                                    </td>
                                </tr>
                                :
                                // File row
                                <tr key={file.name}>
                                    <td>
                                        <Button variant="link" href={file.path.replace('files/', 'view?direct=')} target="_blank" className="d-inline-block text-start">
                                            <FontAwesomeIcon icon={chooseIcon(file.ext)} className="me-2" />{file.name}
                                        </Button>
                                    </td>
                                    <td>
                                        {file.password !== '' && file.password !== null &&
                                            <Badge bg="warning" className="me-1">
                                                <FontAwesomeIcon icon="lock" />
                                            </Badge>
                                        }
                                        {file.permissions.map((permission: any) => (
                                            <Badge key={permission} bg="success" className="me-1">{permission}</Badge>
                                        ))}
                                    </td>
                                    <td>
                                        {/* Convert size to Mo */}
                                        {(file.size / 1000000).toFixed(2)} Mo
                                    </td>
                                    <td>
                                        {/* Convert date to string */}
                                        {new Date(file.date * 1000).toLocaleDateString('fr-FR', { day: '2-digit', month: '2-digit', year: 'numeric' })}
                                    </td>
                                    <td>

                                        {/* Edit button */}
                                        <FileEditor currentFolder={currentFolder} file={file} refreshFiles={refreshFiles} />
                                        {/* See button */}
                                        {/* <Button variant="secondary" size="sm" className="m-1" href={file.path.replace('files/', 'view?direct=')} target="_blank">
                                            <FontAwesomeIcon icon="eye" />
                                        </Button> */}
                                        {/* Copy link button */}
                                        <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                            navigator.clipboard.writeText(file.path.replace('files/', 'https://www.ensciences.fr/view?direct='));
                                            // Display a success toast
                                            toasts.show({
                                                headerContent: 'Succès',
                                                bodyContent: 'Lien copié dans le presse-papiers.',
                                                toastProps: {
                                                    bg: 'success',
                                                    autohide: true,
                                                    delay: 5000,
                                                },
                                            });
                                        }}>
                                            <FontAwesomeIcon icon="link" />
                                        </Button>

                                        {/* Delete button */}
                                        <Button variant="danger" size="sm" className="m-1" onClick={() => {
                                            deleteFile(
                                                currentFolder,
                                                file.name
                                            ).then((data: any) => {
                                                if (data.status === 'success') {
                                                    setModified(true);
                                                    toasts.show({
                                                        headerContent: 'Succès',
                                                        bodyContent: "Fichier supprimé avec succès.",
                                                        toastProps: {
                                                            bg: 'success',
                                                            autohide: true,
                                                            delay: 5000,
                                                        },
                                                    });
                                                } else {
                                                    toasts.show({
                                                        headerContent: 'Erreur',
                                                        bodyContent: data.message,
                                                        toastProps: {
                                                            bg: 'danger',
                                                            autohide: true,
                                                            delay: 5000,
                                                        },
                                                    });
                                                }
                                                // Refresh the files list
                                                refreshFiles();
                                            });
                                        }}>
                                            <FontAwesomeIcon icon="trash" />
                                        </Button>
                                    </td>
                                </tr>
                        ))}
                </tbody>
            </Table>
            <Paginations numberOfPages={numberOfPages} activePage={page} changePage={setPage} />
        </>
    );
}

function FileUploader({ currentFolder, folder, showUpload, handleCloseEditUpload, setModified }: { currentFolder: string, folder: string, showUpload: boolean, handleCloseEditUpload: () => void, setModified: (modified: boolean) => void }) {

    const toasts = useToasts();

    // Handle the upload of a file
    const [fileList, setFileList] = useState<File[]>([]);
    const [customFileList, setCustomFileList] = useState<any>([]);


    const handleFileList = (e: React.ChangeEvent<HTMLInputElement>) => {
        // Add the new files to the list (keep the previous files)
        const newFiles = Array.from(e.target.files as FileList);
        setFileList([...fileList, ...newFiles]);
        // Show the upload file list
        const uploadFileList = document.getElementById("uploadFileList");
        uploadFileList?.classList.remove("d-none");

        // Add the custom files to the list containing name, folder, permissions
        setCustomFileList([...customFileList, ...newFiles.map((file) => {
            return {
                name: file.name,
                folder: folder,
                permissions: file.name.match(/-(\w+)\./) ? [file.name.match(/-(\w+)\./)?.[1]] : ['base']
            }
        })]);
    }

    const handleUpload = () => {
        console.log(customFileList);
        console.log(fileList);
        // Try to upload each file one by one (go to the next file when the previous one is uploaded or failed)
        let i = 0;
        let sucessfulUploads = 0;
        let errorList = [] as any;
        const uploadNext = () => {

            if (i < fileList.length) {
                // Change the status of the file to uploading
                const status = document.getElementById("status-" + fileList[i].name);
                status?.classList.remove("text-secondary");
                status?.classList.add("text-primary");

                // Get the auth option
                let permissions = customFileList[i].permissions;


                // Upload the file
                uploadFile(
                    currentFolder,
                    permissions,
                    fileList[i]).then((data: any) => {
                        if (data.status === 'success') {
                            setModified(true);
                            status?.classList.add("text-success");
                            sucessfulUploads++;
                        } else {
                            status?.classList.add("text-danger");
                            toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: 'Une erreur est survenue lors de l\'envoi du fichier : ' + data.status,
                                toastProps: {
                                    bg: 'danger',
                                    autohide: true,
                                    delay: 5000,
                                },
                            });
                            // Store the error
                            errorList.push(fileList[i].name);
                        }
                        i++;
                        uploadNext();
                    });
            } else {
                if (sucessfulUploads === 0) {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Aucun fichier envoyé.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                } else if (sucessfulUploads < fileList.length) {
                    toasts.show({
                        headerContent: 'Attention',
                        bodyContent: sucessfulUploads + '/' + (
                            fileList.length
                        ) + ' fichiers envoyés avec succès.',
                        toastProps: {
                            bg: 'warning',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                    // Keep only the files that failed
                    setTimeout(() => {
                        setFileList(fileList.filter((file) => errorList.includes(file.name)));
                    }, 3000);
                } else {
                    console.log('Tous les fichiers ont été envoyés avec succès.');
                    toasts.show({
                        headerContent: 'Succès',
                        bodyContent: 'Tous les fichiers ont été envoyés avec succès.',
                        toastProps: {
                            bg: 'success',
                            autohide: true,
                            delay: 5000,
                        },
                    });

                    // Hide upload file list
                    setFileList([]);
                    setCustomFileList([]);
                    const uploadFileList = document.getElementById("uploadFileList");
                    uploadFileList?.classList.add("d-none");
                    // Close the modal
                    handleCloseEditUpload();
                }
            }
        };

        uploadNext();
    }

    return (
        <>
            {/* Upload files modal */}
            <Modal show={showUpload} size="xl" onHide={handleCloseEditUpload} style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <FontAwesomeIcon icon="upload" />&nbsp;Ajouter des fichiers
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        handleUpload();
                    }}>
                        {/* Upload form */}
                        <InputGroup className="mb-3" size="sm">
                            {/* File */}
                            <Form.Control type="file" name="file" multiple onChange={handleFileList} />
                        </InputGroup>

                        {/* Upload file list */}
                        <div className="mb-3 d-none" id="uploadFileList">
                            <Table striped bordered hover responsive>
                                <thead>
                                    <tr>
                                        <th>Statut</th>
                                        <th>Nom</th>
                                        <th>Autorisations</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {fileList.map((file: any, index: number) => (
                                        <tr key={index}>
                                            <td>
                                                <span id={"status-" + file.name} className="me-3 text-secondary">
                                                    <FontAwesomeIcon icon="circle-dot" />
                                                </span>
                                            </td>
                                            <td>
                                                <span>
                                                    {file.name}
                                                </span>
                                            </td>
                                            <td>
                                                <span>
                                                    {/* Select */}
                                                    {/* Analyse the file name. If there is something like "aaa-xxx.ext", extract xxx and select the corresponding value */}
                                                    <Form.Select aria-label="Sélectionner les autorisations" multiple
                                                        size="sm"
                                                        defaultValue={
                                                            file.name.match(/-(\w+)\./) ? [file.name.match(/-(\w+)\./)[1]] : ['base']
                                                        }
                                                        onChange={(e) => {
                                                            // Update the permissions value in the custom file list
                                                            setCustomFileList(customFileList.map((f: any) => {
                                                                if (f.name === file.name) {
                                                                    f.permissions = Array.from(e.target.selectedOptions).map((option) => option.value);
                                                                }
                                                                return f;
                                                            }));
                                                        }}
                                                    >
                                                        <option value="base">Base</option>
                                                        <option value="prof">Professeur</option>
                                                        <option value="eleve">Élève</option>
                                                        <option value="capes">Capes</option>
                                                        <option value="agreg">Agreg</option>
                                                    </Form.Select>
                                                </span>
                                            </td>
                                            <td>
                                                <span>
                                                    {/* Delete button */}
                                                    <Button variant="danger" size="sm" className="m-1" onClick={() => {
                                                        setFileList(fileList.filter((f) => f.name !== file.name));
                                                    }}>
                                                        <FontAwesomeIcon icon="trash" />
                                                    </Button>
                                                </span>
                                            </td>
                                        </tr>
                                    ))}

                                </tbody>
                            </Table>

                            {/* Submit button */}
                            <Button variant="primary" type="submit" size="sm">
                                <FontAwesomeIcon icon="upload" />&nbsp;Envoyer le(s) fichier(s)
                            </Button>
                        </div>



                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );
}

function FileEditor({ currentFolder, file, refreshFiles }: { currentFolder: string, file: FilesManagerFiles, refreshFiles: () => void }) {

    const [show, setShowEdit] = useState(false);
    const toasts = useToasts();

    const [name, setName] = useState(file.name);
    const [permissions, setPermissions] = useState(file.permissions);
    const [password, setPassword] = useState('');
    const [fileInput, setFileInput] = useState<File | undefined>(undefined);

    const handleFileInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            setFileInput(e.target.files[0]);
        }
    }

    return (
        <>
            <Button variant="primary" size="sm" className="m-1" onClick={() => {
                setShowEdit(true);
            }}>
                <FontAwesomeIcon icon="edit" />
            </Button>

            <Modal show={show} size={"xl"} onHide={() => setShowEdit(false)} style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <FontAwesomeIcon icon="edit" className="me-2" />
                        Éditer le fichier
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    updateFile(
                        file.id,
                        currentFolder,
                        file.path,
                        name,
                        permissions,
                        file.size.toString(),
                        fileInput,
                        file.inDB,
                        password
                    ).then((data: any) => {
                        if (data.status === 'success') {
                            setShowEdit(false);
                            toasts.show({
                                headerContent: 'Succès',
                                bodyContent: data.message,
                                toastProps: {
                                    bg: 'success',
                                    autohide: true,
                                    delay: 5000,
                                },
                            });
                            // Refresh the files list
                            refreshFiles();
                        } else {
                            toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: data.message,
                                toastProps: {
                                    bg: 'danger',
                                    autohide: true,
                                    delay: 5000,
                                },
                            });
                        }
                    })
                }}>
                    <Modal.Body>

                        <Form.Group className="mb-3">
                            <Form.Label>Nom</Form.Label>
                            <Form.Control type="text"
                                size="sm"
                                name="name"
                                defaultValue={file.name}
                                onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Autorisations</Form.Label>
                            <Form.Select
                                size="sm"
                                name="permissions"
                                multiple defaultValue={file.permissions}
                                onChange={(e) => {
                                    setPermissions(Array.from(e.target.selectedOptions).map((option) => option.value));
                                }}>
                                <option value="base">Base</option>
                                <option value="prof">Professeur</option>
                                <option value="eleve">Élève</option>
                                <option value="capes">Capes</option>
                                <option value="agreg">Agreg</option>
                            </Form.Select>
                        </Form.Group>
                        {/* Password */}
                        <>
                            <Form.Group className="mb-3">
                                <Form.Label>Mot de passe</Form.Label>
                                <InputGroup size="sm">
                                    <Form.Control type="password" name="password"
                                        defaultValue={file.password}
                                        onChange={(e) => {
                                            setPassword(e.target.value);
                                        }} />
                                    {/* Copy button to clipboard */}
                                    <Button variant="secondary" onClick={() => {
                                        navigator.clipboard.writeText(password);
                                        // Display a success toast
                                        toasts.show({
                                            headerContent: 'Succès',
                                            bodyContent: 'Mot de passe copié dans le presse-papiers.',
                                            toastProps: {
                                                bg: 'success',
                                                autohide: true,
                                                delay: 5000,
                                            },
                                        });
                                    }}>
                                        <FontAwesomeIcon icon="copy" />
                                    </Button>
                                    {/* See button */}
                                    <Button variant="primary" onClick={() => {
                                        const passwordInput = document.getElementsByName("password")[0] as HTMLInputElement;
                                        if (passwordInput.type === 'password') {
                                            passwordInput.type = 'text';
                                        } else {
                                            passwordInput.type = 'password';
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="eye" />
                                    </Button>
                                </InputGroup>
                            </Form.Group>
                        </>
                        {/* File */}
                        <Form.Group className="mb-3">
                            <Form.Label>Fichier</Form.Label>
                            <Form.Control type="file" size="sm" onChange={handleFileInput} />
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" size="sm" onClick={() => setShowEdit(false)}>Fermer</Button>
                        <Button variant="primary" size="sm" type="submit">Enregistrer</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

function FolderAdd({ currentFolder, showAddFolder, handleCloseAddFolder, setModified }: { currentFolder: string, showAddFolder: boolean, handleCloseAddFolder: () => void, setModified: (modified: boolean) => void }) {

    const toasts = useToasts();

    const [folderName, setFolderName] = useState('');

    return (
        <>
            <Modal show={showAddFolder} size={"xl"} onHide={handleCloseAddFolder} style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <FontAwesomeIcon icon="folder-plus" className="me-2" />
                        Ajouter un dossier
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    addFolder(currentFolder, folderName).then((data: any) => {
                        if (data.status === 'success') {
                            handleCloseAddFolder();
                            setModified(true);
                            toasts.show({
                                headerContent: 'Succès',
                                bodyContent: data.message,
                                toastProps: {
                                    bg: 'success',
                                    autohide: true,
                                    delay: 5000,
                                },
                            });
                        } else {
                            toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: data.message,
                                toastProps: {
                                    bg: 'danger',
                                    autohide: true,
                                    delay: 5000,
                                },
                            });
                        }
                    });
                }}>
                    <Modal.Body>

                        <Form.Group className="mb-3">
                            <Form.Label>Nom</Form.Label>
                            <Form.Control type="text"
                                size="sm"
                                name="name"
                                onChange={(e) => setFolderName(e.target.value)} />
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" size="sm" onClick={handleCloseAddFolder}>Fermer</Button>
                        <Button variant="primary" size="sm" type="submit">Enregistrer</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}


// Get the number of pages
function getNumberOfPages(json: FilesManagerFiles[], numberPerPage: number) {
    return Math.ceil(json.length / numberPerPage);
}

// Pagination component
function Paginations({ numberOfPages, activePage, changePage }: { numberOfPages: number, activePage: number, changePage: (page: number) => void }) {
    return (
        <Pagination size="sm">
            <Pagination.Prev onClick={() => {
                if (activePage > 1) {
                    changePage(activePage - 1);
                }
            }} />
            {Array.from(Array(numberOfPages).keys()).map((page) => {
                return <Pagination.Item key={page} active={page === activePage - 1} onClick={() => {
                    changePage(page + 1);
                }}
                >{page + 1}</Pagination.Item>;
            })}
            <Pagination.Next onClick={() => {
                if (activePage < numberOfPages) {
                    changePage(activePage + 1);
                }
            }} />
        </Pagination>
    );
}

// Choose the icon according to the file extension
function chooseIcon(ext: string) {
    switch (ext) {
        case 'pdf':
            return 'file-pdf';
        case 'doc':
        case 'docx':
            return 'file-word';
        case 'xls':
        case 'xlsx':
            return 'file-excel';
        case 'ppt':
        case 'pptx':
            return 'file-powerpoint';
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            return 'file-image';
        case 'mp3':
        case 'wav':
            return 'file-audio';
        case 'mp4':
        case 'avi':
        case 'mov':
            return 'file-video';
        case 'zip':
        case 'rar':
        case '7z':
            return 'file-archive';
        default:
            return 'file';
    }
}
export default FilesManager;