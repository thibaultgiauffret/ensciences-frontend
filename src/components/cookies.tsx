// Components
import { Col, Row, Button, Container } from "react-bootstrap";

// Functions
import { useLocation } from "react-router-dom";
import { load, trackPageView } from "../functions/cookies/matomo";
import { useEffect, useState } from "react";

// CSS
import styles from "./css/cookies.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Matomo() {

    const [cookieBanner, setCookieBanner] = useState(false);
    const [allowCookies, setAllowCookies] = useState(false);

    const location = useLocation();

    // Check if matomo-disable cookie is set to true
    useEffect(() => {
        // Get the matomo-disable cookie
        const cookie = document.cookie.split(';').find((c) => c.trim().startsWith('matomo-disable='));
        if (cookie) {
            setCookieBanner(false);
            // If it's set to false, allow cookies
            if (cookie.split('=')[1] === 'false') {
                setAllowCookies(true);
                const matomoUrl = import.meta.env.VITE_MATOMO_URL;
                load(matomoUrl, 1);
            }

            // If it's set to true, don't allow cookies and remove all _pk cookies
            if (cookie.split('=')[1] === 'true') {
                setAllowCookies(false);
                document.cookie = "_pk_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.cookie = "_pk_ses=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.cookie = "_pk_ref=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.cookie = "_pk_cvar=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            }
        } else {
            setCookieBanner(true);
        }
    }, []);

    // Handle cookie banner
    const handleCookieBanner = (accept: boolean) => {
        if (accept) {
            setAllowCookies(true);
            setCookieBanner(false);
            document.cookie = "matomo-disable=false; expires=" + new Date(new Date().getTime() + 13 * 30 * 24 * 60 * 60 * 1000).toUTCString()+ "; path=/; domain=.ensciences.fr; SameSite=None; Secure";
            const matomoUrl = import.meta.env.VITE_MATOMO_URL;
            load(matomoUrl, 1);
            trackPageView();
        } else {
            setAllowCookies(false);
            setCookieBanner(false);
            document.cookie = "matomo-disable=true; expires=" + new Date(new Date().getTime() + 13 * 30 * 24 * 60 * 60 * 1000).toUTCString() + "; path=/; domain=.ensciences.fr; SameSite=None; Secure";
        }
    }

    useEffect(() => {
        if (allowCookies) {
            trackPageView();
        }
    }, [location.pathname, location.search]);

    return (
        cookieBanner &&
        <div className={styles.cookieBanner}>
            <Container>
                <Row className="d-flex align-items-center">
                    <Col xs={12} sm={9} md={10}>
                        <FontAwesomeIcon icon="cookie-bite" className="me-2" />Ce site utilise Matomo. En continuant à naviguer (sauf refus explicite), vous nous autorisez à déposer un cookie à des fins de mesure d'audience (pas d'utilisation publicitaire ni de revente des données, conservation des cookies pendant 13 mois).<br />
                    </Col>
                    <Col className="d-flex flex-column">
                        <Button variant="success" size="sm" className="m-1 mt-2" onClick={() => handleCookieBanner(true)}>
                            Accepter
                        </Button>

                        <Button variant="danger" size="sm" className="m-1" onClick={() => handleCookieBanner(false)}>
                            Refuser
                        </Button>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}