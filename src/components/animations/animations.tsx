// Imports
import { useState, useEffect, useRef } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Placeholder from "react-bootstrap/Placeholder";
import ScrollAnimation from "react-animate-on-scroll";
import { ButtonLink } from "../buttons";
import { AnimationEditor } from "./animationEditor"
import { Spinner } from "react-bootstrap";

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// CSS
import styles from "../css/cards.module.css";

function AnimationCard({ data, admin }: { data: AnimationData, admin: boolean }) {

    // Toggle modal
    const [showModal, setShowModal] = useState(false);
    const toggleModal = () => setShowModal(!showModal);

    const [isLoaded, setIsLoaded] = useState(false);

    const handleLoad = () => {
        console.log("Image loaded : " + data.image);
        setIsLoaded(true);
    };

    const handleError = () => {
        setIsLoaded(false);
    };

    // If all fields are empty, return an empty card with placeholders
    if (data.title === "") {
        // Placeholder card
        return (
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>
                    <Card.Img variant="top" src="/images/picturePlaceholder.svg" style={{ width: "100%", height: "160px", objectFit: "cover" }} />
                    <Card.Body className="text-center">
                        <Placeholder as={Card.Title} animation="glow">
                            <Placeholder xs={8} /><br />
                            <Placeholder xs={4} />
                        </Placeholder>
                    </Card.Body>
                    <Card.Footer className="text-center">
                        <Placeholder bg="primary" xs={6} />
                    </Card.Footer>
                </Card>
            </ScrollAnimation>
        );
    } else {
        // Card with icon, title, color and children
        return (
            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className="h-100 mx-auto py-2">
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>
                    <Card.Img
                        variant="top"
                        src={data.image}
                        onLoad={handleLoad}
                        onError={handleError}
                        style={{ width: "100%", height: "160px", objectFit: "cover", display: isLoaded ? "block" : "none" }}
                    />
                    {
                        !isLoaded &&
                        <Card.Img
                            variant="top"
                            src="/images/picturePlaceholder.svg"
                            style={{ width: "100%", height: "160px", objectFit: "cover" }}
                        />
                    }
                    <AnimationEditor animationData={data} mode="card" admin={admin} />
                    <Card.Body className="d-flex flex-column justify-content-center align-items-center text-center">
                        <Card.Title>{data.title}</Card.Title>
                    </Card.Body>
                    <Card.Footer className="text-center">
                        <ButtonLink className={styles.cardButton + " stretched-link fw-bold"} onClick={toggleModal}>
                            <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Consulter
                        </ButtonLink >
                    </Card.Footer>

                    {/* Modal */}
                    <AnimationModal data={data} show={showModal} setShow={setShowModal} />
                </Card>
            </ScrollAnimation>
        );
    }
}

function AnimationRow({ data, admin }: { data: AnimationData, admin: boolean }) {
    // Toggle modal
    const [showModal, setShowModal] = useState(false);
    const toggleModal = () => setShowModal(!showModal);

    return (
        <tr key={data.uuid}>
            <td>{data.title}</td>
            <td>{
                // Split keywords and display them as badges
                data.keywords.split(',').map((keyword: string, index: number) => {
                    return (
                        <span key={index} className={"badge bg-info me-1"}>{keyword}</span>
                    );
                })
            }</td>
            <td>
                <Button variant="primary" size="sm" onClick={toggleModal} style={{ margin: "5px" }}>
                    <FontAwesomeIcon icon="eye"></FontAwesomeIcon>
                </Button>
                <AnimationModal data={data} show={showModal} setShow={setShowModal} />
                <AnimationEditor animationData={data} admin={admin} />
            </td>
        </tr>
    );
}

function AnimationModal({ data, show, setShow }: { data: AnimationData, show: boolean, setShow: Function }) {

    // Refs
    const iframeRef = useRef<HTMLIFrameElement>(null);
    const [loading, setLoading] = useState(true);

    // Set the iframe source
    useEffect(() => {
        if (show && iframeRef.current) {
            iframeRef.current.src = data.url;
            setLoading(true); // Show spinner when URL is set
        }
    }, [show, data.url]);

    const handleLoad = () => {
        setLoading(false); // Hide spinner when iframe is loaded
        adjustIframeSize();
    };

    const handleError = () => {
        setLoading(false); // Hide spinner if there's an error
    };

    const modalBodyRef = useRef(null);
    const containerRef = useRef<HTMLDivElement>(null);
    // Adjust container height and width to fit in the modal but keep the aspect ratio of 4:3
    const adjustIframeSize = () => {
        if (modalBodyRef.current && containerRef.current) {
            const modalBodyHeight = window.innerHeight - 200;
            const modalBodyWidth = (modalBodyRef.current as HTMLElement).offsetWidth;
            const ratio = 4 / 3;

            let containerWidth, containerHeight;

            if (modalBodyHeight * ratio <= modalBodyWidth) {
                containerHeight = modalBodyHeight;
                containerWidth = Math.floor(modalBodyHeight * ratio);
            } else {
                containerWidth = modalBodyWidth;
                containerHeight = Math.floor(modalBodyWidth / ratio);
            }

            containerRef.current.style.width = `${containerWidth}px`;
            containerRef.current.style.height = `${containerHeight}px`;
        }
    };

    useEffect(() => {
        window.addEventListener('resize', adjustIframeSize);
        adjustIframeSize();
    }, []);

    return (
        <Modal show={show}
            size="xl" id={"modal_" + data.id} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
                <Modal.Title>
                    <FontAwesomeIcon icon="cube" className="me-2" />
                    {data.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body ref={modalBodyRef} style={{ overflowY: "hidden", padding: 0 }} className="d-flex justify-content-center">
                <div ref={containerRef} style={{ position: "relative" }}>
                    {loading && (
                        <div style={{
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)',
                            zIndex: 1,
                            textAlign: 'center'
                        }}>
                            <Spinner animation="border" role="status" className="mb-2">
                                <span className="visually-hidden">Chargement...</span>
                            </Spinner><br />
                            Chargement de l'animation...
                        </div>
                    )}
                    <iframe
                        title={data.title}
                        id={"iframe_" + data.id}
                        src={data.url}
                        ref={iframeRef}
                        width={"100%"}
                        height={"100%"}
                        onLoad={handleLoad}
                        onError={handleError}
                        style={{ visibility: loading ? 'hidden' : 'visible' }}
                    />
                </div>
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-between w-100">
                <span>
                    {data.keywords.split(',').map((keyword: string, index: number) => {
                        return (
                            <span key={index} className={"badge bg-info me-1"}>{keyword}</span>
                        );
                    })}
                </span>
                <span>
                    {data.urlSource !== "" &&
                        <Button variant="secondary" size="sm" href={data.urlSource} target="_blank" className="ms-2">
                            <FontAwesomeIcon icon="code"></FontAwesomeIcon>
                        </Button>
                    }
                    <Button variant="primary" size="sm" href={data.url} target="_blank" className="ms-2">
                        <FontAwesomeIcon icon="external-link-alt"></FontAwesomeIcon>&nbsp;Ouvrir
                    </Button>
                </span>
            </Modal.Footer>
        </Modal>
    );
}

export { AnimationCard, AnimationRow };