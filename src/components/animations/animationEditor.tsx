// Components
import { InputGroup, Modal, Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { useToasts } from 'react-bootstrap-toasts';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { BigCircleButton } from '../buttons';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useState } from 'react';
import { updateAnimation } from '../../functions/animations/updateAnimation';
import { deleteAnimation } from '../../functions/animations/deleteAnimation';

// Theme editor (button and modal)
function AnimationEditor({ animationData, mode, admin }: { animationData: AnimationData, mode?: string, admin:boolean }) {

    // Toasts
    const toasts = useToasts();

    // Show/hide modal
    const [modified, setModified] = useState<boolean>(false);
    const [data, setData] = useState<AnimationData>(animationData);
    const [show, setShowModal] = useState(false);
    const handleCloseEdit = (noAlert: boolean = false) => {
        if (!noAlert && modified) {
            if (confirm("Voulez-vous enregistrer les modifications ?")) {
                // saveData(true);
                setShowModal(false);
            } else {
                setShowModal(false);
            }
        } else {
            setShowModal(false);
        }
    }
    const handleShowModal = () => setShowModal(true);

    // Update the data
    const updateAnimationData = (e: any) => {
        updateAnimation(data.uuid, e.target.id.value, e.target.title.value, e.target.keywords.value, e.target.image.files[0], e.target.urlSource.value).then((response) => {
            if (response.status === 'success') {
                window.location.href = window.location.href + '?notification=success&message=Animation+modifiée.';
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: "Une erreur s'est produite lors de la mise à jour de l'animation : " + response.status,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        })
    }

    return (
        admin &&
        <>
            {/* Edit button */}
            <Button size="sm" variant="warning"
                onClick={() => {
                    handleShowModal();
                }}
                style={mode === "card" ? (
                    {
                        position: "absolute",
                        top: 5,
                        right: 5,
                        zIndex: 20
                    }) : {
                        margin: "5px"
                    }}>
                <FontAwesomeIcon icon="edit" />
            </Button>
            {/* Edit Modal */}
            <Modal show={show} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Éditer l'animation
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    updateAnimationData(e);
                }}>
                    <Modal.Body>
                        {/* Hidden animation uuid */}
                        <Form.Control type="hidden" value={data.uuid} name="uuid" id="uuid" />
                        {/* Animation id */}
                        <InputGroup className="mb-3" size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationId">Identifiant de l'animation</Tooltip>}>
                                <InputGroup.Text>#</InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control
                                placeholder="Identifiant de l'animation"
                                aria-label="Identifiant de l'animation"
                                aria-describedby="animationId"
                                id="id"
                                name="id"
                                value={data.id}
                                onChange={(e) => {
                                    setData({ ...data, id: e.target.value });
                                    setModified(true);
                                }}
                                required
                            />
                        </InputGroup>

                        {/* Animation title */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationTitle">Titre de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="font" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Titre de l'animation" id="title" name="title" required value={data.title}
                                onChange={(e) => {
                                    setData({ ...data, title: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                        {/* Animation Image */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationImage">Image de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="image" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            {/* File input */}
                            <Form.Control type="file" id="image" name="image" accept="image/*" />
                        </InputGroup>

                        {/* Display the current image */}
                        {data.image && (
                            <img src={data.image} alt="Image de l'animation" className="img-fluid mt-3" />
                        )}

                        {/* Animation keywords */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationKeywords">Mots-clés de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="tags" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Mots-clés de l'animation" id="keywords" name="keywords" required value={data.keywords}
                                onChange={(e) => {
                                    setData({ ...data, keywords: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>
                        <Form.Text className="text-muted">
                            Séparer les mots-clés par des virgules
                        </Form.Text>

                        {/* Animation source url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationSource">Sources de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="code" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Source de l'animation" id="urlSource" name="urlSource" value={data.urlSource}
                                onChange={(e) => {
                                    setData({ ...data, urlSource: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                    </Modal.Body>
                    <Modal.Footer>

                        {/* Submit button */}
                        <Button variant="primary" type="submit" size="sm">
                            <FontAwesomeIcon icon="save" />
                            &nbsp;Enregistrer
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            {/* Delete button */}
            <Button size="sm" variant="danger"
                onClick={() => {
                    if (confirm("Voulez-vous vraiment supprimer cette animation ?")) {
                        deleteAnimation(data.uuid).then((response) => {
                            if (response.status === 'success') {
                                window.location.href = window.location.href + '?notification=success&message=Animation+supprimée.';
                            } else {
                                toasts.show({
                                    headerContent: 'Erreur',
                                    bodyContent: "Une erreur s'est produite lors de la suppression de l'animation : " + response.status,
                                    toastProps: {
                                        bg: 'danger',
                                        autohide: true,
                                        delay: 5000,
                                    },
                                });
                            }
                        });
                    }
                }}
                style={
                    mode === "card" ? {
                        position: "absolute",
                        top: 42,
                        right: 5,
                        zIndex: 20
                    } : {
                         margin: "5px"
                    }}>
                <FontAwesomeIcon icon="trash" />
            </Button>
        </>
    );
}

function AddAnimation() {

    // Toasts
    const toasts = useToasts();

    // Show/hide modal
    const [show, setShowModal] = useState(false);
    const handleCloseEdit = (noAlert: boolean = false) => {
        if (!noAlert) {
            if (confirm("Voulez-vous annuler l'ajout de l'animation ?")) {
                setShowModal(false);
            }
        } else {
            setShowModal(false);
        }
    }

    const handleShowModal = () => setShowModal(true);

    // Add the data
    const addAnimationData = (e: any) => {
        updateAnimation(e.target.uuid.value, e.target.id.value, e.target.title.value, e.target.keywords.value, e.target.image.files[0], e.target.urlSource.value).then((response) => {
            if (response.status === 'success') {
                window.location.href = window.location.href + '?notification=success&message=Animation+ajoutée.';
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: "Une erreur s'est produite lors de l'ajout de l'animation : " + response.status,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        })
    }

    const [data, setData] = useState<AnimationData>({ uuid: -1, id: "", title: "", keywords: "", image: "", url: "", urlSource: "" });

    return (
        <>
            {/* Edit button */}
            <div  className="toolBar">
                    <BigCircleButton  className="toolBtn" color="var(--bs-warning)" icon="plus" onClick={handleShowModal} tooltip='Ajouter une animation' />
            </div>
            {/* Edit Modal */}
            <Modal show={show} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Ajouter une animation
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    addAnimationData(e);
                }}>
                    <Modal.Body>
                        {/* Hidden animation uuid */}
                        <Form.Control type="hidden" value="-1" name="uuid" id="uuid" />
                        {/* Animation id */}
                        <InputGroup className="mb-3" size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationId">Identifiant de l'animation</Tooltip>}>
                                <InputGroup.Text>#</InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control
                                placeholder="Identifiant de l'animation"
                                aria-label="Identifiant de l'animation"
                                aria-describedby="animationId"
                                id="id"
                                name="id"
                                value={data.id}
                                onChange={(e) => {
                                    setData({ ...data, id: e.target.value });
                                }}
                                required
                            />
                        </InputGroup>

                        {/* Animation title */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationTitle">Titre de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="font" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Titre de l'animation" id="title" name="title" required value={data.title}
                                onChange={(e) => {
                                    setData({ ...data, title: e.target.value });
                                }} />
                        </InputGroup>

                        {/* Animation Image */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationImage">Image de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="image" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            {/* File input */}
                            <Form.Control type="file" id="image" name="image" accept="image/*" />
                        </InputGroup>

                        {/* Display the current image */}
                        {data.image && (
                            <img src={data.image} alt="Image de l'animation" className="img-fluid mt-3" />
                        )}

                        {/* Animation keywords */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationKeywords">Mots-clés de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="tags" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Mots-clés de l'animation" id="keywords" name="keywords" required value={data.keywords}
                                onChange={(e) => {
                                    setData({ ...data, keywords: e.target.value });
                                }} />
                        </InputGroup>
                        <Form.Text className="text-muted">
                            Séparer les mots-clés par des virgules
                        </Form.Text>

                        {/* Animation source url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="animationSource">Sources de l'animation</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="code" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Source de l'animation" id="urlSource" name="urlSource" value={data.urlSource}
                                onChange={(e) => {
                                    setData({ ...data, urlSource: e.target.value });
                                }} />
                        </InputGroup>

                    </Modal.Body>
                    <Modal.Footer>

                        {/* Submit button */}
                        <Button variant="primary" type="submit" size="sm">
                            <FontAwesomeIcon icon="save" />
                            &nbsp;Enregistrer
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );

}

export { AnimationEditor, AddAnimation };