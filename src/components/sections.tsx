// ----------------------------
// Section components
// ----------------------------

import React from "react";
import styles from "./css/sections.module.css";
import "@fontsource/dosis/600.css";
import "@fontsource/dosis/700.css";

interface H2Props {
  children: React.ReactNode;
  className?: string;
}

function H2({ children, className }: H2Props) {
  // Mix styles.h2 with props.className
  let finalClassName = styles.h2;
  finalClassName += className ? ' ' + className : '';
  return (
    <h2 className={finalClassName}>
      {children}
    </h2>
  );
}

function H3({ children, className }: H2Props) {
  // Mix styles.h3 with props.className
  let finalClassName = styles.h3;
  finalClassName += className ? ' ' + className : '';
  return (
    <h3 className={finalClassName}>
      {children}
    </h3>
  );
}

export { H2 };
export { H3 };
