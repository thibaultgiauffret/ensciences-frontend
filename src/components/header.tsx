// ----------------------------
// Header component
// The page header
// ----------------------------

// Components
import DOMPurify from 'dompurify';

import styles from "./css/header.module.css";

function Header({ title, subtitle, color, children }: { title: string, subtitle?: string, color?: string, children?: any }) {
  return (
    <div className={styles.header} style={{ backgroundColor: color }}>
      <h1 className={styles.title} id="pageHeader">{title}</h1>
      <h3 className={styles.subtitle} id="pageSubtitle">
        <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(subtitle || '') }}></div>
      </h3>
      {children}
    </div>
  );
}

export default Header;