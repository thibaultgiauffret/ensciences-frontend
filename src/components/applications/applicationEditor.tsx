// Components
import { InputGroup, Modal, Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { useToasts } from 'react-bootstrap-toasts';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useState } from 'react';
import { updateApplication } from '../../functions/applications/updateApplication';
import { deleteApplication } from '../../functions/applications/deleteApplication';
import { BigCircleButton } from '../buttons';

// Theme editor (button and modal)
function ApplicationEditor({ applicationData, mode, admin }: { applicationData: ApplicationData, mode?: string, admin: boolean }) {

    // Toasts
    const toasts = useToasts();

    // Show/hide modal
    const [modified, setModified] = useState<boolean>(false);
    const [data, setData] = useState<ApplicationData>(applicationData);
    const [show, setShowModal] = useState(false);
    const handleCloseEdit = (noAlert: boolean = false) => {
        if (!noAlert && modified) {
            if (confirm("Voulez-vous enregistrer les modifications ?")) {
                // saveData(true);
                setShowModal(false);
            } else {
                setShowModal(false);
            }
        } else {
            setShowModal(false);
        }
    }
    const handleShowModal = () => setShowModal(true);

    // Update the data
    const updateApplicationData = (e: any) => {
        const applicationData = {
            uuid: data.uuid,
            id: e.target.id.value,
            title: e.target.title.value,
            desc: e.target.desc.value,
            logo: e.target.logo.files[0],
            url: e.target.url.value,
            urlSource: e.target.urlSource.value,
            badgeDev: e.target.badgeDev.checked,
            badgeNew: e.target.badgeNew.checked,
            badgeThirdParty: e.target.badgeThirdParty.checked
        };
        updateApplication(applicationData).then((response) => {
            if (response.status === 'success') {
                window.location.href = window.location.href + '?notification=success&message=Application+modifiée.';
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: "Une erreur s'est produite lors de la mise à jour de l'application : " + response.status,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        })
    }

    return (
        admin &&
        <>
            {/* Edit button */}
            <Button size="sm" variant="warning"
                onClick={() => {
                    handleShowModal();
                }}
                style={mode === "card" ? (
                    {
                        position: "absolute",
                        top: 5,
                        left: 5,
                        zIndex: 20
                    }) : {
                    margin: "5px"
                }}>
                <FontAwesomeIcon icon="edit" />
            </Button>
            {/* Edit Modal */}
            <Modal show={show} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Éditer l'application
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    updateApplicationData(e);
                }}>
                    <Modal.Body>
                        {/* Hidden application uuid */}
                        <Form.Control type="hidden" value={data.uuid} name="uuid" id="uuid" />
                        {/* Application id */}
                        <InputGroup className="mb-3" size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationId">Identifiant de l'application</Tooltip>}>
                                <InputGroup.Text>#</InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control
                                placeholder="Identifiant de l'application"
                                aria-label="Identifiant de l'application"
                                aria-describedby="applicationId"
                                id="id"
                                name="id"
                                value={data.id}
                                onChange={(e) => {
                                    setData({ ...data, id: e.target.value });
                                    setModified(true);
                                }}
                                required
                            />
                        </InputGroup>

                        {/* Application title */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationTitle">Titre de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="font" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Titre de l'application" id="title" name="title" required value={data.title}
                                onChange={(e) => {
                                    setData({ ...data, title: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                        {/* Application Logo */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationLogo">Image de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="image" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            {/* File input */}
                            <Form.Control type="file" id="logo" name="logo" accept="image/*" />
                        </InputGroup>

                        {/* Display the current image */}
                        {data.logo && (
                            <img src={data.logo} alt="Logo de l'application" className="img-fluid mt-3" />
                        )}

                        {/* Application keywords */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationKeywords">Description de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="align-left" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Mots-clés de l'application" id="desc" name="keywords" required value={data.desc}
                                onChange={(e) => {
                                    setData({ ...data, desc: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                        {/* Application url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="application">URL de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="link" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="URL de l'application" id="url" name="url" value={data.url}
                                onChange={(e) => {
                                    setData({ ...data, url: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                        {/* Application source url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationSource">Sources de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="code" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Source de l'application" id="urlSource" name="urlSource" value={data.urlSource}
                                onChange={(e) => {
                                    setData({ ...data, urlSource: e.target.value });
                                    setModified(true);
                                }} />
                        </InputGroup>

                        {/* Badge checkbox */}
                        <Form.Check
                            type="checkbox"
                            id="badgeDev"
                            name="badgeDev"
                            label='Badge "en cours de développement"'
                            className='mt-3'
                            checked={data.badgeDev === "true"}
                            onChange={(e) => setData({ ...data, badgeDev: e.target.checked ? "true" : "false" })}
                        />

                        <Form.Check
                            type="checkbox"
                            id="badgeNew"
                            name="badgeNew"
                            label='Badge "nouveauté"'
                            className='mt-3'
                            checked={data.badgeNew === "true"}
                            onChange={(e) => setData({ ...data, badgeNew: e.target.checked ? "true" : "false" })}
                        />

                        <Form.Check
                            type="checkbox"
                            id="badgeThirdParty"
                            name="badgeThirdParty"
                            label='Badge "application tierce"'
                            className='mt-3'
                            checked={data.badgeThirdParty === "true"}
                            onChange={(e) => setData({ ...data, badgeThirdParty: e.target.checked ? "true" : "false" })}
                        />

                    </Modal.Body>
                    <Modal.Footer>

                        {/* Submit button */}
                        <Button variant="primary" type="submit" size="sm">
                            <FontAwesomeIcon icon="save" />
                            &nbsp;Enregistrer
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            {/* Delete button */}
            <Button size="sm" variant="danger"
                onClick={() => {
                    if (confirm("Voulez-vous vraiment supprimer cette application ?")) {
                        deleteApplication(data.uuid).then((response) => {
                            if (response.status === 'success') {
                                window.location.href = window.location.href + '?notification=success&message=Application+supprimée.';
                            } else {
                                toasts.show({
                                    headerContent: 'Erreur',
                                    bodyContent: "Une erreur s'est produite lors de la suppression de l'application : " + response.status,
                                    toastProps: {
                                        bg: 'danger',
                                        autohide: true,
                                        delay: 5000,
                                    },
                                });
                            }
                        });
                    }
                }}
                style={
                    mode === "card" ? {
                        position: "absolute",
                        left: 42,
                        top: 5,
                        zIndex: 20
                    } : {
                        margin: "5px"
                    }}>
                <FontAwesomeIcon icon="trash" />
            </Button>
        </>
    );
}

function AddApplication() {

    // Toasts
    const toasts = useToasts();

    // Show/hide modal
    const [show, setShowModal] = useState(false);
    const handleCloseEdit = (noAlert: boolean = false) => {
        if (!noAlert) {
            if (confirm("Voulez-vous annuler l'ajout de l'application ?")) {
                setShowModal(false);
            }
        } else {
            setShowModal(false);
        }
    }

    const handleShowModal = () => setShowModal(true);

    // Add the data
    const addApplicationData = (e: any) => {
        const applicationData = {
            uuid: -1,
            id: e.target.id.value,
            title: e.target.title.value,
            desc: e.target.desc.value,
            logo: e.target.logo.files[0],
            url: e.target.url.value,
            urlSource: e.target.urlSource.value,
            badgeDev: e.target.badgeDev.checked,
            badgeNew: e.target.badgeNew.checked,
            badgeThirdParty: e.target.badgeThirdParty.checked
        };
        updateApplication(applicationData).then((response) => {
            if (response.status === 'success') {
                window.location.href = window.location.href + '?notification=success&message=Application+ajoutée.';
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: "Une erreur s'est produite lors de l'ajout de l'application : " + response.status,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        })
    }

    const [data, setData] = useState<ApplicationData>({ uuid: -1, id: "", title: "", desc: "", logo: "", url: "", urlSource: "", badgeDev: "", badgeNew: "", badgeThirdParty: "" });

    return (
        <>
            {/* Add button */}
            <div  className="toolBar">
                    <BigCircleButton  className="toolBtn" color="var(--bs-warning)" icon="plus" onClick={handleShowModal} tooltip='Ajouter une application' />
            </div>
            {/* Add Modal */}
            < Modal show={show} onHide={handleCloseEdit} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Ajouter une application
                    </Modal.Title>
                </Modal.Header>
                <Form onSubmit={(e) => {
                    e.preventDefault();
                    addApplicationData(e);
                }}>
                    <Modal.Body>
                        {/* Hidden application uuid */}
                        <Form.Control type="hidden" value="-1" name="uuid" id="uuid" />
                        {/* Application id */}
                        <InputGroup className="mb-3" size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationId">Identifiant de l'application</Tooltip>}>
                                <InputGroup.Text>#</InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control
                                placeholder="Identifiant de l'application"
                                aria-label="Identifiant de l'application"
                                aria-describedby="applicationId"
                                id="id"
                                name="id"
                                value={data.id}
                                onChange={(e) => {
                                    setData({ ...data, id: e.target.value });
                                }}
                                required
                            />
                        </InputGroup>

                        {/* Application title */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationTitle">Titre de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="font" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Titre de l'application" id="title" name="title" required value={data.title}
                                onChange={(e) => {
                                    setData({ ...data, title: e.target.value });
                                }} />
                        </InputGroup>

                        {/* Application Image */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationImage">Logo de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="image" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            {/* File input */}
                            <Form.Control type="file" id="logo" name="logo" accept="image/*" />
                        </InputGroup>

                        {/* Display the current image */}
                        {data.logo && (
                            <img src={data.logo} alt="Image de l'application" className="img-fluid mt-3" />
                        )}

                        {/* Application keywords */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationKeywords">Description de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="align-left" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control as="textarea" placeholder="Description de l'application" id="desc" name="desc" required value={data.desc}
                                onChange={(e) => {
                                    setData({ ...data, desc: e.target.value });
                                }} />
                        </InputGroup>

                        {/* Application url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="application">URL de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="link" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="URL de l'application" id="url" name="url" value={data.url}
                                onChange={(e) => {
                                    setData({ ...data, url: e.target.value });
                                }} />
                        </InputGroup>

                        {/* Application source url */}
                        <InputGroup className='mt-3' size="sm">
                            <OverlayTrigger placement="top" overlay={<Tooltip id="applicationSource">Sources de l'application</Tooltip>}>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon="code" />
                                </InputGroup.Text>
                            </OverlayTrigger>
                            <Form.Control placeholder="Source de l'application" id="urlSource" name="urlSource" value={data.urlSource}
                                onChange={(e) => {
                                    setData({ ...data, urlSource: e.target.value });
                                }} />
                        </InputGroup>

                        {/* Badge checkbox */}
                        <Form.Check type="checkbox" id="badgeDev" name="badgeDev" label='Badge "en cours de développement"' className='mt-3' />

                        <Form.Check type="checkbox" id="badgeNew" name="badgeNew" label='Badge "nouveauté"' className='mt-3' />

                        <Form.Check type="checkbox" id="badgeThirdParty" name="badgeThirdParty" label='Badge "application tierce"' className='mt-3' />

                    </Modal.Body>
                    <Modal.Footer>

                        {/* Submit button */}
                        <Button variant="primary" type="submit" size="sm">
                            <FontAwesomeIcon icon="save" />
                            &nbsp;Enregistrer
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal >
        </>
    );

}

export { ApplicationEditor, AddApplication };