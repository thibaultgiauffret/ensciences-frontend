// Imports
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Placeholder from "react-bootstrap/Placeholder";
import ScrollApplication from "react-animate-on-scroll";
import { Row, Col } from "react-bootstrap";
import { ApplicationEditor } from "./applicationEditor"
import { Badge } from "react-bootstrap";

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// CSS
import styles from "../css/cards.module.css";

function ApplicationCard({ data, admin }: { data: ApplicationData, admin: boolean }) {

    const cleanedTitle = data.title.replace(/\\'/g, "'");
    const cleanedDesc = data.desc.replace(/\\'/g, "'");

    // If all fields are empty, return an empty card with placeholders
    if (data.title === "") {
        // Placeholder card
        return (
            <ScrollApplication animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>
                    <Card.Body className="d-flex flex-column justify-content-center align-items-center text-center">
                        <Row className="w-100">
                            <Col xs={4}>
                                <div style={{ backgroundColor: "#555", width: "50px", height: "50px" }} ></div>
                            </Col>
                            <Col>
                                <Placeholder as={Card.Title} animation="glow">
                                    <Placeholder xs={6} />
                                </Placeholder>
                                <Placeholder as={Card.Text} animation="glow">
                                    <Placeholder xs={6} />
                                    <Placeholder xs={6} />
                                </Placeholder>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer className="text-center">
                        {/* Placeholder button */}
                        <Placeholder.Button variant="primary" xs={4} />
                    </Card.Footer>
                </Card>
            </ScrollApplication>
        );
    } else {
        // Card with icon, title, color and children
        return (
            <ScrollApplication animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className="h-100 mx-auto py-2">
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>

                    <Card.Body className="d-flex flex-column align-items-center text-center">
                        <Row>
                            <Col xs={4}>
                                <Card.Img src={data.logo} style={{ width: "100%", height: "auto", }} />
                            </Col>
                            <Col className="text-start align-items-top">
                                <ApplicationEditor applicationData={data} mode="card" admin={admin} />
                                <Card.Title style={{ fontWeight: 900 }}>{cleanedTitle}</Card.Title>
                                <Card.Text className="small">{cleanedDesc}</Card.Text>
                                {/* Display badges */}
                                {data.badgeDev === "true" && <Badge bg="danger">
                                    <FontAwesomeIcon icon="person-digging"></FontAwesomeIcon>&nbsp;En développement
                                </Badge>}
                                {data.badgeNew === "true" && <Badge bg="success">
                                    <FontAwesomeIcon icon="star"></FontAwesomeIcon>&nbsp;Nouveau
                                </Badge>}
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer className="text-center">
                        <Button className={styles.cardButton + " fw-bold"} href={data.url} target="_blank" variant="primary">
                            <FontAwesomeIcon icon="play"></FontAwesomeIcon>&nbsp;Ouvrir
                        </Button>
                        {
                            data.urlSource &&
                            <Button className={styles.cardButton + " fw-bold ms-2"} href={data.urlSource} target="_blank" variant="secondary">
                                <FontAwesomeIcon icon="code"></FontAwesomeIcon>
                            </Button>
                        }
                    </Card.Footer>
                </Card>
            </ScrollApplication>
        );
    }
}

function ApplicationRow({ data, admin }: { data: ApplicationData, admin: boolean }) {


    const cleanedTitle = data.title.replace(/\\'/g, "'");
    const cleanedDesc = data.desc.replace(/\\'/g, "'");

    return (
        <tr key={data.uuid}>
            <td>{cleanedTitle}</td>
            <td>{cleanedDesc}</td>
            <td>
                <Button variant="primary" size="sm" href={data.url} target="_blank" style={{ margin: "5px" }}>
                    <FontAwesomeIcon icon="eye"></FontAwesomeIcon>
                </Button>
                {data.urlSource &&
                    <Button variant="secondary" size="sm" style={{ margin: "5px" }} href={data.urlSource} target="_blank">
                        <FontAwesomeIcon icon="code"></FontAwesomeIcon>
                    </Button>
                }

                <ApplicationEditor applicationData={data} admin={admin} />
            </td>
        </tr>
    );
}

export { ApplicationCard, ApplicationRow };