// ----------------------------
// Collapsible alert component
// ----------------------------

import React from "react";
import Alert from "react-bootstrap/Alert";
import styles from "./css/collapsibleAlert.module.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import Collapse from "react-bootstrap/Collapse";

// Add the icons to the library
library.add(fas);


// Collapsible alert component
export const CollapsibleAlert = (props: any) => {
    const [show, setShow] = React.useState(false);
    return (
        <Alert variant={props.variant} className={styles.collapsibleAlert + " p-0"} >
            {/* Show or hide the content when the alert heading is clicked */}
            <div onClick={() => setShow(!show)} className={styles.headingContainer} >
                <div className="d-flex justify-content-between p-3"
                >
                    <Alert.Heading className={styles.heading}>{
                        // Display the icon if it is provided
                        props.icon &&
                        <FontAwesomeIcon icon={props.icon} className="me-3" />}{props.title}</Alert.Heading>
                    <div className={styles.icon}>
                        <FontAwesomeIcon icon="chevron-down" id="chevron"
                            // Rotate the icon when the content is shown
                            className={`${styles.icon} ${show ? styles.rotate : styles.noRotate}`} />
                    </div>
                </div>
            </div>
            <Collapse in={show}>
                <div className="px-3 py-2">
                    {props.children}
                </div>
            </Collapse>
        </Alert>
    );
}

