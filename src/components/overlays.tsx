import { OverlayTrigger, Popover } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Message to display when the user is not connected and the file is restricted
function OverlayRestrictedMessage({ connected, children }: { connected: boolean | undefined, children: any }) {
    return (
        <OverlayTrigger overlay={<Popover>
            <Popover.Header as="h3">
                <FontAwesomeIcon icon="lock" style={{ color: "var(--bs-secondary)" }} /> Accès restreint
            </Popover.Header>
            <Popover.Body>
                {connected ? (
                    <span>
                        Veuillez cliquer pour demander une autorisation.
                    </span>
                ) : (
                    <span>
                        Veuillez cliquer pour vous connecter.
                    </span>
                )}
            </Popover.Body>
        </Popover>}>
            {children}
        </OverlayTrigger>
    )
}

export { OverlayRestrictedMessage };