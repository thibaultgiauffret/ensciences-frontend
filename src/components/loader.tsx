// A loader screen that is displayed while the main content is loading
import styles from './css/loader.module.css';

function Loader({ marginTop = true }: { marginTop?: boolean }) {
    return (
        <div className={styles.loader + ' ' + (marginTop ? styles.marginTop : '')}>
            <div className={styles.loaderContainer + ' mb-3'}>
                <div className={styles.tubeHead}></div>
                <div className={styles.tube}>

                    <div className={styles.liquid}>
                        <div className={styles.surface}></div>
                    </div>

                </div>
                <div className={styles.bubble}></div>
                <div className={styles.bubble}></div>
                <div className={styles.bubble}></div>
                <div className={styles.bubble}></div>
                <div>
                    <div className={styles.topBubble}></div>
                    <div className={styles.topBubble}></div>
                    <div className={styles.topBubble}></div>
                    <div className={styles.topBubble}></div>
                </div>
            </div>

            {/* <div className={styles.loaderText}>
                <span>Chargement en cours...</span>
            </div> */}

            <div className={styles.loaderCredits}>
                <img src="./images/favicon.svg" alt="Logo" className={styles.loaderLogo} /><br />
                <span className={styles.loaderBrand}>EnSciences<br />
                    <span className={styles.loaderDate}>Th. G &copy; 2018&nbsp;-&nbsp;2024</span>
                </span>
            </div>
        </div>
    );
};

export { Loader };