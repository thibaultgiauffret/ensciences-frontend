// ----------------------------
// MastodonWidget component
// ----------------------------

import { useEffect } from "react";
import Card from "react-bootstrap/Card";
import styles from "./css/cards.module.css"
import * as MastodonTimeline from "@idotj/mastodon-embed-timeline";
import "@idotj/mastodon-embed-timeline/dist/mastodon-timeline.min.css";
import "./css/mastodonWidget.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonLink } from "./buttons";

function MastodonWidget() {
    useEffect(() => {
        new MastodonTimeline.Init({
            instanceUrl: "https://mastodon.online",
            timelineType: "profile",
            userId: "108209151888080877",
            profileName: "@ThibGiauffret",
            defaultTheme: "light",
        });
    }, []);

    return (
        <Card className={styles.card + " mb-3"}>
            <Card.Header>
                    <FontAwesomeIcon icon={["fab", "mastodon"]} />&nbsp;Mastodon
            </Card.Header>

            <div className="cardText">
                <div id="mt-container" className="mt-container">
                    <div className="mt-body" role="feed">
                        <div className="mt-loading-spinner"></div>
                    </div>
                </div>
            </div>
            <Card.Footer className={styles.cardFooter}>
                <ButtonLink href="https://mastodon.online/@ThibGiauffret" className={styles.cardButton}>
                    <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Consulter
                </ButtonLink>
            </Card.Footer>
        </Card >
    )
}

export default MastodonWidget;

