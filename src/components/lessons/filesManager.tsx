// Components
import { Modal, Dropdown, Button } from 'react-bootstrap';
import FilesManagerContent from '../files/filesManager';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useState } from 'react';
import { updateJson } from '../../functions/lessons/updateJson';
import { useToasts } from 'react-bootstrap-toasts';


// Lessons files manager
function FilesManager({ level, version }: { level: string, version: string }) {

    const toasts = useToasts();

    // Show/hide the modal
    const [show, setShowEdit] = useState(false);
    const [modified, setModified] = useState(false);

    const handleCloseEdit = () => {
        if (modified) {
            updateJsonHandler(level, toasts);
            setShowEdit(false);
        } else {
            setShowEdit(false);
        }
    }
    const handleShowEdit = () => setShowEdit(true);

    // Update the JSON file
    const handleUpdateJson = (level: string) => {
        updateJsonHandler(level, toasts);
    }

    return (
        <>
            {/* Dropdown Item */}
            <Dropdown.Item className='btn-sm me-2'
                onClick={() => {
                    handleShowEdit();
                }}>
                <FontAwesomeIcon icon="file" />&nbsp;Gérer les fichiers
            </Dropdown.Item>
            {/* Modal */}
            <Modal show={show} fullscreen={true} onHide={handleCloseEdit}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <FontAwesomeIcon icon="file" className="me-2" />
                        Gérer les fichiers
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {/* Files manager table */}
                    <FilesManagerContent folder={"/files/cours/" + level + "/" + version} setModified={setModified} />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={() => handleUpdateJson(level)}>
                        Forcer la mise à jour
                    </Button>
                    <Button variant="secondary" onClick={handleCloseEdit}>
                        Fermer
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );

}

function updateJsonHandler(level: string, toasts: any) {
    updateJson(level).then((data) => {
        if (data.status === 'success') {
            const url = new URL(window.location.href);
            const params = new URLSearchParams(url.search);

            // Add the notification to the URL
            params.set('notification', 'success');
            params.set('message', 'Le fichier JSON a été mis à jour avec succès.');

            // Update the URL
            url.search = params.toString();
            window.location.href = url.toString();
        } else {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la mise à jour du fichier JSON.',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        }
    });
}

export default FilesManager;