// Components
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import { CollapsibleAlert } from '../collapsibleAlert';
import IconSelector from '../iconSelector';
import { Button } from 'react-bootstrap';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

// CSS
import styles from '../css/cards.module.css';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// CodeMirror
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/mode/xml/xml';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';

// Functions
import { useEffect, useState, useRef } from 'react';

// Themes editor card (modal content)
function ThemesEditorContent({ version, json, setJson, setModified }: { version: string, json: Lessons, setJson: any, setModified: any }) {

    const isFirstRender = useRef(true);

    // Filter the json.version and keep only the version we want to edit
    const versionData = json.versions.filter((v: Version) => v.version === version);
    const [themes, setThemes] = useState<Theme[]>(versionData[0].themes);

    // Send the updated themes to the parent component when the themes are updated
    useEffect(() => {
        // Do nothing on the first render
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        setJson({
            ...json, versions: json.versions.map((v: Version) => {
                if (v.version === version) {
                    return { ...v, themes: themes };
                }
                return v;
            })
        });
        setModified(true);
    }, [themes]);

    // Handle the tab click
    const [activeTab, setActiveTab] = useState<number>(themes.length > 0 ? themes[0].id : 0);
    const handleTabClick = (tabId: number) => {
        setActiveTab(tabId);
    };

    // Add a new theme
    const handleAddTheme = () => {
        const newThemes = addTheme([...themes]);
        let newThemeId = 0;
        if (newThemes.length > 0) {
            newThemeId = newThemes[newThemes.length - 1].id;
        }
        setThemes(newThemes);
        setActiveTab(newThemeId);
    };

    // CodeMirror editor reference
    const editorRef = useRef<any>(null);

    useEffect(() => {
        if (editorRef.current) {
            editorRef.current.editor.refresh();
        }
    }, []);

    return (
        <Card className={styles.card + " mb-3"} key={themes.length}>
            <Card.Header>
                <ul className="nav nav-tabs card-header-tabs">
                    {/* Theme ids */}
                    {themes && themes.map((theme: Theme) => (
                        <li key={theme.id + '_theme_nav'} className="nav-item">
                            <a onClick={() => {
                                handleTabClick(theme.id)
                            }} className={"nav-link " + (
                                activeTab === theme.id ? 'active' : ''
                            )}>
                                {theme.name}
                            </a>
                        </li>
                    ))}

                    {/* Add theme button */}
                    <li className="nav-item">
                        <a className='nav-link' onClick={handleAddTheme}>
                            <FontAwesomeIcon icon="plus" />
                        </a>
                    </li>
                </ul>
            </Card.Header>

            {/* Card Body */}
            <Card.Body>
                <div className="tab-content">
                    {themes && themes.map((theme: Theme) => (
                        <div key={theme.id} className={"tab-pane " + (
                            activeTab === theme.id ? 'active' : ''
                        )} id={theme.id.toString()}>

                            <div key={theme.id + "_theme_content"}>
                                <h4>Paramétrage du thème</h4>
                                {/* Theme parameters */}
                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Identifiant unique du thème</Tooltip>}>
                                        <InputGroup.Text id="theme_id">
                                            #
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Identifiant"
                                        aria-label="Identifiant"
                                        aria-describedby="theme_id"
                                        value={theme.name}
                                        data-path={"themes[" + theme.id + "].name"}
                                        onChange={(e) => {
                                            const updatedThemes = themes.map((t: Theme) => {
                                                if (t.id === theme.id) {
                                                    return { ...t, name: e.target.value };
                                                }
                                                return t;
                                            });
                                            setThemes(updatedThemes);
                                        }}
                                    />
                                    {/* Move left and right */}
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le thème vers la gauche</Tooltip>}>
                                        <Button variant="secondary" size="sm" onClick={() => {
                                            const currentIndex = themes.findIndex(t => t.id === theme.id);
                                            if (currentIndex > 0) {
                                                const updatedThemes = [...themes];
                                                const temp = updatedThemes[currentIndex - 1];
                                                updatedThemes[currentIndex - 1] = updatedThemes[currentIndex];
                                                updatedThemes[currentIndex] = temp;
                                                setThemes(updatedThemes);
                                            }
                                        }}>
                                            <FontAwesomeIcon icon="arrow-left" />
                                        </Button>
                                    </OverlayTrigger>

                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le thème vers la droite</Tooltip>}>
                                        <Button variant="secondary" size="sm" onClick={() => {
                                            const currentIndex = themes.findIndex(t => t.id === theme.id);
                                            if (currentIndex < themes.length - 1) {
                                                const updatedThemes = [...themes];
                                                const temp = updatedThemes[currentIndex + 1];
                                                updatedThemes[currentIndex + 1] = updatedThemes[currentIndex];
                                                updatedThemes[currentIndex] = temp;
                                                setThemes(updatedThemes);
                                            }
                                        }}>
                                            <FontAwesomeIcon icon="arrow-right" />
                                        </Button>
                                    </OverlayTrigger>
                                    {/* Delete theme button */}
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Supprimer le thème</Tooltip>}>
                                        <Button variant="danger" size="sm" onClick={() => {
                                            if (confirm("Voulez-vous vraiment supprimer ce thème ?")) {
                                                const updatedThemes = themes.filter((t: Theme) => t.id !== theme.id);
                                                setThemes(updatedThemes);
                                                setActiveTab(updatedThemes[updatedThemes.length - 1].id);
                                            }
                                        }}>
                                            <FontAwesomeIcon icon="trash" />
                                        </Button>
                                    </OverlayTrigger>
                                </InputGroup>

                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du thème</Tooltip>}>
                                        <InputGroup.Text id="theme_name">
                                            <FontAwesomeIcon icon="font" />
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Titre du chapitre"
                                        aria-label="Titre du chapitre"
                                        aria-describedby="theme_name"
                                        value={theme.title}
                                        data-path={"themes[" + theme.id + "].title"}
                                        onChange={(e) => {
                                            const updatedThemes = themes.map((t: Theme) => {
                                                if (t.id === theme.id) {
                                                    return { ...t, title: e.target.value };
                                                }
                                                return t;
                                            });
                                            setThemes(updatedThemes);
                                        }}
                                    />
                                </InputGroup>
                                <Row xs={2}>
                                    <Col>
                                        <InputGroup className="mb-3" size="sm">
                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du thème</Tooltip>}>
                                                <InputGroup.Text id="theme_icon">
                                                    <FontAwesomeIcon icon="star" />
                                                </InputGroup.Text>
                                            </OverlayTrigger>
                                            <IconSelector value={theme.icon} setValue={(icon: string) => {
                                                const updatedThemes = themes.map((t: Theme) => {
                                                    if (t.id === theme.id) {
                                                        return { ...t, icon: icon };
                                                    }
                                                    return t;
                                                }
                                                );
                                                setThemes(updatedThemes);
                                            }
                                            } />
                                        </InputGroup>
                                    </Col>
                                    <Col>
                                        <InputGroup className="mb-3" size="sm">
                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Couleur du thème</Tooltip>}>
                                                <InputGroup.Text id="theme_color">
                                                    <FontAwesomeIcon icon="palette" />
                                                </InputGroup.Text>
                                            </OverlayTrigger>
                                            <Form.Control
                                                size="sm"
                                                type="color"
                                                value={theme.color}
                                                aria-label="Couleur"
                                                aria-describedby="theme_color"
                                                data-path={"themes[" + theme.id + "].color"}
                                                onChange={(e) => {
                                                    const updatedThemes = themes.map((t: Theme) => {
                                                        if (t.id === theme.id) {
                                                            return { ...t, color: e.target.value };
                                                        }
                                                        return t;
                                                    });
                                                    setThemes(updatedThemes);
                                                }}
                                            />
                                        </InputGroup>
                                    </Col>
                                </Row>

                                <hr />

                                <h4>Chapitres</h4>

                                {/* Chapter list */}
                                {theme.chapters && theme.chapters.map((chapter: Chapter) => (
                                    <>
                                        <div className="d-flex flex-column flex-md-row justify-content-between align-items-center">
                                            <span className="w-100">
                                                <CollapsibleAlert key={"theme_" + theme.id + "_chapter_" + chapter.id} title={chapter.name} variant="secondary">
                                                    <div className="mt-3">
                                                        {/* Id */}
                                                        <InputGroup size="sm" className="mb-3">
                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Identifiant unique du chapitre qui constitue le début du nom des fichiers associés</Tooltip>}>
                                                                <InputGroup.Text id="chapter_id">
                                                                    #
                                                                </InputGroup.Text>
                                                            </OverlayTrigger>
                                                            <Form.Control
                                                                placeholder="Identifiant"
                                                                aria-label="Identifiant"
                                                                aria-describedby="chapter_id"
                                                                value={chapter.name}
                                                                data-path={"themes[" + theme.id + "].chapters[" + chapter.id + "].name"}
                                                                onChange={(e) => {
                                                                    const updatedThemes = themes.map((t: Theme) => {
                                                                        if (t.id === theme.id) {
                                                                            const updatedChapters = t.chapters.map((c: Chapter) => {
                                                                                if (c.id === chapter.id) {
                                                                                    return { ...c, name: e.target.value };
                                                                                }
                                                                                return c;
                                                                            });
                                                                            return { ...t, chapters: updatedChapters };
                                                                        }
                                                                        return t;
                                                                    });
                                                                    setThemes(updatedThemes);
                                                                }}
                                                            />


                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Supprimer le chapitre</Tooltip>}>
                                                                <Button variant="danger" size="sm" onClick={() => {
                                                                    if (confirm("Voulez-vous vraiment supprimer ce chapitre ?")) {
                                                                        const updatedThemes = themes.map((t: Theme) => {
                                                                            if (t.id === theme.id) {
                                                                                const updatedChapters = t.chapters.filter((c: Chapter) => c.id !== chapter.id);
                                                                                return { ...t, chapters: updatedChapters };
                                                                            }
                                                                            return t;
                                                                        });
                                                                        setThemes(updatedThemes);
                                                                    }
                                                                }}>
                                                                    <FontAwesomeIcon icon="trash" />
                                                                </Button>
                                                            </OverlayTrigger>

                                                        </InputGroup>

                                                        {/* Title */}
                                                        <InputGroup size="sm">
                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du chapitre</Tooltip>}>
                                                                <InputGroup.Text id="chapter_name">
                                                                    <FontAwesomeIcon icon="font" />
                                                                </InputGroup.Text>
                                                            </OverlayTrigger>
                                                            <Form.Control
                                                                placeholder="Titre du chapitre"
                                                                aria-label="Titre du chapitre"
                                                                aria-describedby="chapter_name"
                                                                value={chapter.title}
                                                                data-path={"themes[" + theme.id + "].chapters[" + chapter.id + "].title"}
                                                                onChange={(e) => {
                                                                    const updatedThemes = themes.map((t: Theme) => {
                                                                        if (t.id === theme.id) {
                                                                            const updatedChapters = t.chapters.map((c: Chapter) => {
                                                                                if (c.id === chapter.id) {
                                                                                    return { ...c, title: e.target.value };
                                                                                }
                                                                                return c;
                                                                            });
                                                                            return { ...t, chapters: updatedChapters };
                                                                        }
                                                                        return t;
                                                                    });
                                                                    setThemes(updatedThemes);
                                                                }}
                                                            />
                                                        </InputGroup>

                                                        {/* Code Mirror additional content */}
                                                        <div className='my-3'>
                                                            <b>Contenu additionnel</b>
                                                        </div>
                                                        <CodeMirror
                                                            ref={editorRef}
                                                            className='mt-3'
                                                            value={typeof chapter.additional_content === 'string' ? chapter.additional_content : ''}
                                                            options={{
                                                                mode: 'xml',
                                                                theme: 'material',
                                                                lineNumbers: true
                                                            }}
                                                            onBeforeChange={(_editor, _data, value) => {
                                                                const updatedThemes = themes.map((t) => {
                                                                    if (t.id === theme.id) {
                                                                        const updatedChapters = t.chapters.map((c) => {
                                                                            if (c.id === chapter.id) {
                                                                                return { ...c, additional_content: value };
                                                                            }
                                                                            return c;
                                                                        });
                                                                        return { ...t, chapters: updatedChapters };
                                                                    }
                                                                    return t;
                                                                });
                                                                setThemes(updatedThemes);
                                                            }}
                                                            onChange={(_editor, _data, _value) => {
                                                                // Handle change if needed
                                                            }}
                                                        />
                                                    </div>
                                                </CollapsibleAlert>
                                            </span>
                                            <div className="d-flex justify-content-end mb-3 ms-3">
                                                {/* Up and down buttons */}
                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le chapitre vers le haut</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const chapterIndex = theme.chapters.findIndex(c => c.id === chapter.id);
                                                        if (chapterIndex > 0) {
                                                            const updatedThemes = themes.map((t) => {
                                                                if (t.id === theme.id) {
                                                                    const updatedChapters = [...t.chapters];
                                                                    const currentIndex = updatedChapters.findIndex(c => c.id === chapter.id);
                                                                    if (currentIndex > 0) {
                                                                        const temp = updatedChapters[currentIndex - 1];
                                                                        updatedChapters[currentIndex - 1] = updatedChapters[currentIndex];
                                                                        updatedChapters[currentIndex] = temp;
                                                                    }
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }
                                                    }}>
                                                        <FontAwesomeIcon icon="arrow-up" />
                                                    </Button>
                                                </OverlayTrigger>

                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le chapitre vers le bas</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const chapterIndex = theme.chapters.findIndex(c => c.id === chapter.id);
                                                        if (chapterIndex < theme.chapters.length - 1) {
                                                            const updatedThemes = themes.map((t) => {
                                                                if (t.id === theme.id) {
                                                                    const updatedChapters = [...t.chapters];
                                                                    const currentIndex = updatedChapters.findIndex(c => c.id === chapter.id);
                                                                    if (currentIndex < updatedChapters.length - 1) {
                                                                        const temp = updatedChapters[currentIndex + 1];
                                                                        updatedChapters[currentIndex + 1] = updatedChapters[currentIndex];
                                                                        updatedChapters[currentIndex] = temp;
                                                                    }
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }

                                                    }}>
                                                        <FontAwesomeIcon icon="arrow-down" />
                                                    </Button>
                                                </OverlayTrigger>
                                                {/* Move the chapter to the next/previous theme */}
                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le chapitre vers le thème précédent</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const currentIndex = themes.findIndex(t => t.id === theme.id);
                                                        if (currentIndex !== -1 && currentIndex > 0) {
                                                            const previousIndex = currentIndex - 1;
                                                            const updatedThemes = themes.map((t: Theme, index: number) => {
                                                                if (index === currentIndex) {
                                                                    const updatedChapters = t.chapters.filter((c: Chapter) => c.id !== chapter.id);
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                if (index === previousIndex) {
                                                                    return { ...t, chapters: [...t.chapters, chapter] };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }

                                                    }}>
                                                        <FontAwesomeIcon icon="arrow-left" />
                                                    </Button>
                                                </OverlayTrigger>

                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer le chapitre vers le thème suivant</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const currentIndex = themes.findIndex(t => t.id === theme.id);
                                                        if (currentIndex !== -1 && currentIndex < themes.length - 1) {
                                                            const nextIndex = currentIndex + 1;
                                                            const updatedThemes = themes.map((t: Theme, index: number) => {
                                                                if (index === currentIndex) {
                                                                    const updatedChapters = t.chapters.filter((c: Chapter) => c.id !== chapter.id);
                                                                    return { ...t, chapters: updatedChapters };
                                                                }
                                                                if (index === nextIndex) {
                                                                    return { ...t, chapters: [...t.chapters, chapter] };
                                                                }
                                                                return t;
                                                            });
                                                            setThemes(updatedThemes);
                                                        }
                                                    }}>
                                                        <FontAwesomeIcon icon="arrow-right" />
                                                    </Button>
                                                </OverlayTrigger>

                                            </div>
                                        </div>
                                    </>
                                ))}

                                {/* Add chapter button */}
                                <Button variant="success" size="sm" className="mt-3" onClick={() => {
                                    var newThemes = addChapter(theme.id, themes);
                                    const newThemesCopy = [...newThemes];
                                    setThemes(newThemesCopy);
                                }}>
                                    <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un chapitre
                                </Button>
                            </div>
                        </div>
                    ))}
                </div>

            </Card.Body>
        </Card>
    )
}


// Add a new theme to the themes list
function addTheme(themes: Theme[]) {
    const maxThemeId = themes.reduce((max, theme) => (theme.id > max ? theme.id : max), 0);
    themes.push({
        id: maxThemeId + 1,
        name: "theme",
        title: "",
        icon: "",
        color: "",
        chapters: []
    });

    return themes;
}

// Add a new chapter to the chapters list
function addChapter(themeId: Number, themes: Theme[]) {
    themes.map((theme: Theme) => {
        if (theme.id === themeId) {
            // Find the max chapter id
            const maxChapterId = theme.chapters.reduce((max, chapter) => (chapter.id > max ? chapter.id : max), 0);
            theme.chapters.push({
                id: maxChapterId + 1,
                name: "",
                title: "",
                files: [],
                additional_content: "",
                sources: {} as File
            });
        }
    })
    return themes;
}

export default ThemesEditorContent;