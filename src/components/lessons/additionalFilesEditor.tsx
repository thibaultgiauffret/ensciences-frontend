// Components
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { CollapsibleAlert } from '../collapsibleAlert';
import IconSelector from '../iconSelector';
import { Button } from 'react-bootstrap';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

// CSS
import styles from '../css/cards.module.css';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState, useRef } from 'react';

// Additional files editor card (modal content)
function AdditionalFilesEditorContent({ version, json, setJson, setModified }: { version: string, json: Lessons, setJson: any, setModified: any }) {

    const isFirstRender = useRef(true);

    // Filter the json.version and keep only the version we want to edit
    const versionData = json.versions.filter((v: Version) => v.version === version);
    const [sections, setSections] = useState(versionData[0].additional_files);
    const [availableAdditionalFiles] = useState(versionData[0].availableAdditionalFiles);

    // Send the updated sections to the parent component when the sections are updated
    useEffect(() => {
        // Do nothing on the first render
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        setJson({
            ...json, versions: json.versions.map((v: Version) => {
                if (v.version === version) {
                    return { ...v, additional_files: sections };
                }
                return v;
            })
        });
        setModified(true);
    }, [sections]);

    // Handle the tab click
    const [activeTab, setActiveTab] = useState(sections.length > 0 ? sections[0].id : 0);
    const handleTabClick = (tabId: number) => {
        setActiveTab(tabId);
    };

    // Add a new section
    const handleAddSection = () => {
        // Add a new section (with id = last section id + 1)
        const newSections = addSection([...sections]);
        let newSectionId = 0;
        // Set the active tab to the new section
        if (newSections.length > 0) {
            newSectionId = newSections[newSections.length - 1].id;
        }
        setSections(newSections);
        setActiveTab(newSectionId);
    };

    return (
        <Card className={styles.card + " mb-3"} key={sections.length}>
            <Card.Header>
                <ul className="nav nav-tabs card-header-tabs">
                    {sections && sections.map((section: AdditionalFile) => (
                        <li key={section.id + '_section_nav'} className="nav-item">
                            <a onClick={() => {
                                handleTabClick(section.id)
                            }} className={"nav-link " + (
                                activeTab === section.id ? 'active' : ''
                            )}>
                                {section.section}
                            </a>
                        </li>
                    ))}

                    {/* Add section button */}
                    <li className="nav-item">
                        <a className='nav-link' onClick={handleAddSection}>
                            <FontAwesomeIcon icon="plus" />
                        </a>
                    </li>
                </ul>
            </Card.Header>

            {/* Card Body */}
            <Card.Body>
                <div className="tab-content">
                    {sections && sections.map((section: AdditionalFile) => (
                        <div key={section.id + '_section_tab'} className={"tab-pane " + (
                            activeTab === section.id ? 'active' : ''
                        )} id={section.section}>

                            <div key={section.id + "_section_content"}>
                                <h4>Paramétrage de la section</h4>
                                {/* Section parameters */}
                                <InputGroup className="mb-3" size="sm">
                                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre de la section.</Tooltip>}>
                                        <InputGroup.Text id="theme_id">
                                            <FontAwesomeIcon icon="font" />
                                        </InputGroup.Text>
                                    </OverlayTrigger>
                                    <Form.Control
                                        placeholder="Identifiant"
                                        aria-label="Identifiant"
                                        aria-describedby="section_id"
                                        value={section.section}
                                        onChange={(e) => {
                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                if (s.id === section.id) {
                                                    return { ...s, section: e.target.value };
                                                }
                                                return s;
                                            });
                                            setSections(updatedSections);
                                        }}
                                    />
                                    {/* Move to the left */}
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        const sectionIndex = sections.findIndex((s: AdditionalFile) => s.id === section.id);
                                        if (sectionIndex > 0) {
                                            const updatedSections = [...sections];
                                            const temp = updatedSections[sectionIndex - 1];
                                            updatedSections[sectionIndex - 1] = updatedSections[sectionIndex];
                                            updatedSections[sectionIndex] = temp;
                                            setSections(updatedSections);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="arrow-left" />
                                    </Button>
                                    {/* Move to the right */}
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        const sectionIndex = sections.findIndex((s: AdditionalFile) => s.id === section.id);
                                        if (sectionIndex < sections.length - 1) {
                                            const updatedSections = [...sections];
                                            const temp = updatedSections[sectionIndex + 1];
                                            updatedSections[sectionIndex + 1] = updatedSections[sectionIndex];
                                            updatedSections[sectionIndex] = temp;
                                            setSections(updatedSections);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="arrow-right" />
                                    </Button>
                                    {/* Delete section button */}
                                    <Button variant="danger" size="sm" onClick={() => {
                                        if (confirm("Voulez-vous vraiment supprimer cette section ?")) {
                                            const updatedSections = sections.filter((s: AdditionalFile) => s.id !== section.id);
                                            setSections(updatedSections);
                                            setActiveTab(updatedSections[updatedSections.length - 1].id);
                                        }
                                    }}>
                                        <FontAwesomeIcon icon="trash" />
                                    </Button>
                                </InputGroup>

                                <hr />

                                <h4>Fichiers</h4>

                                {/* File list */}
                                {section.files && section.files.map((file: AdditionalFileFiles) => (
                                    <>
                                        <div className="d-flex flex-column flex-md-row justify-content-between align-items-center">
                                            <span className="w-100">
                                                <CollapsibleAlert key={"section_" + section.id + "_file_" + file.id} title={file.title} variant="secondary">
                                                    <div className="mt-3">
                                                        {/* Title */}
                                                        <InputGroup size="sm" className="mb-3">
                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Titre du fichier</Tooltip>}>
                                                                <InputGroup.Text id="file_title">
                                                                    <FontAwesomeIcon icon="font" />
                                                                </InputGroup.Text>
                                                            </OverlayTrigger>
                                                            <Form.Control
                                                                placeholder="Titre"
                                                                aria-label="Titre"
                                                                aria-describedby="file_title"
                                                                value={file.title}
                                                                onChange={(e) => {
                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                        if (s.id === section.id) {
                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                if (f.id === file.id) {
                                                                                    return { ...f, title: e.target.value };
                                                                                }
                                                                                return f;
                                                                            });
                                                                            return { ...s, files: updatedFiles };
                                                                        }
                                                                        return s;
                                                                    });
                                                                    setSections(updatedSections);
                                                                }}
                                                            />
                                                            <Button variant="danger" size="sm" onClick={() => {
                                                                if (confirm("Voulez-vous vraiment supprimer ce fichier ?")) {
                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                        if (s.id === section.id) {
                                                                            const updatedFiles = s.files.filter((f: AdditionalFileFiles) => f.id !== file.id);
                                                                            return { ...s, files: updatedFiles };
                                                                        }
                                                                        return s;
                                                                    });
                                                                    setSections(updatedSections);
                                                                }
                                                            }}>
                                                                <FontAwesomeIcon icon="trash" />
                                                            </Button>
                                                        </InputGroup>
                                                        {/* Description */}
                                                        <InputGroup size="sm" className="mb-3">
                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Description du fichier</Tooltip>}>
                                                                <InputGroup.Text id="file_description">
                                                                    <FontAwesomeIcon icon="info" />
                                                                </InputGroup.Text>
                                                            </OverlayTrigger>
                                                            <Form.Control
                                                                placeholder="Description"
                                                                aria-label="Description"
                                                                aria-describedby="file_description"
                                                                value={file.description}
                                                                onChange={(e) => {
                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                        if (s.id === section.id) {
                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                if (f.id === file.id) {
                                                                                    return { ...f, description: e.target.value };
                                                                                }
                                                                                return f;
                                                                            });
                                                                            return { ...s, files: updatedFiles };
                                                                        }
                                                                        return s;
                                                                    });
                                                                    setSections(updatedSections);
                                                                }}
                                                            />
                                                        </InputGroup>
                                                        {/* Icon */}
                                                        <InputGroup size="sm" className="mb-3">
                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du fichier</Tooltip>}>
                                                                <InputGroup.Text id="file_icon">
                                                                    <FontAwesomeIcon icon="star" />
                                                                </InputGroup.Text>
                                                            </OverlayTrigger>
                                                            <IconSelector value={file.icon} setValue={(icon: string) => {
                                                                const updatedSections = sections.map((s: AdditionalFile) => {
                                                                    if (s.id === section.id) {
                                                                        const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                            if (f.id === file.id) {
                                                                                return { ...f, icon: icon };
                                                                            }
                                                                            return f;
                                                                        });
                                                                        return { ...s, files: updatedFiles };
                                                                    }
                                                                    return s;
                                                                }
                                                                );
                                                                setSections(updatedSections);
                                                            }
                                                            } />
                                                        </InputGroup>

                                                        {/* Buttons */}
                                                        <h5>Boutons</h5>
                                                        <div className="mt-3">
                                                            {file.buttons && file.buttons.map((button: Button) => (
                                                                <CollapsibleAlert key={"section_" + section.id + "_file_" + file.id + "_button_" + button.id} variant="secondary" title={button.title}>
                                                                    <div className="mt-3">
                                                                        {/* Title */}
                                                                        <InputGroup size="sm" className="mb-3">
                                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Texte du bouton</Tooltip>}>
                                                                                <InputGroup.Text id="button_text">
                                                                                    <FontAwesomeIcon icon="font" />
                                                                                </InputGroup.Text>
                                                                            </OverlayTrigger>
                                                                            <Form.Control
                                                                                placeholder="Texte"
                                                                                aria-label="Texte"
                                                                                aria-describedby="button_text"
                                                                                value={button.title}
                                                                                onChange={(e) => {
                                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                        if (s.id === section.id) {
                                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                                if (f.id === file.id) {
                                                                                                    const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                        if (b.id === button.id) {
                                                                                                            return { ...b, title: e.target.value };
                                                                                                        }
                                                                                                        return b;
                                                                                                    });
                                                                                                    return { ...f, buttons: updatedButtons };
                                                                                                }
                                                                                                return f;
                                                                                            });
                                                                                            return { ...s, files: updatedFiles };
                                                                                        }
                                                                                        return s;
                                                                                    });
                                                                                    setSections(updatedSections);
                                                                                }}
                                                                            />
                                                                            <Button variant="danger" size="sm" onClick={() => {
                                                                                if (confirm("Voulez-vous vraiment supprimer ce bouton ?")) {
                                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                        if (s.id === section.id) {
                                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                                if (f.id === file.id) {
                                                                                                    const updatedButtons = f.buttons.filter((b: Button) => b.id !== button.id);
                                                                                                    return { ...f, buttons: updatedButtons };
                                                                                                }
                                                                                                return f;
                                                                                            });
                                                                                            return { ...s, files: updatedFiles };
                                                                                        }
                                                                                        return s;
                                                                                    });
                                                                                    setSections(updatedSections);
                                                                                }
                                                                            }}>
                                                                                <FontAwesomeIcon icon="trash" />
                                                                            </Button>
                                                                        </InputGroup>
                                                                        {/* Icon */}
                                                                        <InputGroup size="sm" className="mb-3">
                                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Icône du bouton</Tooltip>}>
                                                                                <InputGroup.Text id="button_icon">
                                                                                    <FontAwesomeIcon icon="star" />
                                                                                </InputGroup.Text>
                                                                            </OverlayTrigger>
                                                                            <IconSelector value={button.icon} setValue={(icon: string) => {
                                                                                const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                    if (s.id === section.id) {
                                                                                        const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                            if (f.id === file.id) {
                                                                                                const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                    if (b.id === button.id) {
                                                                                                        return { ...b, icon: icon };
                                                                                                    }
                                                                                                    return b;
                                                                                                });
                                                                                                return { ...f, buttons: updatedButtons };
                                                                                            }
                                                                                            return f;
                                                                                        });
                                                                                        return { ...s, files: updatedFiles };
                                                                                    }
                                                                                    return s;
                                                                                }
                                                                                );
                                                                                setSections(updatedSections);
                                                                            }} />
                                                                        </InputGroup>
                                                                        {/* Color select */}
                                                                        <InputGroup size="sm" className="mb-3">
                                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Couleur du bouton</Tooltip>}>
                                                                                <InputGroup.Text id="button_color">
                                                                                    <FontAwesomeIcon icon="palette" />
                                                                                </InputGroup.Text>
                                                                            </OverlayTrigger>
                                                                            <Form.Select
                                                                                aria-label="Couleur"
                                                                                aria-describedby="button_color"
                                                                                value={button.color}
                                                                                onChange={(e) => {
                                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                        if (s.id === section.id) {
                                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                                if (f.id === file.id) {
                                                                                                    const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                        if (b.id === button.id) {
                                                                                                            return { ...b, color: e.target.value };
                                                                                                        }
                                                                                                        return b;
                                                                                                    });
                                                                                                    return { ...f, buttons: updatedButtons };
                                                                                                }
                                                                                                return f;
                                                                                            });
                                                                                            return { ...s, files: updatedFiles };
                                                                                        }
                                                                                        return s;
                                                                                    });
                                                                                    setSections(updatedSections);
                                                                                }}
                                                                            >
                                                                                <option value="primary">
                                                                                    <span className="bg-primary">Primaire</span></option>
                                                                                <option value="secondary">
                                                                                    <span className="text-secondary">Secondaire</span></option>
                                                                                <option value="success">
                                                                                    <span className="text-success">Succès</span></option>
                                                                                <option value="danger">
                                                                                    <span className="text-danger">Danger</span></option>
                                                                                <option value="warning">
                                                                                    <span className="text-warning">Attention</span></option>
                                                                                <option value="info">
                                                                                    <span className="text-info">Info</span></option>
                                                                                <option value="light">
                                                                                    <span className="text-light">Clair</span></option>
                                                                                <option value="dark">
                                                                                    <span className="text-dark">Sombre</span></option>
                                                                            </Form.Select>
                                                                        </InputGroup>
                                                                        {/* File selection select */}
                                                                        <InputGroup size="sm" className="mb-3">
                                                                            <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Fichier associé au bouton</Tooltip>}>
                                                                                <InputGroup.Text id="button_file">
                                                                                    <FontAwesomeIcon icon="file" />
                                                                                </InputGroup.Text>
                                                                            </OverlayTrigger>
                                                                            <Form.Select
                                                                                aria-label="Fichier"
                                                                                aria-describedby="button_file"
                                                                                value={button.name}
                                                                                onChange={(e) => {
                                                                                    const updatedSections = sections.map((s: AdditionalFile) => {
                                                                                        if (s.id === section.id) {
                                                                                            const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                                                if (f.id === file.id) {
                                                                                                    const updatedButtons = f.buttons.map((b: Button) => {
                                                                                                        if (b.id === button.id) {
                                                                                                            return {
                                                                                                                ...b, name: e.target.options[e.target.selectedIndex].text,
                                                                                                                dbid: parseInt(e.target.options[e.target.selectedIndex].getAttribute('data-id') || '0')
                                                                                                            };
                                                                                                        }
                                                                                                        return b;
                                                                                                    });
                                                                                                    return { ...f, buttons: updatedButtons };
                                                                                                }
                                                                                                return f;
                                                                                            });
                                                                                            return { ...s, files: updatedFiles };
                                                                                        }
                                                                                        return s;
                                                                                    });
                                                                                    setSections(updatedSections);
                                                                                }}
                                                                            >
                                                                                <option value="">Sélectionner un fichier</option>
                                                                                {availableAdditionalFiles && availableAdditionalFiles.map((s: any) => (
                                                                                    <option value={s.name} data-id={s.id} key={s.name}>{s.name}</option>

                                                                                ))}
                                                                            </Form.Select>
                                                                        </InputGroup>
                                                                    </div>
                                                                </CollapsibleAlert>
                                                            ))}
                                                            <Button variant="success" size="sm" className="m-1" onClick={() => {
                                                                const updatedSections = sections.map((s: AdditionalFile) => {
                                                                    if (s.id === section.id) {
                                                                        const updatedFiles = s.files.map((f: AdditionalFileFiles) => {
                                                                            if (f.id === file.id) {
                                                                                const newButton = {
                                                                                    id: file.buttons.length,
                                                                                    icon: "",
                                                                                    date: 0,
                                                                                    color: "",
                                                                                    name: "",
                                                                                    path: "",
                                                                                    size: 0,
                                                                                    title: "",
                                                                                    enabled: true
                                                                                };
                                                                                f.buttons.push(newButton);
                                                                                return f;
                                                                            }
                                                                            return f;
                                                                        });
                                                                        return { ...s, files: updatedFiles };
                                                                    }
                                                                    return s;
                                                                });
                                                                setSections(updatedSections);
                                                            }}>
                                                                <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un bouton
                                                            </Button>
                                                        </div>
                                                    </div>
                                                </CollapsibleAlert>
                                            </span>
                                            <div className="d-flex justify-content-end mb-3 ms-3">
                                                {/* Up and down buttons */}
                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer la ressource vers le haut</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const fileIndex = section.files.findIndex((f: AdditionalFileFiles) => f.id === file.id);
                                                        if (fileIndex !== 0) {
                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                if (s.id === section.id) {
                                                                    const updatedFiles = [...s.files];
                                                                    const currentIndex = updatedFiles.findIndex((f: AdditionalFileFiles) => f.id === file.id);
                                                                    if (currentIndex > 0) {
                                                                        const temp = updatedFiles[currentIndex - 1];
                                                                        updatedFiles[currentIndex - 1] = file;
                                                                        updatedFiles[currentIndex] = temp;
                                                                    }
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }
                                                    }
                                                    }>
                                                        <FontAwesomeIcon icon="arrow-up" />
                                                    </Button>
                                                </OverlayTrigger>

                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer la ressource vers le bas</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const fileIndex = section.files.findIndex((f: AdditionalFileFiles) => f.id === file.id);
                                                        if (fileIndex < section.files.length - 1) {
                                                            const updatedSections = sections.map((s: AdditionalFile) => {
                                                                if (s.id === section.id) {
                                                                    const updatedFiles = [...s.files];
                                                                    const currentIndex = updatedFiles.findIndex((f: AdditionalFileFiles) => f.id === file.id);
                                                                    if (currentIndex < updatedFiles.length - 1) {
                                                                        const temp = updatedFiles[currentIndex + 1];
                                                                        updatedFiles[currentIndex + 1] = file;
                                                                        updatedFiles[currentIndex] = temp;
                                                                    }
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }
                                                    }
                                                    }>
                                                        <FontAwesomeIcon icon="arrow-down" />
                                                    </Button>
                                                </OverlayTrigger>

                                                {/* Move to next/previous section button */}
                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer la ressource vers la section précédente</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const currentIndex = sections.findIndex((s: AdditionalFile) => s.id === section.id);
                                                        if (currentIndex !== -1 && currentIndex > 0) {
                                                            const previousIndex = currentIndex - 1;
                                                            const updatedSections = sections.map((s: AdditionalFile, index: number) => {
                                                                if (index === currentIndex) {
                                                                    // Remove the file from the current section
                                                                    const updatedFiles = s.files.filter((f: AdditionalFileFiles) => f.id !== file.id);
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                if (index === previousIndex) {
                                                                    // Add the file to the previous section
                                                                    return { ...s, files: [...s.files, file] };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }
                                                    }
                                                    }>
                                                        <FontAwesomeIcon icon="arrow-left" />
                                                    </Button>
                                                </OverlayTrigger>

                                                <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Déplacer la ressource vers la section suivante</Tooltip>}>
                                                    <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                                        const currentIndex = sections.findIndex((s: AdditionalFile) => s.id === section.id);
                                                        if (currentIndex !== -1 && currentIndex < sections.length - 1) {
                                                            const nextIndex = currentIndex + 1;
                                                            const updatedSections = sections.map((s: AdditionalFile, index: number) => {
                                                                if (index === currentIndex) {
                                                                    // Remove the file from the current section
                                                                    const updatedFiles = s.files.filter((f: AdditionalFileFiles) => f.id !== file.id);
                                                                    return { ...s, files: updatedFiles };
                                                                }
                                                                if (index === nextIndex) {
                                                                    // Add the file to the next section
                                                                    return { ...s, files: [...s.files, file] };
                                                                }
                                                                return s;
                                                            });
                                                            setSections(updatedSections);
                                                        }
                                                    }}>
                                                        <FontAwesomeIcon icon="arrow-right" />
                                                    </Button>
                                                </OverlayTrigger>
                                            </div>
                                        </div>
                                    </>
                                ))}

                                {/* Add file button */}
                                <Button variant="success" size="sm" className="mt-3" onClick={() => {
                                    var newSections = addFile(section.id, sections);
                                    const newSectionsCopy = [...newSections];
                                    setSections(newSectionsCopy);
                                }}>
                                    <FontAwesomeIcon icon="plus" />&nbsp;Ajouter un fichier
                                </Button>
                            </div>
                        </div>
                    ))}
                </div>

            </Card.Body>
        </Card>
    )
}


// Add a new section to the additional files
function addSection(sections: AdditionalFile[]) {
    const maxSectionId = sections.reduce((max, section) => (section.id > max ? section.id : max), 0);
    sections.push({
        id: maxSectionId + 1,
        section: "Documents supplémentaires",
        files: []
    });
    return sections;
}

// Add a new file to the additional files
function addFile(sectionId: number, sections: AdditionalFile[]) {
    sections.map((section: AdditionalFile) => {
        const maxFileId = section.files.reduce((max, file) => (file.id > max ? file.id : max), 0);
        if (section.id === sectionId) {
            section.files.push({
                id: maxFileId + 1,
                icon: "",
                title: "",
                description: "",
                buttons: []
            });
        }
    })
    return sections;
}

export default AdditionalFilesEditorContent;