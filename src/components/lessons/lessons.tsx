// Components to build the lessons page with themes, chapters and files

// Imports
import { useEffect, useState } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Tooltip from 'react-bootstrap/Tooltip';
import Table from 'react-bootstrap/Table';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { Form, InputGroup, Dropdown, Placeholder } from 'react-bootstrap';
import { CollapsibleAlert } from '../collapsibleAlert';
import { ThemeCard } from '../cards';
import { H2 } from '../sections';
import { FileCard } from '../cards';
import ScrollAnimation from "react-animate-on-scroll";
import { OverlayRestrictedMessage } from '../overlays';

import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// Return the lessons themes cards with chapters and files in a modal
function LessonsThemes({ lessons, level, levelVersion }: { lessons: any, level: string, levelVersion: string }) {
    return (
        <Row xs={1} sm={2} md={2} lg={4} id="themeCards" key={lessons.id}>
            {/* Create a col with card for each theme */}
            {lessons && lessons.themes ? (lessons.themes.map((theme: Theme) => (
                <Col className="mb-3" key={theme.name}>
                    <ThemeCard title={theme.title} icon={theme.icon as IconProp} color={theme.color} name={theme.name}>
                        {/* Create a collapsible alert for each chapter in theme.chapters */}
                        {theme.chapters ? (
                            // Test if the chapter is in single mode (only one chapter)
                            theme.chapters.length === 1 ? (
                                <>
                                    <div className="d-flex justify-content-center align-items-center text-center">
                                        {/* Create a dropdown for each file in chapter.files */}
                                        {theme.chapters[0].files ? (theme.chapters[0].files.map((file: { title: string, color: string, type: string, versions: any }) => (
                                            <FileDropdownButton title={file.title} color={file.color} type={file.type} versions={file.versions} chapter={theme.chapters[0].name}
                                                level={level} levelVersion={levelVersion}
                                                key={theme.chapters[0].name + "_" + file.title} connected={lessons.connected} />
                                        ))) : (
                                            <div>
                                                <Placeholder as="p" animation="glow" size="sm" />
                                            </div>
                                        )}
                                        {/* Add the sources */}
                                        {theme.chapters[0].sources && (
                                                <FileDropdownButton title="Sources" color="secondary" type="sources" versions={theme.chapters[0].sources.versions} chapter={theme.chapters[0].name}
                                                    level={level} levelVersion={levelVersion}
                                                    key={theme.chapters[0].name + "_sources"} connected={lessons.connected} outline={true} />
                                        )}
                                    </div>

                                    {/* Render the additional content if it exists */}
                                    {theme.chapters[0].additional_content &&
                                        <div>
                                            <div className='my-3'>
                                                <b>Contenu additionnel</b>
                                            </div>
                                            {/* Render the html */}
                                            <div dangerouslySetInnerHTML={{ __html: theme.chapters[0].additional_content }} />
                                        </div>
                                    }
                                </>

                            ) : (
                                // If there are multiple chapters, create a collapsible alert for each chapter
                                theme.chapters.map((chapter: Chapter) => (
                                    <CollapsibleAlert title={chapter.title} icon="book" variant="secondary" key={chapter.title}>
                                        <div className="d-flex justify-content-center align-items-center text-center">
                                            {/* Create a dropdown for each file in chapter.files */}
                                            {chapter.files ? (chapter.files.map((file: { title: string, color: string, type: string, versions: any }) => (
                                                <>
                                                    <FileDropdownButton title={file.title} color={file.color} type={file.type} versions={file.versions} chapter={chapter.name}
                                                        level={level} levelVersion={levelVersion}
                                                        key={chapter + "_" + file.title} connected={lessons.connected} />
                                                </>
                                            ))) : (
                                                <div>
                                                    <Placeholder as="p" animation="glow" size="sm" />
                                                </div>
                                            )}

                                            {/* Add the sources */}
                                            {chapter.sources && (
                                                <FileDropdownButton title="Sources" color="secondary" type="sources" versions={chapter.sources.versions} chapter={chapter.name}
                                                    level={level} levelVersion={levelVersion}
                                                    key={chapter + "_sources"} connected={lessons.connected} outline={true} />
                                            )}
                                        </div>

                                        {/* Render the additional content if it exists */}
                                        {chapter.additional_content &&
                                            <div>
                                                <div className='my-3'>
                                                    <b>Contenu additionnel</b>
                                                </div>
                                                {/* Render the html */}
                                                <div dangerouslySetInnerHTML={{ __html: chapter.additional_content }} />
                                            </div>
                                        }
                                    </CollapsibleAlert>
                                ))
                            )

                        ) : (
                            <div>
                                <Placeholder as="p" animation="glow" size="sm" />
                            </div>
                        )}
                    </ThemeCard>
                </Col>
            ))) : (
                // If there are no lessons, display a placeholder card
                <>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                    <Col className="mb-3">
                        <ThemeCard title="" icon="question-circle" color="" name="">
                            Test
                        </ThemeCard>
                    </Col>
                </>
            )}
        </Row>
    );
}

// Return the lessons files table with each line for each chapter
function LessonsFileTable({ lessons, level, levelVersion }: { lessons: any, level: string, levelVersion: string }) {
    console.log(lessons);
    // Filter the table when the user types in the search bar
    const [search, setSearch] = useState('');
    const [filteredData, setFilteredData] = useState(lessons);
    useEffect(() => {
        if (search === '') {
            setFilteredData(lessons);
        } else {
            //  Filter by chapter.title and chapter.name
            const filtered = {
                connected: lessons.connected,
                themes: lessons.themes.map((theme: { title: string, chapters: any }) => {
                    const filteredChapters = theme.chapters.filter((chapter: { title: string, name: string }) => {
                        const searchLower = search.toLowerCase();
                        return chapter.title.toLowerCase().includes(searchLower) || chapter.name.toLowerCase().includes(searchLower) || theme.title.toLowerCase().includes(searchLower);
                    });
                    return { ...theme, chapters: filteredChapters };
                }).filter((theme: { chapters: any }) => theme.chapters.length > 0)
            };
            setFilteredData(filtered);
        }
    }, [search, lessons]);


    return (
        <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
            {/* Search bar */}
            <InputGroup className="mb-3" id="searchFiles">
                <InputGroup.Text id="searchIcon">
                    <FontAwesomeIcon icon="search" />
                </InputGroup.Text>
                <Form.Control
                    id="searchInput"
                    placeholder="Rechercher un document"
                    aria-label="Recherche"
                    aria-describedby="searchIcon"
                    onChange={(e) => {
                        setSearch(e.target.value);
                    }
                    }
                />
            </InputGroup>

            {/* Table with the files */}
            <Table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th className='text-center' style={{ width: '50px' }}>Thème</th>
                        <th className='text-center' style={{ width: "40%" }}>Séquence</th>
                        <th className='text-center' style={{ width: "100%" }}>Documents</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Create a row for each chapter */}
                    {filteredData && filteredData.themes ? (filteredData.themes.map((theme: { title: string; icon: IconProp; color: string; name: string, chapters: any }) => (
                        theme.chapters && (theme.chapters.map((chapter: { title: string, name: string, files: any, sources: any }) => (
                            <tr key={chapter.name}
                                className='align-middle'>
                                <td className='text-center'>
                                    <FontAwesomeIcon icon={theme.icon} style={{ color: theme.color, fontSize: '1.5rem' }} />
                                </td>
                                <td style={{ textAlign: 'left' }}>{chapter.title}</td>
                                <td className='text-center'>
                                    {chapter.files && (chapter.files.map((file: { title: string, color: string, type: string, versions: any }) => (
                                        // Add a dropdown button for each file in chapter.files
                                        // fileDropdownButton({ title: file.title, color: file.color, type: file.type, versions: file.versions, chapter: chapter.name, connected: lessons.connected })
                                        <FileDropdownButton title={file.title} color={file.color} type={file.type} versions={file.versions} chapter={chapter.name} connected={filteredData.connected}
                                            level={level} levelVersion={levelVersion}
                                            key={chapter + "_" + file.title} outline={true} />
                                    )))}
                                    {/* If sources is set, add the button */}
                                    {chapter.sources && (
                                        <FileDropdownButton title="Sources" color="secondary" type="sources" versions={chapter.sources.versions} chapter={chapter.name} connected={filteredData.connected}
                                            level={level} levelVersion={levelVersion}
                                            key={chapter + "_sources"} outline={true}/>
                                    )}
                                </td>
                            </tr>
                        )))))) : (
                        <>
                            <tr className='align-middle'>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={2} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={6} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder.Button variant="primary" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="success" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="info" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                </td>
                            </tr>
                            <tr className='align-middle'>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={2} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder animation="glow">
                                        <Placeholder xs={10} /><br />
                                    </Placeholder>
                                </td>
                                <td className='text-center'>
                                    <Placeholder.Button variant="primary" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="success" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                    <Placeholder.Button variant="warning" xs={6} sm={4} md={3} className='btn-sm m-1' />
                                </td>
                            </tr>
                        </>
                    )}
                </tbody>
            </Table>
        </ScrollAnimation>
    );
}

// Return the additional files section with cards for each section
function LessonsAdditionalFiles({ lessons }: { lessons: any }) {
    return (
        // For each section in additionalFiles, add a <H2> and add the files cards
        <>
            {lessons && lessons.additional_files ? (lessons.additional_files.map((section: { section: string, files: any }) => (
                <div key={section.section}>
                    <H2>{section.section}</H2>
                    {section.files ? (section.files.map((file: {
                        title: string, icon: IconProp,
                        description: string, buttons: any
                    }) => (
                        <FileCard id={file.title} title={file.title} icon={file.icon} description={file.description} buttons={file.buttons} key={file.title} connected={lessons.connected} />
                    ))) : (
                        <div>
                            <Placeholder as="p" animation="glow" size="sm" />
                        </div>
                    )}
                </div>
            ))) : (
                <>
                    <H2>Documents supplémentaires</H2>
                    <FileCard id="" title="" icon="question-circle" description="" buttons={[]} connected={lessons.connected} />
                </>
            )}
        </>
    );
}



// Return a dropdown button for each file type (lessons, exercises, ...). The dropdown contains the different versions of this file (teacher, student, ...)
function FileDropdownButton({ title, color, type, versions, chapter, connected, level, levelVersion, outline }: { title: string, color: string, type: string, versions: any, chapter: string, connected?: boolean, level: string, levelVersion: string, outline?: boolean }) {
    console.log(title, color, type, versions, chapter, connected, level, levelVersion, outline);
    if (!versions || versions.length === 0) {
        return
    }
    else if (versions.length === 1) {
        if (type === "sources") {
            if (connected) {
                if (versions[0].authorized) {
                    // Return a button if there is only one version
                    return (
                        <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Sources</Tooltip>}>
                            <a href={"/view?direct=cours/" + level + "/" + levelVersion + "/sources/" + chapter + "." + versions[0].ext} target="_blank" rel="noreferrer" className={"btn btn-sm m-1 btn-" + (outline ? "outline-" : "") + color}>
                                <FontAwesomeIcon icon="box-open" />
                            </a>
                        </OverlayTrigger>
                    )
                } else {
                    return (
                        <OverlayRestrictedMessage connected={connected}>
                            <a href={"/contact?request=demande_acces&additionalInfo=Fichier : " + chapter + "." + versions[0].ext + ", Niveau : " + level + ", Version : " + levelVersion} className="disabled">
                                <a className={"btn btn-sm m-1 btn-" + (outline ? "outline-" : "") + color + " disabled"}>
                                    <FontAwesomeIcon icon="box-open" />
                                </a>
                            </a>
                        </OverlayRestrictedMessage>
                    )
                }
            } else {
                return (
                    <OverlayRestrictedMessage connected={connected}>
                        <a href={"/login?notification=warning&message=Veuillez vous connecter pour accéder au document."} className="disabled">
                            <a className={"btn btn-sm m-1 btn-" + (outline ? "outline-" : "") + color + " disabled"}>
                                <FontAwesomeIcon icon="box-open" />
                            </a>
                        </a>
                    </OverlayRestrictedMessage>
                )
            }
        } else {
            // Return a button if there is only one version
            return (
                <a href={"/view?level=" + level + "&version=" + levelVersion + "&file=" + chapter + "_" + type + "-" + versions[0].audience + "." + versions[0].ext} target="_blank" rel="noreferrer" className={"btn btn-sm m-1 btn-" + (outline ? "outline-" : "") + color}>
                    <span className="fw-bold">{title}</span>
                </a>
            )
        }
    } else {
        // Return a dropdown if there are multiple versions
        return (
            <Dropdown className='d-inline-block'>
                <Dropdown.Toggle variant={(outline ? "outline-" : "") + color} id="dropdown-basic" className='m-1 btn-sm'>
                    <span className="fw-bold">{title}</span>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {versions ? (versions.map((version: { title: string, audience: string, ext: string, authorized: boolean }) =>
                        version.authorized ? (
                            <Dropdown.Item key={chapter + "_" + title + "_" + version.title} href={"/view?level=" + level + "&version=" + levelVersion + "&file=" + chapter + "_" + type + "-" + version.audience + "." + version.ext} style={{ color: "var(--bs-body)" }} target='_blank'>
                                {version.title}
                            </Dropdown.Item>
                        ) : (
                            <OverlayRestrictedMessage connected={connected} key={chapter + "_" + title + "_" + version.title}>
                                <span className="d-inline-block w-100">
                                    <Dropdown.Item key={version.title} style={{ color: "var(--bs-secondary)" }} href={connected ? ("/contact?request=demande_acces&additionalInfo= Fichier : " + chapter + "_" + type + "-" + version.audience + "." + version.ext
                                        + ", Niveau : " + level + ", Version : " + levelVersion
                                    ) : "/login?notification=warning&message=Veuillez vous connecter pour accéder au document."} target='_blank'>
                                        <div style={{
                                            display: 'flex', justifyContent: 'space-between', alignItems: 'center',
                                            width: '100%'
                                        }}>
                                            <span>{version.title}</span>
                                            <FontAwesomeIcon icon="lock" style={{ color: "var(--bs-secondary)" }} />
                                        </div>
                                    </Dropdown.Item>
                                </span>
                            </OverlayRestrictedMessage>
                        ))
                    ) : (
                        <div>
                            <Placeholder as="p" animation="glow" size="sm" />
                        </div>
                    )}
                </Dropdown.Menu>
            </Dropdown>
        )
    }
}

export { LessonsThemes, LessonsFileTable, LessonsAdditionalFiles };