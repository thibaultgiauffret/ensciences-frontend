// Components
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Functions
import { useEffect, useState, useRef } from 'react';

function LevelEditorContent({ json, setJson, setModified }: { json: Lessons, setJson: any, setModified: any }) {

    const isFirstRender = useRef(true);

    // Get the versions
    const [lesson, setLesson] = useState<Lessons>(json);

    // Send the updated versions to the parent component when the versions are updated
    useEffect(() => {
        // Do nothing on the first render
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        setJson(lesson);
        setModified(true);
    }, [lesson]);

    return (
        <>
            <h5>Paramètres du niveau</h5>
            {/* Level ID */}
            <InputGroup className="mb-3" size="sm">
                <OverlayTrigger placement="top" overlay={<Tooltip id="levelId">Identifiant du niveau</Tooltip>}>
                    <InputGroup.Text>#</InputGroup.Text>
                </OverlayTrigger>
                <Form.Control
                    placeholder="Identifiant du niveau"
                    aria-label="Identifiant du niveau"
                    aria-describedby="levelId"
                    id="levelId"
                    value={lesson.id}
                    onChange={(e) => {
                        setLesson({ ...lesson, id: e.target.value });
                    }}
                    required
                />
            </InputGroup>

            {/* Level title */}
            <InputGroup className='mt-3' size="sm">

                <OverlayTrigger placement="top" overlay={<Tooltip id="levelTitle">Titre du niveau</Tooltip>}>
                    <InputGroup.Text>
                        <FontAwesomeIcon icon="font" />
                    </InputGroup.Text>
                </OverlayTrigger>
                <Form.Control placeholder="Titre du niveau" id="levelTitle" required value={lesson.title} onChange={(e) => {
                    setLesson({ ...lesson, title: e.target.value });
                }} />
            </InputGroup>

            {/* Level description */}
            <InputGroup className='mt-3' size="sm">
                <OverlayTrigger placement="top" overlay={<Tooltip id="levelDescription">Description du niveau</Tooltip>}>
                    <InputGroup.Text>
                        <FontAwesomeIcon icon="align-left" />
                    </InputGroup.Text>
                </OverlayTrigger>
                <Form.Control placeholder="Description du niveau" id="levelDescription"
                    value={lesson.description} onChange={(e) => {
                        setLesson({ ...lesson, description: e.target.value });
                    }} />
            </InputGroup>

            <hr />

            <h5>Versions</h5>

            {lesson.versions && lesson.versions.map((version: Version) => (
                version.isDeleted ? null :
                    <InputGroup className="mb-3" key={version.id} size="sm">
                        {/* Version Id */}
                        <InputGroup.Text>#</InputGroup.Text>
                        <Form.Control type="text" placeholder='Identifiant de la version' value={version.version} onChange={(e) => {
                            const newVersion = lesson.versions.map((v: Version) => {
                                if (v.id === version.id) {
                                    return { ...v, version: e.target.value, isModified: true };
                                } else {
                                    return v;
                                }
                            });
                            setLesson({ ...lesson, versions: newVersion });
                        }} />
                        {/* Duplicate button */}
                        <Button variant="primary" size="sm" onClick={() => {
                            const newVersion = lesson.versions.find((v: Version) => v.id === version.id);
                            const newVersionData: Version = newVersion ? {
                                ...newVersion,
                                id: lesson.versions.length + 1,
                                isNew: true,
                                version: newVersion.version + '_copy',
                                oldVersion: newVersion.version + '_copy',
                                themes: newVersion.themes,
                                additional_files: newVersion.additional_files,
                                availableAdditionalFiles: newVersion.availableAdditionalFiles
                            } : {
                                id: lesson.versions.length + 1,
                                isNew: true,
                                version: '',
                                oldVersion: '',
                                themes: [],
                                additional_files: [],
                                availableAdditionalFiles: []
                            };
                            setLesson({ ...lesson, versions: [...lesson.versions, newVersionData] });
                        }
                        }>
                            <FontAwesomeIcon icon="copy" />
                        </Button>
                        {/* Delete */}
                        <Button variant="danger" className="btn-sm" onClick={() => {
                            // Check if the version is new
                            if (version.isNew) {
                                const newVersion = lesson.versions.filter((v: Version) => v.id !== version.id);
                                setLesson({ ...lesson, versions: newVersion });
                                return;
                            } else {
                                // Set is deleted to true
                                const newVersion = lesson.versions.map((v: Version) => {
                                    if (v.id === version.id) {
                                        return { ...v, isDeleted: true };
                                    } else {
                                        return v;
                                    }
                                });
                                setLesson({ ...lesson, versions: newVersion });
                            }
                        }}>
                            <FontAwesomeIcon icon="trash" />
                        </Button>
                    </InputGroup>
            ))}


            {/* Add version button */}
            <Button variant="success" className="btn-sm" onClick={() => {
                const newVersion: Version = {
                    id: lesson.versions.length + 1,
                    isNew: true,
                    version: '',
                    oldVersion: '',
                    themes: [],
                    additional_files: [],
                    availableAdditionalFiles: []
                };
                setLesson({ ...lesson, versions: [...lesson.versions, newVersion] });
            }}>
                <FontAwesomeIcon icon="plus" />&nbsp;Ajouter une version
            </Button>
        </>
    );
}

export default LevelEditorContent;