// A QRCode component

import { QRCodeSVG } from 'qrcode.react';

function QRCode( {value, size = 256} : {value: string, size?: number} ) {
    return (
        <QRCodeSVG value={value} size={size} bgColor={'var(--bs-body-bg)'} fgColor={'var(--bs-body-color)'}/>
    );
};

export default QRCode;
