// ----------------------------
// Topbar component
// The topbar of the website, containing the main navigation links
// ----------------------------

// Imports
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Dropdown from 'react-bootstrap/Dropdown';
import Collapse from 'react-bootstrap/Collapse';
import { Alert, Badge, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// Functions
import React, { useState, useEffect, useRef } from 'react';
import { createContext, useContext } from 'react';
import { ReactNode } from 'react';
import { useLocation } from 'react-router-dom';

// CSS
import styles from './css/topbar.module.css';

// Icons
import { IconProp, library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// Functions
import getUserInfos from '../functions/users/getUserInfos';
import { switchTheme, getTheme } from '../functions/theme/setTheme';

// Variables
const wordpressUrl = import.meta.env.VITE_WORDPRESS_URL;

// ----------------------------
// Topbar component
// ----------------------------
const Topbar: React.FC = () => {

  // State variables for user info and admin status
  const [userInfo, setUserInfo] = useState<{ username: string, role: any } | null>(null);
  const [userIsAdmin, setUserIsAdmin] = useState(false);
  const [theme, setTheme] = useState('light');
  const [expanded, setExpanded] = useState(false);

  // Get the user infos when the component is mounted
  useEffect(() => {
    // Get the theme from the local storage
    setTheme(getTheme());

    getUserInfos().then((response: any) => {
      if (response.data) {
        setUserInfo(response.data);
        // Check if response.data.role contains 'administrator'
        if (response.data.role.includes('administrator')) {
          setUserIsAdmin(true);
        }
      }
    });
  }, []);

  // Hide the navbar on route change
  const location = useLocation();
  useEffect(() => {
    // Collapse the navbar
    setExpanded(false);
    // Hide the dropdown menus
    const dropdowns = document.querySelectorAll('.dropdown-menu');
    dropdowns.forEach(dropdown => {
      dropdown.classList.remove('show');
    })
  }, [location]);

  return (
    <Navbar expand="lg" className={styles.Navbar} sticky="top" variant="dark" expanded={expanded}>
      <DropdownProvider>
        {/* Main container */}
        <Container className={styles.NavbarContainer}>

          {/* Brand with logo */}
          <Navbar.Brand as={Link} to="/" className="d-flex align-items-center">
            <img src="/images/favicon.svg" alt="EnSciences" width="30" height="30" className={styles.BrandIcon + " d-inline-block align-top"} />
            <span className={styles.Brand}>
              EnSciences
            </span>
          </Navbar.Brand>

          {/* Hamburger button */}
          <Navbar.Toggle aria-controls="main-nav" onClick={() => setExpanded(!expanded)} />
          <Navbar.Collapse id="main-nav">
            <Nav className="me-auto">

              {/* Course dropdown */}
              <CustomNavDropdown icon="chalkboard-teacher" title="Cours" id="cours">
                <NavDropdown.Item as={Link} to="/seconde_pc?version=2024" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>2nde</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/terminale_es" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>Tle ES</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/terminale_spe?version=2024" className={styles.NavDropdownItem}>
                  <span className={styles.NavDropdownItemText}>Tle Spé</span>
                  <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                </NavDropdown.Item>

                <NavDropdown.Divider />

                <CustomNavSecondaryDropdown icon="archive" title="Archives">
                  <Dropdown.Item disabled>
                    <span className={styles.NavDropdownItemLabel}>2023-2024</span>
                  </Dropdown.Item>
                  <NavDropdown.Item as={Link} to="/terminale_sti2d?version=2023" className={styles.NavDropdownItem}>
                    <span className={styles.NavDropdownItemText}>Tle STI2D</span>
                    <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                  </NavDropdown.Item>
                  <Dropdown.Item as={Link} to="/seconde_snt" className={styles.NavDropdownItem}>
                    <span className={styles.NavDropdownItemText}>2nde</span>
                    <span className={styles.NavDropdownItemTextMuted}>Sciences Numériques et Technologie</span>
                  </Dropdown.Item>
                  <Dropdown.Item disabled>
                    <span className={styles.NavDropdownItemLabel}>2022-2023</span>
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/premiere_es?version=2022" className={styles.NavDropdownItem}>
                    <span className={styles.NavDropdownItemText}>1ère ES</span>
                    <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                  </Dropdown.Item>
                  <Dropdown.Item disabled>
                    <span className={styles.NavDropdownItemLabel}>2021-2022</span>
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/bts1_systemes" className={styles.NavDropdownItem}>
                    <span className={styles.NavDropdownItemText}>BTS MS</span>
                    <span className={styles.NavDropdownItemTextMuted}>Physique-Chimie</span>
                  </Dropdown.Item>
                </CustomNavSecondaryDropdown>

                <NavDropdown.Item as={Link} to="/ressources" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="puzzle-piece" /> Autres ressources
                </NavDropdown.Item>

                <NavDropdown.Divider />

                <NavDropdown.Item as={Link} to="/cahier_texte" className={styles.NavDropdownItem} disabled>
                  <FontAwesomeIcon icon="calendar-check" /> Cahier de texte
                </NavDropdown.Item>

                <NavDropdown.Item as={Link} to="https://physnet.ensciences.fr" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="school" /> PhysNet
                </NavDropdown.Item>

                <NavDropdown.Item as={Link} to="https://www.moodle.ensciences.fr/" className={styles.NavDropdownItem} disabled>
                  <FontAwesomeIcon icon="graduation-cap" /> Moodle
                </NavDropdown.Item>
              </CustomNavDropdown>

              {/* Animations */}
              <Nav.Link as={Link} to="/animations" className={styles.mainNavLink}>
                <FontAwesomeIcon icon="cube" /> Animations
              </Nav.Link>

              {/* Applications */}
              {/* Course dropdown */}
              <CustomNavDropdown icon="shapes" title="Applications" id="applications">
                <NavDropdown.Item as={Link} to="/applications" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="rocket" /> Applications en ligne
                </NavDropdown.Item>

                <CustomNavSecondaryDropdown icon="display"
                  title="Logiciels">
                  <Dropdown.Item as={Link} to="/multicompile" className={styles.NavDropdownItem}>
                    MultiCompile
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/physnet" className={styles.NavDropdownItem}>
                    PhysNet
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/chronophys" className={styles.NavDropdownItem}>
                    ChronoPhys
                  </Dropdown.Item>
                </CustomNavSecondaryDropdown>


                <NavDropdown.Item as={Link} to="https://objets.ensciences.fr" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="microchip" /> Objets connectés
                </NavDropdown.Item>

              </CustomNavDropdown>

              {/* Resources */}
              <CustomNavDropdown icon="box-open"
                id="ressources"
                title="Ressources">
                <NavDropdown.Item as={Link} to="https://banques.ensciences.fr" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="database" /> Banque de ressources <Badge bg="success" className="ms-2">Nouveau</Badge>
                </NavDropdown.Item>

                <NavDropdown.Item as={Link} to="/latex" className={styles.NavDropdownItem}>
                  <FontAwesomeIcon icon="file-alt" /> LaTeX <Badge bg="success" className="ms-2">Nouveau</Badge>
                </NavDropdown.Item>

                <NavDropdown.Divider />

                <CustomNavSecondaryDropdown icon="graduation-cap" title="Concours pour l'enseignement">
                  <Dropdown.Item as={Link} to="/capes" className={styles.NavDropdownItem}>
                    CAPES (2018)
                  </Dropdown.Item>
                  <Dropdown.Item as={Link} to="/agreg" className={styles.NavDropdownItem}>
                    Agrégation de Chimie (2019)
                  </Dropdown.Item>
                </CustomNavSecondaryDropdown>
              </CustomNavDropdown>

              {/* Blog */}
              <Nav.Link as={Link} to="/blog" className={styles.mainNavLink}>
                <FontAwesomeIcon icon="feather-pointed" /> Blog
              </Nav.Link>

              {/* Additional links dropdown */}
              <CustomNavDropdown icon="external-link-alt" title="Liens" id="liens">
                <NavDropdown.Item as={Link} to="https://www.atrium-sud.fr/web/lgt-guillaume-apollinaire-063001" className={styles.NavDropdownItem}>
                  Site du Lycée Guillaume Apollinaire
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="https://www.atrium-sud.fr/" className={styles.NavDropdownItem}>
                  Atrium (ENT)
                </NavDropdown.Item>
              </CustomNavDropdown>
            </Nav>

            {/* Links on the right */}
            <Nav className="ms-auto">

              {/* Change theme */}
              <Nav.Link onClick={() => {
                switchTheme();
                setTheme(getTheme());
              }} className={styles.mainNavLink} id="themeButton">
                <OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip-theme">Changer de thème</Tooltip>}>
                  {theme === 'light' ? <FontAwesomeIcon icon="moon" /> : <FontAwesomeIcon icon="sun" />}
                </OverlayTrigger>
                <span className={"d-inline d-lg-none"}> Thème</span>
              </Nav.Link>

              {/* Contact */}
              <Nav.Link as={Link} to="/contact" className={styles.mainNavLink}>
                <OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip-contact">Me contacter</Tooltip>}>
                  <FontAwesomeIcon icon="envelope" />
                </OverlayTrigger>
                <span className={"d-inline d-lg-none"}> Contact</span>
              </Nav.Link>

              {/* Login */}
              {/* Afficher des informations de l'utilisateur ici, par exemple: */}
              {userInfo && (
                // Dropdown with user info
                <CustomNavDropdown icon="user" title={userInfo.username} id="user" align="end">
                  <div className="px-3">
                    <div style={{ fontSize: "1.2rem", fontWeight: "bold", marginBottom: "5px", marginTop: "5px" }}>Bienvenue {userInfo.username} !</div>
                    <Alert variant="info" className={styles.NavDropdownItem}>
                      <FontAwesomeIcon icon="lightbulb" /> Vos droits d'accès : <strong>{userInfo.role.map((role: any) => role).join(', ')}</strong>.
                    </Alert>
                  </div>
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={Link} to="/reset_password" className={styles.NavDropdownItem}>
                    <FontAwesomeIcon icon="lock" /> Changer de mot de passe
                  </NavDropdown.Item>
                  {userIsAdmin && (
                    <div>
                      <NavDropdown.Item as={Link} to="/admin" className={styles.NavDropdownItem}>
                        <FontAwesomeIcon icon="tachometer-alt" /> Tableau de bord
                      </NavDropdown.Item>
                      <NavDropdown.Item href={wordpressUrl + 'wp-admin'} className={styles.NavDropdownItem}>
                        <FontAwesomeIcon icon="tools" /> Administration de Wordpress
                      </NavDropdown.Item>
                      <NavDropdown.Item href={
                        'https://analytics.ensciences.fr/'
                      } className={styles.NavDropdownItem}>
                        <FontAwesomeIcon icon="chart-line" /> Audiences
                      </NavDropdown.Item>
                      {/* Webmail */}
                      <NavDropdown.Item as={Link} to="https://webmail.ensciences.fr" className={styles.NavDropdownItem}>
                        <FontAwesomeIcon icon="envelope" /> Webmail
                      </NavDropdown.Item>
                      <NavDropdown.Item href={'https://filemanager.ensciences.fr'} className={styles.NavDropdownItem}>
                        <FontAwesomeIcon icon="file" /> Fichiers
                      </NavDropdown.Item>
                    </div>
                  )}
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={Link} to="/login?logout=true" className={styles.NavDropdownItem} style={{ color: "var(--bs-danger)" }}>
                    <FontAwesomeIcon icon="sign-out-alt" /> Déconnexion
                  </NavDropdown.Item>
                </CustomNavDropdown>
              )}
              {!userInfo && (
                <Nav.Link as={Link} to="/login" className={styles.mainNavLink}>
                  <OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip-login">Se connecter</Tooltip>}>
                    <FontAwesomeIcon icon="sign-in-alt" />
                  </OverlayTrigger>
                  <span className={"d-inline d-lg-none"}> Connexion</span>
                </Nav.Link>
              )}

            </Nav>
          </Navbar.Collapse>
        </Container>
      </DropdownProvider>
    </Navbar >
  );
}

// ----------------------------
// Custom NavDropdown components
// @param icon: IconProp - the icon to display
// @param title: string - the title of the dropdown
// @param id: string - the id of the dropdown
// @param children: React.ReactNode - the children of the dropdown
// ----------------------------
type CustomNavDropdownProps = {
  icon: IconProp;
  title: string;
  id: string;
  children: React.ReactNode;
  align?: 'start' | 'end';
};

function CustomNavDropdown({ icon, title, id, children, align = 'start'
}: CustomNavDropdownProps) {

  // Handle hover and click events. Initialize the dropdown as closed.
  const [isOpen, setIsOpen] = useState(false);
  let timeoutId: NodeJS.Timeout;
  const { openDropdownId, setOpenDropdownId } = useDropdownContext();

  useEffect(() => {
    if (openDropdownId === id) {
      setIsOpen(true);
    } else {
      setIsOpen(false);
    }
  }, [openDropdownId, id]);

  const handleMouseEnter = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Clear the timeout and open the dropdown
    clearTimeout(timeoutId);
    setIsOpen(true);
    setOpenDropdownId(id);
  };

  const handleMouseLeave = () => {
    // Do nothing if the window is small
    if (window.innerWidth <= 992) return;

    // Close the dropdown after a delay
    timeoutId = setTimeout(() => setIsOpen(false), 250);
  };


  const handleDropdownClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    // Do nothing if the window is large
    if (window.innerWidth > 992) return;

    // Prevent the parent NavDropdown from closing when clicking on a SecondaryNavDropdown
    if (hasClass(event.target as Element, styles.SecondaryNavDropdown)) {
      return;
    }

    // Toggle the dropdown
    setIsOpen(!isOpen);
  }

  return (
    <NavDropdown align={align}
      title={<span><FontAwesomeIcon icon={icon} /> {title}</span>}
      id={id}
      // On width > 992px (bootstrap lg), the dropdown is shown on hover. On width <= 992px, the dropdown is shown on click.
      show={isOpen}
      onMouseEnter={() => handleMouseEnter()}
      onMouseLeave={() => handleMouseLeave()}
      onClick={(event) => handleDropdownClick(event as React.MouseEvent<HTMLDivElement, MouseEvent>)}
      className={styles.mainNavDropdown}
    >
      <Collapse in={isOpen}>
        <div>
          {children}
        </div>
      </Collapse>
    </NavDropdown>
  );
}

// ----------------------------
// Custom NavSecondaryDropdown components
// @param icon: IconProp - the icon to display
// @param title: string - the title of the dropdown
// @param children: React.ReactNode - the children of the dropdown
// ----------------------------
type CustomNavSecondaryDropdownProps = {
  icon: IconProp;
  title: string;
  children: React.ReactNode;
};

function CustomNavSecondaryDropdown({ icon, title, children }: CustomNavSecondaryDropdownProps) {
  const [isOpen, setIsOpen] = useState(false);
  const timeoutIdRef = useRef<NodeJS.Timeout | null>(null);
  const dropdownRef = useRef<HTMLDivElement | null>(null);

  const handleMouseEnter = () => {
    if (window.innerWidth <= 992) return;
    if (timeoutIdRef.current) {
      clearTimeout(timeoutIdRef.current);
    }
    setIsOpen(true);
  };

  const handleMouseLeave = () => {
    if (window.innerWidth <= 992) return;

    const handleMouseMove = (event: MouseEvent) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
        if (dropdownRef.current) {
          dropdownRef.current.classList.add('fade-out');
        }
        timeoutIdRef.current = setTimeout(() => {
          setIsOpen(false);
          if (dropdownRef.current) {
            dropdownRef.current.classList.remove('fade-out');
          }
        }, 250);
        document.removeEventListener('mousemove', handleMouseMove);
      }
    };

    document.addEventListener('mousemove', handleMouseMove);
  };

  const handleDropdownClick = () => {
    if (window.innerWidth > 992) return;
    setIsOpen(!isOpen);
  };

  return (
    <Dropdown
      drop="end"
      show={isOpen}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleDropdownClick}
      ref={dropdownRef}
    >
      <Dropdown.Toggle bsPrefix="custom" className={styles.SecondaryNavDropdown}>
        <div className={styles.SecondaryNavDropdownContent}>
          <span className={styles.SecondaryNavDropdownText}><FontAwesomeIcon icon={icon} /> {title}</span>
          <FontAwesomeIcon icon='caret-right' />
        </div>
      </Dropdown.Toggle>

      <Dropdown.Menu className={styles.SecondaryNavDropdownMenu}>
        {children}
      </Dropdown.Menu>
    </Dropdown>
  );
}

// ----------------------------
// hasClass function
// Check if an element has a specific class in a recursive way
// @param element: Element | null - the element to check
// @param className: string - the class to check
// @returns boolean - true if the class is found, false otherwise
// ----------------------------
function hasClass(element: Element | null, className: string): boolean {
  if (!element || element === document.body) {
    return false;
  } else if (element.classList.contains(className)) {
    return true;
  } else {
    return hasClass(element.parentElement, className);
  }
}

// NavDropdownContext (to manage the dropdowns)
type DropdownContextType = {
  openDropdownId: string | null;
  setOpenDropdownId: (id: string | null) => void;
};

const DropdownContext = createContext<DropdownContextType | undefined>(undefined);

export const DropdownProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [openDropdownId, setOpenDropdownId] = useState<string | null>(null);

  return (
    <DropdownContext.Provider value={{ openDropdownId, setOpenDropdownId }}>
      {children}
    </DropdownContext.Provider>
  );
};

export const useDropdownContext = () => {
  const context = useContext(DropdownContext);
  if (!context) {
    throw new Error('useDropdownContext must be used within a DropdownProvider');
  }
  return context;
};

// Export the component
export default Topbar;