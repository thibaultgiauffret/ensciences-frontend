// ----------------------------
// Container components
// ----------------------------

import React from "react";
import styles from "./css/containers.module.css";

function PageContainer({ children, ...props }: { children: React.ReactNode }) {
    return (
        <div className={styles.pageContainer} {...props}>
            {children}
        </div>
    );
}

export { PageContainer };

function MenuContainer(props: { children: React.ReactNode }) {
    return (
        <div className={styles.menuContainer}>
            {props.children}
        </div>
    );
}

export { MenuContainer };

function FullContainer({ children, ...props }: { children: React.ReactNode }) {
    return (
        <div className={styles.fullContainer} {...props}>
            {children}
        </div>
    );
}

export { FullContainer };