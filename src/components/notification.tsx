// A notification component that displays a toast message based on the URL parameters

import { useEffect, useRef } from 'react';
import { useToasts } from 'react-bootstrap-toasts';
import removeParam from '../functions/url/removeParam';

function Notification() {
    console.log('Notification component');
    const urlParams = new URLSearchParams(window.location.search);
    const notification = urlParams.get('notification');
    const message = urlParams.get('message');

    console.log('Notification:', notification);
    console.log('Message:', message);

    // Toasts
    const toasts = useToasts();

    // Refs
    const notificationPrev = useRef<string | null>(null);
    const messagePrev = useRef<string | null>(null);

    useEffect(() => {
        // Check if the notification or message has changed
        if (notification !== notificationPrev.current || message !== messagePrev.current) {
            notificationPrev.current = notification;
            messagePrev.current = message;

            if (notification !== null || message !== null) {
                switch (notification) {
                    case 'success':
                        toasts.show({
                            headerContent: 'Succès',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'success',
                                autohide: true,
                                delay: 5000,
                            },
                        });
                        break;
                    case 'error':
                        toasts.show({
                            headerContent: 'Erreur',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'danger',
                                autohide: true,
                                delay: 5000,
                            },
                        });
                        break;
                    case 'info':
                        toasts.show({
                            headerContent: 'Information',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'primary',
                                autohide: true,
                                delay: 5000,
                            },
                        });
                        break;
                    case 'warning':
                        toasts.show({
                            headerContent: 'Attention',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'warning',
                                autohide: true,
                                delay: 5000,
                            },
                        });
                        break;
                    default:
                        toasts.show({
                            headerContent: 'Notification',
                            bodyContent: message || '',
                            toastProps: {
                                bg: 'primary',
                                autohide: true,
                                delay: 5000,
                            },
                        });
                }
            }
        }
    }, []);

    // Remove the notification parameter from the URL
    removeParam(window.location.href, ['notification', 'message']);

    return null
}

export default Notification;