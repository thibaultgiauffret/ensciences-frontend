// Imports
import Card from "react-bootstrap/Card";
import Placeholder from "react-bootstrap/Placeholder";
import ScrollApplication from "react-animate-on-scroll";
import { ButtonLink } from "../buttons";
import DOMPurify from "dompurify";

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// CSS
import styles from "../css/cards.module.css";

// Functions
import { useEffect, useState } from "react";

function BlogPostCard({ data }: { data: BlogPostData }) {

    // If all fields are empty, return an empty card with placeholders
    if (data.title === "") {
        // Placeholder card
        return (
            <ScrollApplication animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>

                    <Card.Img variant="top" src="./images/picturePlaceholder.svg" style={{ width: "100%", height: "160px", objectFit: "cover" }} />
                    <Placeholder as={Card.Body} animation="glow">
                        <Placeholder as={Card.Title} animation="glow" className="text-center">
                            <Placeholder xs={6} />
                        </Placeholder>
                        <Placeholder as={Card.Text} animation="glow">
                            <Placeholder xs={7} />
                            <Placeholder xs={4} />
                            <Placeholder xs={4} />
                            <Placeholder xs={6} />
                        </Placeholder>

                    </Placeholder>
                    <Card.Footer>
                        <div className="small me-3">
                            {/* Author */}
                            <span className="small me-3">
                                <FontAwesomeIcon icon="user"></FontAwesomeIcon>
                                &nbsp;
                                <Placeholder xs={6} />
                            </span><br />

                            {/* Date */}
                            <span className="small me-3">
                                <FontAwesomeIcon icon="calendar-alt"></FontAwesomeIcon>
                                &nbsp;
                                <Placeholder xs={8} />
                            </span><br />

                            {/* Categories */}
                            <span className="small me-3">
                                <FontAwesomeIcon icon="tags"></FontAwesomeIcon>
                                &nbsp;
                                <Placeholder xs={10} />
                            </span>
                        </div>
                        <ButtonLink href={"/blog/read?post=" + data.id} className={styles.cardButton}>
                            <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Lire la suite
                        </ButtonLink>
                    </Card.Footer>
                </Card>
            </ScrollApplication>
        );
    } else {
        // Card with icon, title, color and children
        return (
            <ScrollApplication animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className="h-100 mx-auto py-2">
                <Card className={styles.card + " " + styles.themeCard + " h-100 mx-auto"}>

                    <Card.Img src={
                        extractImageFromContent(data.content)
                    } className={styles.cardImage} style={{ objectFit: "contain", padding: '5px' }} />
                    <Card.Body className="d-flex flex-column justify-content-center align-items-center">
                        <Card.Title className="text-center">
                            <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(data.title) }}></div>
                        </Card.Title>
                        <Card.Text className="text-justify">
                            <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(shortenExcerpt(data.excerpt)) }}></div>
                        </Card.Text>

                    </Card.Body>
                    <Card.Footer className={styles.cardFooter}>
                        <div className="small me-3">
                            {/* Author */}
                            <span className="small me-3">
                                <FontAwesomeIcon icon="user"></FontAwesomeIcon>
                                &nbsp;{data.author}</span><br />

                            {/* Date */}
                            <span className="small me-3">
                                <FontAwesomeIcon icon="calendar-alt"></FontAwesomeIcon>
                                &nbsp;{data.date}</span><br />

                            {/* Categories */}
                            <div className="small me-3" style={{ textAlign: 'left' }}>
                                <FontAwesomeIcon icon="tags"></FontAwesomeIcon>
                                &nbsp;
                                {/* Show only three first tags, rest is ... */}
                                {data.post_categories.slice(0, 3).map((category: string, index: number) => {
                                    return (
                                        category + (index < 2 ? ', ' : '')
                                    );
                                })}
                            </div>
                        </div>
                        <ButtonLink href={"/blog/read?post=" + data.id} className={styles.cardButton + " stretched-link"}>
                            <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Lire la suite
                        </ButtonLink>
                    </Card.Footer>
                </Card>
            </ScrollApplication>
        );
    }
}

function extractImageFromContent(content: string) {
    const regex = /<img.*?src="(.*?)"/;
    const found = content.match(regex);
    return found ? found[1] : '';
}

function shortenExcerpt(excerpt: string) {
    // Maximum length of the excerpt
    const maxLength = 230;

    // Replace <p> and </p> tags
    excerpt = excerpt.replace(/<p>/g, '');
    excerpt = excerpt.replace(/<\/p>/g, '');

    // Trim the excerpt to the maximum length
    excerpt = excerpt.length > maxLength ? excerpt.substring(0, maxLength) + '...' : excerpt;

    // Retrim the excerpt if we cut a word
    excerpt = excerpt.substr(0, Math.min(excerpt.length, excerpt.lastIndexOf(" ")));

    return excerpt + '...';
}

// Table of content for blog posts
interface TableOfContentProps {
    content: string;
}

interface TocItem {
    id: string;
    tagName: string;
    textContent: string;
    children: TocItem[];
}

function TableOfContent({ content }: TableOfContentProps) {
    const [tocItems, setTocItems] = useState<TocItem[]>([]);

    useEffect(() => {
        const parser = new DOMParser();
        const htmlDocument = parser.parseFromString(content, "text/html");
        const headings = htmlDocument.querySelectorAll("h2, h3, h4");
        const toc: TocItem[] = [];

        headings.forEach((heading) => {
            if (!(heading instanceof HTMLElement)) return;
            const item: TocItem = {
                id: heading.id,
                tagName: heading.tagName,
                textContent: heading.textContent || '',
                children: [],
            };

            if (heading.tagName === "H2") {

                toc.push(item);
            } else if (heading.tagName === "H3" && toc.length > 0) {
                toc[toc.length - 1].children.push(item);
            } else if (heading.tagName === "H4" && toc.length > 0 && toc[toc.length - 1].children.length > 0) {
                toc[toc.length - 1].children[toc[toc.length - 1].children.length - 1].children.push(item);
            }
        });

        setTocItems(toc);
    }, [content]);

    const renderToc = (items: TocItem[]) => (
        <ul>
            {items.map((item) => (
                <li key={item.id}>
                    <a href={`#${item.id}`}>{item.textContent}</a>
                    {item.children.length > 0 && renderToc(item.children)}
                </li>
            ))}
        </ul>
    );

    return <div>{renderToc(tocItems)}</div>;
};

export { BlogPostCard, TableOfContent };