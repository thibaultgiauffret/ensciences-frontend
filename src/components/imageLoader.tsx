import { useEffect } from 'react';
import { getImage } from '../functions/files/getImage';

const ImageLoader = () => {
    useEffect(() => {
        const loadImages = async () => {
            const elements = document.querySelectorAll('[data-path]');
            elements.forEach(async (element) => {
                const dataPath = element.getAttribute('data-path');
                if (dataPath) {

                    // Set a placeholder image
                    element.setAttribute('src', './images/picturePlaceholder.svg');

                    // Load the image
                    getImage(dataPath).then((response: any) => {
                        if (response.status === 'success') {
                            element.setAttribute('src', response.file);
                        } else {
                            console.error(response);
                        }
                    }).catch((error) => {
                        console.error(error);
                    });
                }
            });
        }
        loadImages();
    }
    , []);

return null; // Ce composant n'a pas besoin de rendre quoi que ce soit
};

export default ImageLoader;