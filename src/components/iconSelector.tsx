// Components
import { Button, InputGroup, Form, Dropdown } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp, library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);

// Import the CSS
import './css/iconSelector.css';

// Functions
import { useState, useEffect, useRef } from 'react';

const IconSelector = ({ value, setValue }: { value: string, setValue: any }) => {

    // Variables
    const isFirstRender = useRef(true); // Used to prevent the useEffect from running on the first render
    const timeoutRef = useRef<NodeJS.Timeout | null>(null); // Timeout used to wait for the user to stop typing

    // States
    const [iconList, setIconList] = useState([] as string[]);
    const [inputValue, setInputValue] = useState(value);
    const [dropdownVisible, setDropdownVisible] = useState(false);

    useEffect(() => {
        // On component mount, do nothing
        if (isFirstRender.current) {
            isFirstRender.current = false;
            return;
        }

        // Clear the previous setTimeout
        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current);
        }

        // Wait for the user to stop typing
        timeoutRef.current = setTimeout(() => {
            const filteredIcons = Array.from(new Set(Object.keys(fas)
                .map(key => fas[key].iconName)
                .filter(iconName => iconName.toLowerCase().includes(inputValue.toLowerCase()))))
                .sort();

            setIconList(filteredIcons);
            setValue(inputValue);
        }, 500);

        // Cleanup
        return () => {
            if (timeoutRef.current) {
                clearTimeout(timeoutRef.current);
            }
        };

    }, [inputValue]);

    // Check if the icon is valid before displaying it
    const isValidIcon = (icon: string): boolean => {
        return iconList.includes(icon);
    }

    return (
        <>
            {/* Input */}
            <Form.Control
                type="text"
                className={"form-control " + (isValidIcon(inputValue) ? ' no-right-border' : '')}
                placeholder="Type to filter..."
                onChange={(e) => setInputValue(e.target.value)}
                onFocus={() => setDropdownVisible(true)}
                value={inputValue}
            />
            {/* Icon div */}
            {iconList.length > 1 &&
                <Dropdown show={dropdownVisible}>
                    <Dropdown.Menu className="icon-dropdown">
                        <Dropdown.Item as="div" className="icon-item">
                            <div className={iconList.length > 1 ? 'visible icon-grid' : 'hidden'}>
                                {iconList.map(icon => (
                                    <Button key={icon} className={'icon'} onClick={() => {
                                        // Change the input value to the selected icon
                                        setInputValue(icon);
                                        // Hide the dropdown
                                        setDropdownVisible(false);
                                    }} size="sm" variant="outline-secondary">
                                        <FontAwesomeIcon icon={icon as IconProp} />
                                    </Button>
                                ))}
                            </div>
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            }
            {/* Icon render */}
            {isValidIcon(inputValue) && (
                <InputGroup.Text className="no-bg-no-left-border">
                    <FontAwesomeIcon icon={inputValue as IconProp} />
                </InputGroup.Text>
            )}
        </>
    );
};

export default IconSelector;