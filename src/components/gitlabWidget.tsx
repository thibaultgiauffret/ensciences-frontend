// ----------------------------
// GitlabWidget component
// ----------------------------

import React, { useEffect, useState } from 'react';
import Card from "react-bootstrap/Card";
import styles from "./css/cards.module.css";
import stylesGit from "./css/gitlabWidget.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ButtonLink } from "./buttons";

const GitLabWidget = ({ username, server, title }: { username: string, server: string, title: string }) => {
    const [activity, setActivity] = useState<any[]>([]);

    useEffect(() => {
        // Get the data from the GitLab API
        getAllData(server, username)
            .then((data) => {
                // Set the data in the state
                setActivity(data);
            });
    }, [username, server]);

    return (

        <Card className={styles.card + " mb-3"}>
            <Card.Header><FontAwesomeIcon icon={["fab", "gitlab"]} />&nbsp;{title}</Card.Header>

            <div className={stylesGit.waiter} id={"loader_"+server}>
                <div className="spinner-border text-primary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>

            <div className={stylesGit.cardText + " d-none"} id={"content_"+server}>
                {activity.map((event, index) => (
                    <React.Fragment key={index}>
                        {renderEvent(event)}
                    </React.Fragment>
                ))}
            </div>

            <Card.Footer className={styles.cardFooter}>
                <ButtonLink href={server + username} className={styles.cardButton} target="_blank">
                    <FontAwesomeIcon icon="eye"></FontAwesomeIcon>&nbsp;Consulter
                </ButtonLink>
            </Card.Footer>
        </Card >
    );

};

export default GitLabWidget;

// Get all data from the GitLab API (20 last events)
async function getAllData(server: string, username: string) {
    const eventsUrl = server + 'api/v4/users/' + username + '/events';
    const response = await fetch(eventsUrl);
    const data = await response.json();
    
    // Get the project names
    const projectNames: any = {};

    // For each event, get the project name (if not already done) and generate a custom json
    const promises = data.map(async (event: any) => {
        let projectName;
        let projectPath;

        if (projectNames[event.project_id]) {
            projectName = projectNames[event.project_id];
        } else {
            const projectUrl = server + 'api/v4/projects/' + event.project_id;
            const response = await fetch(projectUrl);
            const project = await response.json();
            projectName = project.name;
            projectPath = project.path;

            projectNames[event.project_id] = projectName;
        }
        
        // Generate a custom json depending on the event type
        switch (event.action_name) {
            case 'pushed to':
                event = {
                    id: event.id,
                    type: 'pushed to',
                    icon: 'code-commit',
                    project: projectName,
                    action: 'a poussé du code sur',
                    date: generateDate(event.created_at),
                    message: event.push_data.commit_title,
                    commit_id: event.push_data.commit_to,
                    commit_url: server + event.author_username + "/" + projectPath + "/commit/" + event.push_data.commit_to,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'pushed new':
                event = {
                    id: event.id,
                    type: 'pushed new',
                    icon: 'code-branch',
                    project: projectName,
                    action: 'a créé une nouvelle branche sur',
                    date: generateDate(event.created_at),
                    message: event.push_data.commit_title,
                    commit_id: event.push_data.commit_to,
                    commit_url: server + projectPath + "/commit/" + event.push_data.commit_to,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'opened':
                event = {
                    id: event.id,
                    type: 'opened',
                    icon: 'plus',
                    project: projectName,
                    action: 'a ouvert',
                    date: generateDate(event.created_at),
                    message: event.target_title,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'commented on':
                event = {
                    id: event.id,
                    type: 'commented on',
                    icon: 'comment',
                    project: projectName,
                    action: 'a posté un commentaire sur',
                    date: generateDate(event.created_at),
                    message: event.note.body,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'updated':
                event = {
                    id: event.id,
                    type: 'updated',
                    icon: 'edit',
                    project: projectName,
                    action: 'a mis à jour',
                    date: generateDate(event.created_at),
                    message: event.target_title,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'closed':
                event = {
                    id: event.id,
                    type: 'closed',
                    icon: 'times',
                    project: projectName,
                    action: 'a fermé sur',
                    date: generateDate(event.created_at),
                    message: event.target_title,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'deleted':
                event = {
                    id: event.id,
                    type: 'deleted',
                    icon: 'trash',
                    project: projectName,
                    action: 'a supprimé la branche',
                    date: generateDate(event.created_at),
                    message: event.target,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'created':
                event = {
                    id: event.id,
                    type: 'created',
                    icon: 'plus',
                    project: projectName,
                    action: 'a créé',
                    date: generateDate(event.created_at),
                    message: event.target_title,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
            case 'imported':
                event = {
                    id: event.id,
                    type: 'imported',
                    icon: 'download',
                    project: projectName,
                    action: 'a importé',
                    date: generateDate(event.created_at),
                    message: event.target,
                    urlName: projectName,
                    url: server + event.author_username + "/" + projectPath,
                    profile: event.author.name
                }
                break;
        }

        return event;
    });

    // Wait for all promises to resolve
    const events = await Promise.all(promises);

    // Show the content and hide the loader
    document.getElementById("loader_"+server)?.classList.add("d-none");
    document.getElementById("content_"+server)?.classList.remove("d-none");

    return events;
}

// Generate a date string
function generateDate(dateString: string) {
    const date = new Date(dateString);
    return date.toLocaleDateString() + " " + date.toLocaleTimeString();
}

// Render a single event
function renderEvent(event: any) {
    return (
        <div className={stylesGit.gitlabEvent} key={event.id}>

            <p className={stylesGit.gitlabEventTitle}>
                <FontAwesomeIcon icon={event.icon} /> &nbsp;
                {event.profile} {event.action} <a href={event.url ? event.url : ''}>{event.urlName}</a>
            </p>

            <p className={stylesGit.gitlabEventMessage}>
                {event.commit_id ? <span><a href={event.commit_url}> {
                    event.commit_id.substring(0, 8)
                }
                </a>&nbsp;•&nbsp;</span> : ''}<span className="text-muted">{event.message}</span>
            </p>
            <span className={stylesGit.gitlabEventDate}>
                {event.date}
            </span>
        </div>
    );
}