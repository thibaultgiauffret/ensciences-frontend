// ----------------------------
// ChronoPhys page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, Button, Modal, Alert } from 'react-bootstrap';
import { ButtonLink } from '../components/buttons';
import { H2 } from '../components/sections';
import { SimpleBlogCard } from '../components/cards';
import ScrollAnimation from 'react-animate-on-scroll';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import ImageLoader from '../components/imageLoader';

// Functions
import { useState, useEffect } from 'react';
import { getBlogPost } from '../functions/blog/getBlogPost';
// CSS
import styles from './css/multicompile.module.css';
import "@fontsource/dosis/400.css";

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
import { CollapsibleAlert } from '../components/collapsibleAlert';
library.add(fas)
library.add(fab)



function Chronophys(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();


    }, []);

    // Get the blog post n°524
    const [blogPost, setBlogPost] = useState({ id:'',  title: '', author: '', date: '', content: '', link: '' });
    useEffect(() => {
        getBlogPost(820).then((response: any) => {
            setBlogPost(response.blogPost);
        }).catch((error) => {
            console.error(error);
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Impossible de récupérer le dernier article du blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
    }, []);

    // Modal
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return (
        <>
            <ImageLoader />
            <Header title={''} subtitle={props.subtitle} color="#34495e">
                <h1 className={styles.title + ' mt-5'}><span className={styles.lightTitle}>Chrono</span><span className={styles.boldTitle}>Phys</span></h1>
                <h3 className={styles.subtitle}>Logiciel de chronophotographie pour les Sciences-Physiques</h3>
                <div className={styles.titleContainer}>
                    <div className={'my-4'}>
                        {/* Download */}
                        <Button variant="primary" className="m-1" onClick={handleShow}>
                            <FontAwesomeIcon icon={['fas', 'download']} />&nbsp;Télécharger
                        </Button>

                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Téléchargement</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Alert variant="info">
                                    Veuillez consulter la documentation avant d'utiliser ChronoPhys !
                                </Alert>

                                <Row className="text-center">
                                    <Col>

                                        <ButtonLink href="https://nuage02.apps.education.fr/index.php/s/KqQPzMzQDSfmLEs" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Windows"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'windows']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink href="https://nuage02.apps.education.fr/index.php/s/jez8m34DCEwY4d6" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Linux"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'linux']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink className={styles.downloadButton + ' m-1'} target="_blank">
                                            <FontAwesomeIcon icon={['fab', 'apple']} />
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink href="https://forge.apps.education.fr/thibaultgiauffret/chronophys" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger les sources"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'gitlab']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                </Row>


                            </Modal.Body>
                        </Modal>

                        {/* Documentation */}
                        <Button variant="secondary" className="m-1" href="/blog/read?post=820">
                            <FontAwesomeIcon icon={['fas', 'book']} />&nbsp;Documentation
                        </Button>

                        {/* Forum */}
                        <Button variant="secondary" className="m-1" disabled>
                            <FontAwesomeIcon icon={['fas', 'comments']} />&nbsp;Forum
                        </Button>
                    </div>
                    <div>
                        Disponible sur <FontAwesomeIcon icon={['fab', 'windows']} />&nbsp;Windows et bien évidemment <FontAwesomeIcon icon={['fab', 'linux']} />&nbsp;Linux ! Peut-être un jour sur <FontAwesomeIcon icon={['fab', 'apple']} />&nbsp;MacOs...
                    </div>

                    <div className={'my-4'}>
                        <img alt="ChronoPhys" className={styles.headImage} data-path="chronophys/gui.png" />
                    </div>
                </div>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        <Col xs={12} md={8}>
                            <H2>Présentation</H2>

                            <Alert variant="warning">
                                <Alert.Heading>
                                    <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Attention !</Alert.Heading>

                                Le projet est en cours de développement et la version actuelle (dev-beta, 03 juin 2024) est encore en phase de test !
                                Le logiciel reste pour l'instant à <b>utiliser avec précautions en classe</b>.

                            </Alert>

                            <p>ChronoPhys est un logiciel <u>libre</u> permettant de <b>réaliser des chronophotographies en Sciences-Physiques</b>. Le logiciel tire parti des fonctionnalités proposées par les librairies Matplotlib et OpenCV pour traiter la vidéo et réaliser le pointage.</p>

                            <p>Entièrement codé de zéro, le développement du logiciel a tiré son inspiration des différents logiciels actuellement utilisés dans les classes en Sciences-Physiques. Il se positionne dans leur lignée et vise à satisfaire le cahier des charges général suivant :</p>
                            <ul>
                                <li>Multiplateforme (Linux, Windows, Mac OSX à partir des sources)&nbsp;;</li>
                                <li>Gratuit et disponibilité du code source sous licence GNU GPLv3&nbsp;;</li>
                                <li>Fonctionnalités et interface relativement proches des logiciels actuellement disponibles&nbsp;;</li>
                                <li>Compatibilité étendue au niveau des extensions et codecs vidéos pris en charge&nbsp;;</li>
                                <li>Basé sur Python et utilisation de librairies telles que Matplotlib et OpenCV (qui laissent de très nombreuses ouvertures en terme de fonctionnalités)&nbsp;;</li>
                                <li>Interface permettant une progression guidée lors de la réalisation de la chronophotographie, avec des étapes clairement définies et des "checkpoints".</li>
                            </ul>
                            <p>C'est en gardant ces grands principes en tête que ChronoPhys a été développé. Il est à présent disponible au téléchargement sous forme d'exécutable ou de code source.</p>

                            <Alert variant="info">
                                <Alert.Heading>
                                    <FontAwesomeIcon icon={['fas', 'coffee']} />&nbsp;Important, à lire !
                                </Alert.Heading>

                                Vu que je ne suis pas expert en la matière, le développement d'un tel logiciel demande énormément d'investissement personnel et de temps (le tout accompagné d'au moins plusieurs dizaines de litres de café...). Le logiciel restera donc en phase de test un certain temps. De nombreux bugs et aberrations pourront être rencontrés lors de l'utilisation du logiciel ou de la lecture du code source.

                                <hr />

                                Par la mise à disposition <b>gratuite et libre</b> de ce logiciel, j'espère obtenir des <b>retours d'utilisateurs</b> sans lesquels le développement et le maintien ne pourra perdurer. N'hésitez donc pas à me contacter <a href="/contact?request=chronoPhys">ici</a> pour tout bug rencontré ou idée d'amélioration.
                            </Alert>

                        </Col>
                        <Col>
                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <H2>Installation</H2>
                            </ScrollAnimation>
                            <div className='mb-3'>
                                Les versions exécutables de ChronoPhys sont dites "portables" et ne requièrent <b>pas d'installation à proprement parler</b>. Elles peuvent être transférées d'un ordinateur à un autre et ne requièrent pas l'installation de composantes supplémentaires (codecs, librairies ou même Python...).
                            </div>

                            <CollapsibleAlert title="Windows" variant="secondary" icon={['fab', 'windows']}>
                                <ul>
                                    <li>Télécharger l'exécutable : <a href="https://nuage02.apps.education.fr/index.php/s/KqQPzMzQDSfmLEs">ici</a>&nbsp;;</li>
                                    <li>Double-cliquer sur l'exécutable pour lancer le logiciel.</li>
                                </ul>
                            </CollapsibleAlert>

                            <CollapsibleAlert title="Linux" variant="secondary" icon={['fab', 'linux']}>
                                <ul>
                                    <li>Télécharger l'exécutable : <a href="https://nuage02.apps.education.fr/index.php/s/jez8m34DCEwY4d6">ici</a>&nbsp;;</li>
                                    <li>Extraire l'archive puis se rendre dans le dossier extrait&nbsp;;</li>
                                    <li>Double-cliquer sur l'exécutable "ChronoPhys" pour lancer le logiciel.</li>
                                </ul>
                                <hr />
                                Si vous souhaitez créer un raccourci (au format <code>.desktop</code>) :
                                <ul>
                                    <li>Ouvrir une console depuis le dossier contenant le fichier <code>install.sh</code> puis entrer la commande <code>sh ./install.sh</code>&nbsp;;</li>
                                    <li>Le raccourci ChronoPhys devrait apparaître depuis votre lanceur d'application.</li>
                                </ul>
                            </CollapsibleAlert>

                            <CollapsibleAlert title="Depuis les sources" variant="secondary" icon={['fab', 'python']}>
                                <ul>
                                    <li>Installer Python 3.12. Plusieurs libraires seront requises pour l'execution du script. À vous de les installer (à l'aide de la commande <code>pip3 install nom_de_la_librairie</code> par exemple) :
                                        <ul>
                                            <li>pyperclip</li>
                                            <li>numpy</li>
                                            <li>matplotlib</li>
                                            <li>PyQt6</li>
                                            <li>opencv-python</li>
                                            <li>flask</li>
                                            <li>qrcode</li>
                                            <li>qtawesome</li>
                                            <li>darkdetect</li>
                                        </ul>
                                    </li>
                                    <li>Télécharger l'archive <a href="https://forge.apps.education.fr/thibaultgiauffret/chronophys">ici</a>&nbsp;;</li>
                                    <li>Extraire l'archive puis se rendre dans le dossier extrait&nbsp;;</li>
                                    <li>Ouvrir une console depuis ce dossier puis entrer la commande <code>python3 main.py</code>.</li>
                                </ul>
                            </CollapsibleAlert>

                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <H2>Billet de blog</H2>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className='mb-3'>
                                <SimpleBlogCard title={blogPost.title} author={blogPost.author} date={blogPost.date} href={"/blog/read?post=" + blogPost.id}>
                                    {/* Safe ? */}
                                    {blogPost.content}
                                </SimpleBlogCard>
                            </ScrollAnimation>
                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}


export default Chronophys;