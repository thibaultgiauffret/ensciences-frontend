// ----------------------------
// Ressources page
// ----------------------------

// Components
import Header from '../components/header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { H2 } from '../components/sections'
import { PageContainer, FullContainer } from '../components/containers';
import { FileCardByPath } from '../components/cards';
// Functions
import { useState, useEffect } from 'react';
import { getFileInfos } from '../functions/files/getFileInfos';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas)
library.add(fab)

// CSS
import "animate.css/animate.compat.css"


function Ressources(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Get user infos
    const [connected, setConnected] = useState(false);
    useEffect(() => {
        getUserInfos().then((data) => {
            if (data.data !== undefined) {
                setConnected(true);
            }
        });
    }, []);

    // Set the different states
    const files: any = [
        {
            icon: "file-alt",
            path: "cours/ressources/renseignements.pdf",
            title: "Fiche de renseignements",
            description: "La fiche de renseignements que je distribue en début d'année.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "cours/ressources/regles.pdf",
            title: "Règles de vie de classe",
            description: "Un rappel de l'ensemble des règles de vie de classe.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "cours/ressources/securite.pdf",
            title: "Sécurité au laboratoire",
            description: "Un rappel de l'ensemble des règles de sécurité au laboratoire.",
            buttons: []
        }
    ];
    const [filesData, setFilesData] = useState<any[]>([]);

    // Get the filesData
    useEffect(() => {
        const fetchData = async () => {
            const updatedFiles = await Promise.all(files.map(async (file: any) => {
                const data = await getFileInfos(file.path);
                if (data.status === 'error') {
                    return file;
                } else {
                    file.buttons.push({
                        id: 1,
                        title: "Consulter",
                        name: "Consulter",
                        icon: "eye",
                        color: "primary",
                        path: "/view?direct=" + file.path,
                        date: data.data.date,
                        size: data.data.size,
                        enabled: data.data.enabled
                    });
                    return file;
                }
            }));
            setFilesData(updatedFiles);
        };

        fetchData();
    }, []);


    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row>
                        <Col xs={12} md={8}>
                            <H2>Gestion de classe</H2>

                            <FileCardByPath path="cours/ressources/renseignements.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="cours/ressources/regles.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="cours/ressources/securite.pdf" filesData={filesData} connected={connected} />
                            
                        </Col>
                        <Col>

                        </Col>
                    </Row>

                </FullContainer>
            </PageContainer>
        </>
    );
}

export default Ressources;