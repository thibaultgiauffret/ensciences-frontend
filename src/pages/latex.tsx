// ----------------------------
// LaTeX page
// ----------------------------

// Components
import Header from '../components/header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { H2 } from '../components/sections'
import { PageContainer, FullContainer } from '../components/containers';
import { FileCardByPath } from '../components/cards';
// Functions
import { useState, useEffect } from 'react';
import { getFileInfos } from '../functions/files/getFileInfos';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas)
library.add(fab)

// CSS
import "animate.css/animate.compat.css"


function LaTeX(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Get user infos
    const [connected, setConnected] = useState(false);
    useEffect(() => {
        getUserInfos().then((data) => {
            if (data.data !== undefined) {
                setConnected(true);
            }
        });
    }, []);

    // Set the different states
    const files: any = [
        {
            icon: "file-alt",
            path: "outils/latex/documentation_extension_cours.pdf",
            title: "Documentation de l'extension de cours",
            description: "Documentation de l'extension de cours utilisée pour rédiger des documents LaTeX.",
            buttons: []
        },
        {
            icon: "box",
            path: "outils/latex/extension_cours.zip",
            title  : "Extension de cours",
            description: "Archive contenant l'extension de cours utilisée pour rédiger des documents LaTeX, avec sa documentation et un exemple.",
            buttons: []
        }
    ];
    const [filesData, setFilesData] = useState<any[]>([]);

    // Get the filesData
    useEffect(() => {
        const fetchData = async () => {
            const updatedFiles = await Promise.all(files.map(async (file: any) => {
                const data = await getFileInfos(file.path);
                if (data.status === 'error') {
                    return file;
                } else {
                    file.buttons.push({
                        id: 1,
                        title: "Consulter",
                        name: "Consulter",
                        icon: "eye",
                        color: "primary",
                        path: "/view?direct=" + file.path,
                        date: data.data.date,
                        size: data.data.size,
                        enabled: data.data.enabled
                    });
                    return file;
                }
            }));
            setFilesData(updatedFiles);
        };

        fetchData();
    }, []);


    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <H2>Extension pour rédiger des documents de cours</H2>
                    <Row>
                        <Col xs={12} md={8}>


                            <iframe src="/view?direct=outils/latex/documentation_extension_cours.pdf" width="100%" height="600px"></iframe>

                        </Col>
                        <Col>
                            <FileCardByPath path="outils/latex/documentation_extension_cours.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="outils/latex/extension_cours.zip" filesData={filesData} connected={connected} />
                        </Col>
                    </Row>

                </FullContainer>
            </PageContainer>
        </>
    );
}

export default LaTeX;