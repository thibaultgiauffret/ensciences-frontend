// ----------------------------
// Blog page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, InputGroup, Form, Button, Spinner, Badge, Pagination, Alert } from 'react-bootstrap';
import { BlogPostCard } from '../components/blog/blog';
import { H2 } from '../components/sections';

// Functions
import { useState, useEffect } from 'react';
import { getBlogPosts, getBlogCategories, getBlogYears } from '../functions/blog/getBlogPosts';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)



function Blog(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Set the different states
    const [userIsAdmin, setUserIsAdmin] = useState(false);
    const [blogPosts, setBlogPosts] = useState<BlogPostData[]>([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [badge, setBadge] = useState('');
    const [firstLoad, setFirstLoad] = useState(true);

    // Get the blog posts list
    useEffect(() => {
        getBlogPosts(currentPage).then((response: any) => {
            if (response.status === 'success') {
                setBlogPosts(response.blogPosts);
                setTotalPages(response.totalPages);
                setFirstLoad(false);
            }
        }).catch(() => {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la récupération des billets de blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        })

        // Get user infos
        getUserInfos().then((response: any) => {
            if (response.data) {
                if (response.data.role.includes('administrator')) {
                    setUserIsAdmin(true);
                }
            }
        });
    }, []);

    // Get the years list
    const [years, setYears] = useState<{ year: number, count: number }[]>([]);
    useEffect(() => {
        getBlogYears().then((response: any) => {
            if (response.status === 'success') {
                setYears(response.years);
            }
        }).catch(() => {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la récupération des années des billets de blog.',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
    }, []);

    // Get the categories list
    const [categories, setCategories] = useState<{ id: string, name: string }[]>([]);
    useEffect(() => {
        getBlogCategories().then((response: any) => {
            if ((response as { status: string }).status === 'success') {
                setCategories(response.categories);
            }
        }).catch(() => {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la récupération des catégories des billets de blog.',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
    }, []);


    const searchBlogPosts = ({ search = '', category = {}, year = '' }: { search?: string, category?: any, year?: string }) => {
        getBlogPosts(currentPage, search, category.id, year).then((response: any) => {
            if (response.status === 'success') {
                setBlogPosts(response.blogPosts);
                setTotalPages(response.totalPages);
                if (category.name) {
                    setBadge(category.name);
                }
                if (year !== '') {
                    setBadge(year);
                }
            }
        });
    }

    // Search bar
    const [search, setSearch] = useState('');
    useEffect(() => {
        const delayDebounceFn = setTimeout(() => {
            searchBlogPosts({ search });
            if (search !== '') {
                setBadge('Recherche');
            } else {
                setBadge('');
            }
        }, 400);

        return () => clearTimeout(delayDebounceFn);
    }, [search]);

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        searchBlogPosts({ search });
    };


    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        {/* Blog posts */}
                        <Col xs={12} md={8}>

                            <p>Cette page regroupe tous les billets de blog publiés portant sur <b>divers sujets autour de l'enseignement et du numérique</b>. Vous y trouverez du traitement de texte et d'images, des objets connectés, des microcontrôleurs, différents languages de programmation...

                                Un <b>espace de commentaires</b> est mis à disposition afin de partager vos retours/commentaires.</p>

                            <H2>
                                Billets de Blog
                                {
                                    badge !== '' ? (
                                        <Badge bg="primary" className='ms-2' style={{
                                            cursor: 'pointer',
                                            fontSize: '1rem'
                                        }}>
                                            {badge}
                                            {/* Close btn */}
                                            <a className='btn-close ms-2'
                                                onClick={() => {
                                                    searchBlogPosts({ search: '' })
                                                    setBadge('')
                                                    setSearch('')
                                                }}>
                                            </a>
                                        </Badge>
                                    ) : ''
                                }
                            </H2>


                            <Form className='d-block d-md-none'>
                                <InputGroup className="mb-3">
                                    <Form.Control
                                        type="text"
                                        placeholder="Rechercher un article"
                                        value={search}
                                        onChange={(e) => setSearch(e.target.value)}
                                    />
                                    <Button variant="outline-secondary" id="button-addon2">
                                        <FontAwesomeIcon icon="search" />
                                    </Button>
                                </InputGroup>
                            </Form>

                            <Row className='my-3' xs={1} md={1} lg={2} xl={3}>
                                {blogPosts.length > 0
                                    ? blogPosts.map((BlogPost: BlogPostData, index: number) => {
                                        return (
                                            <Col>
                                                <BlogPostCard key={index} data={BlogPost} />
                                            </Col>
                                        );
                                    }) : (
                                        firstLoad && (
                                            <>
                                                {Array.from({ length: 8 }).map((_, index) => (
                                                    <Col key={index}>
                                                        <BlogPostCard data={{
                                                            id: 0,
                                                            title: '',
                                                            content: '',
                                                            excerpt: '',
                                                            date: '',
                                                            post_categories: [],
                                                            author: '',
                                                            link: '',

                                                        }} />
                                                    </Col>
                                                ))}
                                            </>
                                        )
                                    )}
                            </Row>

                            {/* No blog posts */}
                            {blogPosts.length === 0 && !firstLoad && (
                                <Alert variant='warning'>
                                    <FontAwesomeIcon icon='exclamation-triangle' />&nbsp;Aucun billet de blog trouvé...
                                </Alert>
                            )}

                            {/* Pagination */}
                            <Paginations numberOfPages={totalPages} activePage={currentPage} changePage={(page) => {
                                setCurrentPage(page);
                                getBlogPosts(page, search).then((response: any) => {
                                    if (response.status === 'success') {
                                        setBlogPosts(response.blogPosts);
                                    }
                                });
                            }} />

                        </Col>
                        {/* Search bar, categories and years */}
                        <Col>
                            {userIsAdmin && (
                                <Button variant='warning' href='https://wordpress.ensciences.fr/wp-admin/post-new.php' target='_blank' className='mb-3'>
                                    <FontAwesomeIcon icon='plus' />
                                    &nbsp;Ajouter un billet de blog
                                </Button>
                            )}
                            <Form className='d-none d-md-block' onSubmit={handleSubmit}>
                                <InputGroup className="mb-3">
                                    <Form.Control
                                        type="text"
                                        placeholder="Rechercher un billet"
                                        value={search}
                                        onChange={(e) => setSearch(e.target.value)}
                                    />
                                    <Button variant="outline-secondary" id="button-addon2" type='submit'>
                                        <FontAwesomeIcon icon="search" />
                                    </Button>
                                </InputGroup>
                            </Form>

                            <H2>Categories</H2>


                            {categories.length > 0 ? (
                                <ul>
                                    {categories.map((category, index) => {
                                        return (
                                            <li>
                                                <a key={index} onClick={() => searchBlogPosts({ category: category })} className='primary' style={{ cursor: 'pointer' }}>
                                                    {category.name}
                                                </a>
                                            </li>
                                        );
                                    })}
                                </ul>
                            ) : (
                                <div className='d-flex justify-content-center align-items-center'>
                                    {/* Loading */}
                                    <Spinner animation="border" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </Spinner>
                                </div>
                            )}



                            <H2>Années</H2>
                            {years.length > 0 ? (
                                <ul>
                                    {years.map((year, index) => (
                                        <li>
                                            <a key={index} onClick={() => searchBlogPosts({ year: year.year.toString() })} className='primary' style={{ cursor: 'pointer' }}>
                                                {year.year} ({year.count})
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            ) : (
                                <div className='d-flex justify-content-center align-items-center'>
                                    {/* Loading */}
                                    <Spinner animation="border" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </Spinner>
                                </div>
                            )}
                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}

// Pagination component
function Paginations({ numberOfPages, activePage, changePage }: { numberOfPages: number, activePage: number, changePage: (page: number) => void }) {
    return (
        <Pagination size="sm">
            <Pagination.Prev onClick={() => {
                if (activePage > 1) {
                    changePage(activePage - 1);
                }
            }} />
            {Array.from(Array(numberOfPages).keys()).map((page) => {
                return <Pagination.Item key={page} active={page === activePage - 1} onClick={() => {
                    changePage(page + 1);
                }}
                >{page + 1}</Pagination.Item>;
            })}
            <Pagination.Next onClick={() => {
                if (activePage < numberOfPages) {
                    changePage(activePage + 1);
                }
            }} />
        </Pagination>
    );
}

export default Blog;