// ----------------------------
// Admin page
// ----------------------------

// Imports
import Header from '../components/header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';
import { H2 } from '../components/sections';
import { PageContainer, FullContainer } from '../components/containers';
import ScrollAnimation from 'react-animate-on-scroll';
import { Alert, Button, Card, Modal, Form, InputGroup, Table, Pagination, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DOMPurify from 'dompurify';
import FilesManager from '../components/files/filesManager';

// CSS
import "animate.css/animate.compat.css";
import cardStyles from '../components/css/cards.module.css';
import styles from './css/admin.module.css';

// Functions
import { useState, useEffect, useRef } from 'react';
import { getMessages } from '../functions/messages/getMessages';
import { markAsRead, deleteMessage } from '../functions/messages/updateMessage';
import { getUrls } from '../functions/shorturls/getUrls';
import { useToasts } from 'react-bootstrap-toasts';
import { addUrl } from '../functions/shorturls/addUrl';
import { deleteUrl } from '../functions/shorturls/deleteUrl';
import { updateUrl } from '../functions/shorturls/updateUrl';
import { getUserInfos } from '../functions/users/getUserInfos';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(fas)
library.add(fab)

function Admin(props: { title: string, subtitle?: string }) {

  // State
  const [messages, setMessages] = useState([]);
  const [refreshMessages, setRefreshMessages] = useState(0);
  const [audienceTokenAuth, setAudienceTokenAuth] = useState('');
  const [showUrlModal, setShowUrlModal] = useState(false);
  const [urls, setUrls] = useState([]);
  const [refreshUrls, setRefreshUrls] = useState(0);
  const [isAdministrator, setIsAdministrator] = useState(false);
  //@ts-ignore
  const [filesModified, setFilesModified] = useState(false);

  // Set the title
  useEffect(() => {
    const setTitle = () => {
      document.title = props.title + " | EnSciences";
    };
    setTitle();
  }, []);

  // Get the user infos
  useEffect(() => {
    getUserInfos().then((data: any) => {
      if (data.data.role.includes('administrator')) {
        setIsAdministrator(true);
      }
    });
  }, []);

  // Get the messages
  useEffect(() => {
    if (isAdministrator === false) {
      return;
    }
    getMessages().then((data: any) => {
      if (data.status === 'success') {
        setMessages(data.data);
      } else {
        toasts.show({
          headerContent: 'Erreur',
          bodyContent: 'getMessages : ' + data.message,
          toastProps: {
            bg: 'danger',
            autohide: true,
            delay: 5000,
          }
        });
      }
    });
  }, [refreshMessages, isAdministrator]);

  // Get the URLs
  useEffect(() => {
    if (isAdministrator === false) {
      return;
    }
    getUrls().then((data: any) => {
      if (data.status === 'success') {
        setUrls(data.data);
      } else {
        toasts.show({
          headerContent: 'Erreur',
          bodyContent: 'getUrls : ' + data.message,
          toastProps: {
            bg: 'danger',
            autohide: true,
            delay: 5000,
          }
        });
      }
    });
  }, [refreshUrls, isAdministrator]);

  const handleRefreshUrls = () => {
    setRefreshUrls(prev => prev + 1);
  };

  // Get the Matomo token_auth
  useEffect(() => {
    if (isAdministrator === false) {
      return;
    }
    const getMatomoTokenAuth = async () => {
      const response = await fetch(import.meta.env.VITE_API_URL + 'audiences/token_auth.php', {
        method: 'GET',
        credentials: 'include'
      });
      const responseData = await response.json();
      if (responseData.status === 'success') {
        setAudienceTokenAuth(responseData.token);
      } else {
        toasts.show({
          headerContent: 'Erreur',
          bodyContent: 'getMatomoTokenAuth : ' + responseData.message,
          toastProps: {
            bg: 'danger',
            autohide: true,
            delay: 5000,
          }
        });
      }
    };
    getMatomoTokenAuth();
  }, [isAdministrator]);

  // Adapt iframe height to content
  const iframeRef = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    const handleIframeLoad = () => {
      const iframe = iframeRef.current!;
      if (iframe && iframe.contentWindow) {
        iframe.style.height = iframe.contentWindow.document.body.scrollHeight + 'px';
      }
    };

    const iframe = iframeRef.current as HTMLIFrameElement;
    if (iframe) {
      iframe.addEventListener('load', handleIframeLoad);
    }

    return () => {
      if (iframe) {
        iframe.removeEventListener('load', handleIframeLoad);
      }
    };
  }, []);

  // Handle the messages
  const updateMarkAsRead = (id: string, status: boolean) => {
    markAsRead(id, status).then((data: any) => {
      if (data.status === 'success') {
        let message = "Le message a été marqué comme lu.";
        if (!status) {
          message = "Le message a été marqué comme non lu.";
        }
        toasts.show({
          headerContent: 'Succès',
          bodyContent: message,
          toastProps: {
            bg: 'success',
            autohide: true,
            delay: 5000,
          }
        });
        // Refresh the messages
        setRefreshMessages(refreshMessages + 1);
      } else {
        toasts.show({
          headerContent: 'Erreur',
          bodyContent: data.message,
          toastProps: {
            bg: 'danger',
            autohide: true,
            delay: 5000,
          }
        });
      }
    });
  };

  // Toasts
  const toasts = useToasts();


  return (
    <>
      <Header title={props.title} subtitle={props.subtitle}>
      </Header>
      <PageContainer>
        <FullContainer>
          {isAdministrator === false &&
            <Alert variant="danger" className='mt-3'>
              <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Vous n'êtes pas autorisé à accéder à cette page.
            </Alert>
          }

          {isAdministrator === true &&

            <>
              <Row>
                <Col xs={12} md={12} lg={8} className="mb-3">
                  <H2>Messagerie</H2>

                  {/* Card with two tabs in header to show the messages*/}
                  <Card className={cardStyles.card}>
                    <Card.Body className={styles.cardMessages}>
                      {messages.length === 0 &&
                        <div className={styles.message}>
                          <div className="d-flex justify-content-between align-items-center flex-wrap">
                            <b>Aucun message</b>
                          </div>
                        </div>
                      }
                      {messages.slice().reverse().map((message: any) => {
                        const sanitizedMessageText = DOMPurify.sanitize(message.message_text.replace(/\\n\\n/g, '<br />'));
                        return (
                          <div key={message.message_id} className={styles.message}>
                            <div className="d-flex justify-content-between align-items-center flex-wrap">
                              <b>Message n°{message.message_id}
                                {message.message_status === 1 &&
                                  <Badge className="ms-2">Non lu</Badge>
                                }
                              </b>
                              <span className="d-flex flex-wrap justify-content-center small">
                                <Button variant="primary" size="sm" className="m-1" href={'mailto:' + message.message_mail + '?subject=' + message.message_object + '&body=' + message.message_text}>
                                  <FontAwesomeIcon icon={['fas', 'reply']} />
                                </Button>
                                {message.message_status === 1 ? (
                                  <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                    updateMarkAsRead(message.message_id, true);
                                  }}> <FontAwesomeIcon icon={['fas', 'eye']} />
                                  </Button>) : (
                                  <Button variant="secondary" size="sm" className="m-1" onClick={() => {
                                    updateMarkAsRead(message.message_id, false);
                                  }}>
                                    <FontAwesomeIcon icon={['fas', 'eye-slash']} />
                                  </Button>
                                )}


                                <Button variant="danger" size="sm" className="m-1" onClick={() => {
                                  // Delete the message
                                  deleteMessage(message.message_id).then((data: any) => {
                                    if (data.status === 'success') {
                                      toasts.show({
                                        headerContent: 'Succès',
                                        bodyContent: 'Le message a été supprimé.',
                                        toastProps: {
                                          bg: 'success',
                                          autohide: true,
                                          delay: 5000,
                                        }
                                      });
                                      // Refresh the messages
                                      setRefreshMessages(refreshMessages + 1);
                                    } else {
                                      toasts.show({
                                        headerContent: 'Erreur',
                                        bodyContent: data.message,
                                        toastProps: {
                                          bg: 'danger',
                                          autohide: true,
                                          delay: 5000,
                                        }
                                      });
                                    }
                                  });
                                }}>
                                  <FontAwesomeIcon icon={['fas', 'trash-alt']} />
                                </Button>
                              </span>
                            </div>
                            <b>De&nbsp;:&nbsp;</b>{message.message_author}&nbsp;({message.message_mail})<br />
                            <b>Objet&nbsp;:&nbsp;</b>{message.message_object}<br />
                            <div className="small" dangerouslySetInnerHTML={{ __html: sanitizedMessageText }}></div>
                          </div>
                        );
                      })}
                    </Card.Body>
                  </Card>

                </Col>
                <Col className="mb-3">
                  <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                    <H2>Outils</H2>
                    {/* CPanel */}
                    <Button variant="secondary" className="m-1 w-100" href="https://cpanel.ensciences.fr/">
                      <div className="d-flex justify-content-between align-items-center">
                        <span className='d-flex align-items-center'>
                          <FontAwesomeIcon icon={['fas', 'wrench']} />&nbsp;Administration du serveur
                        </span>
                        <Badge bg="light" style={{ color: "black" }} className='ms-3'>CPanel</Badge>
                      </div>
                    </Button>
                    {/* BlogAdmin */}
                    <Button variant="secondary" className="m-1 w-100" href="https://www.blog.ensciences.fr/wp-admin/">
                      <div className="d-flex justify-content-between align-items-center">
                        <span className='d-flex align-items-center'>
                          <FontAwesomeIcon icon={['fab', 'wordpress']} />&nbsp;Administration du blog
                        </span>
                        <Badge bg="light" style={{ color: "black" }} className='ms-3'>WordPress</Badge>
                      </div>
                    </Button>
                    {/* Audiences */}
                    <Button variant="secondary" className="m-1 w-100" href="https://www.analytics.ensciences.fr/">
                      <div className="d-flex justify-content-between align-items-center">
                        <span className='d-flex align-items-center'>
                          <FontAwesomeIcon icon={['fas', 'chart-line']} />&nbsp;Administration des audiences
                        </span>
                        <Badge bg="light" style={{ color: "black" }} className='ms-3'>Matomo</Badge>
                      </div>
                    </Button>
                    {/* Webmail Roundcube */}
                    <Button variant="secondary" className="m-1 w-100" href="https://webmail.ensciences.fr/">
                      <div className="d-flex justify-content-between align-items-center">
                        <span>
                          <FontAwesomeIcon icon={['fas', 'envelope']} />&nbsp;Webmail
                        </span>
                        <Badge bg="light" style={{ color: "black" }} className='ms-3'>Roundcube</Badge>
                      </div>
                    </Button>
                  </ScrollAnimation>

                  <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                    <H2>Gestion des fichiers</H2>
                    {/* File Manager */}
                    <Button variant="secondary" className="m-1 w-100" href="https://filemanager.ensciences.fr/">
                      <div className="d-flex justify-content-between align-items-center">
                        <span>
                          <FontAwesomeIcon icon={['fas', 'folder-open']} />&nbsp;Gestionnaire de fichiers
                        </span>
                      </div>
                    </Button>
                    {/* URL Shorcuts */}
                    <Button variant="secondary" className="m-1 w-100" onClick={() => setShowUrlModal(true)}>
                      <div className="d-flex justify-content-between align-items-center">
                        <span>
                          <FontAwesomeIcon icon={['fas', 'link']} />&nbsp;Raccourcisseur d'URL
                        </span>
                      </div>
                    </Button>

                    {/* Modal */}
                    <Modal show={showUrlModal} onHide={() => setShowUrlModal(false)} size='lg'>
                      <Modal.Header closeButton>
                        <Modal.Title>Raccourcisseur d'URL</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>

                        {/* Form to add URL */}
                        <Form onSubmit={(e) => {
                          e.preventDefault();
                          const target = e.target as typeof e.target & {
                            file: { value: string };
                            url: { value: string };
                          };
                          addUrl(target.file.value, target.url.value).then((data: any) => {
                            if (data.status === 'success') {
                              toasts.show({
                                headerContent: 'Succès',
                                bodyContent: 'L\'URL a été ajoutée.',
                                toastProps: {
                                  bg: 'success',
                                  autohide: true,
                                  delay: 5000,
                                }
                              });
                              // Refresh the URLs
                              handleRefreshUrls();
                            } else {
                              toasts.show({
                                headerContent: 'Erreur',
                                bodyContent: data.message,
                                toastProps: {
                                  bg: 'danger',
                                  autohide: true,
                                  delay: 5000,
                                }
                              });
                            }
                          });
                        }}>
                          <InputGroup className="mb-3" size="sm">
                            <InputGroup.Text>
                              <FontAwesomeIcon icon={['fas', 'quote-left']} />
                            </InputGroup.Text>
                            <Form.Control type="text" placeholder="Raccourci" name="file" />
                          </InputGroup>

                          <InputGroup className="mb-3" size="sm">
                            <InputGroup.Text>
                              <FontAwesomeIcon icon={['fas', 'link']} />
                            </InputGroup.Text>
                            <Form.Control type="text" placeholder="URL" name="url" />
                          </InputGroup>

                          <Button variant="primary" type="submit" size="sm" >
                            <FontAwesomeIcon icon={['fas', 'plus']} />&nbsp;Ajouter
                          </Button>
                        </Form>

                        <hr />

                        {urls &&
                          <UrlManager data={urls} handleRefreshUrls={handleRefreshUrls} />
                        }

                      </Modal.Body>
                    </Modal>
                  </ScrollAnimation>
                </Col>
              </Row>

              <H2>Fichiers</H2>

              <Card className={cardStyles.card + " p-3"}>
                <FilesManager folder="/files" setModified={setFilesModified} />
              </Card>

              <H2>Audiences</H2>
              <Card className={cardStyles.card + " mb-2"}>
                <iframe ref={iframeRef} src={audienceTokenAuth && "https://www.analytics.ensciences.fr/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=1&period=week&date=today&token_auth=" + audienceTokenAuth} className={styles.iframe}></iframe>
              </Card>
              <Alert variant="info">
                Si le dashboard ne charge pas, penser à désactiver le bloquer de publicité pour cette page et Matomo (<code>www.ensciences.fr/admin</code> et <code>www.analytics.ensciences.fr/</code>) !
              </Alert>
            </>
          }
        </FullContainer>
      </PageContainer >
    </>
  );
}

// Files manager table
function UrlManager({ data, handleRefreshUrls }: { data: any, handleRefreshUrls: any }) {

  // Toasts
  const toasts = useToasts();

  // Files and other state
  const [urls, setUrls] = useState<UrlManagerData[]>([]);
  const [page, setPage] = useState(1);
  const [numberPerPage, setNumberPerPage] = useState(10);
  const [numberOfPages, setNumberOfPages] = useState(0);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('name');
  const [desc, setDesc] = useState(false);

  useEffect(() => {
    setUrls(data);
  }, []);

  useEffect(() => {
    // Calculate the number of pages
    const newNumberOfPages = getNumberOfPages(urls.filter((url) => url.file.toLowerCase().includes(search.toLowerCase()) || url.url.toLowerCase().includes(search.toLowerCase())), numberPerPage);
    setNumberOfPages(newNumberOfPages);

    // If the page is higher than the number of pages, set it to the last page
    if (page > newNumberOfPages) {
      setPage(newNumberOfPages > 0 ? newNumberOfPages : 1);
    }

    // Sort the files according to the sort and desc states
    urls.sort((a, b) => {
      const multi = desc === false ? 1 : -1;
      switch (sort) {
        case 'file':
          return a.file.localeCompare(b.file) * multi;
        case 'url':
          return a.url.localeCompare(b.url) * multi;
        default:
          return 0;
      }
    });
  }, [urls, numberPerPage, page, search, sort, desc, data]);

  // Handle the sort of the table
  const handleSort = (sortName: string) => {
    if (sort === sortName) {
      setDesc(!desc);
    } else {
      setDesc(false);
    }
    setSort(sortName);
  }

  return (
    <>
      <Row>
        <Col>
          {/* Search bar */}
          <InputGroup className="mb-3" size="sm">
            <InputGroup.Text><FontAwesomeIcon icon="search" /></InputGroup.Text>
            <Form.Control type="text" placeholder="Rechercher un fichier..." value={search} onChange={(e) => {
              setSearch(e.target.value);
              setPage(1);
            }} />
            {/* Clear button when search is not empty */}
            {search !== '' && <Button variant="secondary" onClick={() => {
              setSearch('');
              setPage(1);
            }}>
              <FontAwesomeIcon icon="times" />
            </Button>}
          </InputGroup>
        </Col>
        <Col>
          {/* Select numberPerPage */}
          <InputGroup className="mb-3" size="sm">
            <InputGroup.Text><FontAwesomeIcon icon="list" /></InputGroup.Text>
            <Form.Select onChange={(e) => {
              setNumberPerPage(parseInt(e.target.value));
              setPage(1);
            }}>
              <option>10</option>
              <option>25</option>
              <option>50</option>
              <option>100</option>
            </Form.Select>
          </InputGroup>
        </Col>
      </Row>
      {/* Table */}
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>Nom
              {/* Sort buttons */}
              <OverlayTrigger
                placement="top"
                overlay={
                  <Tooltip id="sortTooltip">
                    Trier par nom
                  </Tooltip>
                }
              >
                <Button variant="link" size="sm" className="ms-2" onClick={() => {
                  handleSort('name');

                }}>
                  {sort === "name" && desc === true ? <FontAwesomeIcon icon="sort-alpha-down" /> : <FontAwesomeIcon icon="sort-alpha-up" />}
                </Button>
              </OverlayTrigger>
            </th>
            <th>URL </th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {/* Display the files according to the search and pagination */}
          {urls.filter((url) => url.file.toLowerCase().includes(search.toLowerCase()) || url.url.toLowerCase().includes(search.toLowerCase())).slice((page - 1) * numberPerPage, page * numberPerPage).map((url) => (
            // File row
            <tr key={url.id}>
              <td>
                <Form.Control type="text" value={url.file} onChange={
                  (e) => {
                    const newUrls = urls.map((u) => {
                      if (u.id === url.id) {
                        return { ...u, file: e.target.value };
                      } else {
                        return u;
                      }
                    });
                    setUrls(newUrls);
                  }
                } />
              </td>
              <td>
                <Form.Control type="text" value={url.url} onChange={
                  (e) => {
                    const newUrls = urls.map((u: any) => {
                      if (u.id === url.id) {
                        return { ...u, url: e.target.value };
                      } else {
                        return u;
                      }
                    });
                    setUrls(newUrls);
                  }
                } />
              </td>
              <td>
                {/* Apply button */}
                <OverlayTrigger placement="top" overlay={
                  <Tooltip id="applyTooltip">
                    Appliquer les modifications
                  </Tooltip>
                }>
                  <Button variant="primary" size="sm" className="me-2" onClick={() => {
                    // Get the value of the URL and file
                    const file = urls.find((u: any) => u.id === url.id)?.file;
                    const urlValue = urls.find((u: any) => u.id === url.id)?.url;
                    // Update the URL
                    updateUrl(url.id, file ?? '', urlValue ?? '').then((data: any) => {
                      if (data.status === 'success') {
                        toasts.show({
                          headerContent: 'Succès',
                          bodyContent: 'L\'URL a été mise à jour.',
                          toastProps: {
                            bg: 'success',
                            autohide: true,
                            delay: 5000,
                          }
                        });
                        // Refresh the URLs
                        handleRefreshUrls();
                      } else {
                        toasts.show({
                          headerContent: 'Erreur',
                          bodyContent: data.message,
                          toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                          }
                        });
                      }
                    }
                    );
                  }}>
                    <FontAwesomeIcon icon="check" />
                  </Button>
                </OverlayTrigger>
                {/* Copy button */}
                <OverlayTrigger placement="top" overlay={
                  <Tooltip id="applyTooltip">
                    Copier l'URL : https://url.ensciences.fr/{url.file}
                  </Tooltip>
                }>
                  <Button variant="secondary" size="sm" className="me-2" onClick={() => {
                    // Copy the URL to the clipboard
                    navigator.clipboard.writeText('https://url.ensciences.fr/' + url.file).then(() => {
                      toasts.show({
                        headerContent: 'Succès',
                        bodyContent: 'L\'URL a été copiée.',
                        toastProps: {
                          bg: 'success',
                          autohide: true,
                          delay: 5000,
                        }
                      });
                    }).catch(() => {
                      toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Impossible de copier l\'URL.',
                        toastProps: {
                          bg: 'danger',
                          autohide: true,
                          delay: 5000,
                        }
                      });
                    });
                  }}>
                    <FontAwesomeIcon icon="copy" />
                  </Button>
                </OverlayTrigger>
                {/* Delete button */}
                <OverlayTrigger placement="top" overlay={
                  <Tooltip id="applyTooltip">
                    Supprimer l'URL
                  </Tooltip>
                }>
                  <Button variant="danger" size="sm" className="me-2" onClick={() => {
                    deleteUrl(url.id).then((data: any) => {
                      if (data.status === 'success') {
                        toasts.show({
                          headerContent: 'Succès',
                          bodyContent: 'L\'URL a été supprimée.',
                          toastProps: {
                            bg: 'success',
                            autohide: true,
                            delay: 5000,
                          }
                        });
                        // Refresh the URLs
                        handleRefreshUrls();
                      } else {
                        toasts.show({
                          headerContent: 'Erreur',
                          bodyContent: data.message,
                          toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                          }
                        });
                      }
                    });
                  }}>
                    <FontAwesomeIcon icon="trash" />
                  </Button>
                </OverlayTrigger>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Paginations numberOfPages={numberOfPages} activePage={page} changePage={setPage} />
    </>
  );
}

// Get the number of pages
function getNumberOfPages(json: UrlManagerData[], numberPerPage: number) {
  return Math.ceil(json.length / numberPerPage);
}

// Pagination component
function Paginations({ numberOfPages, activePage, changePage }: { numberOfPages: number, activePage: number, changePage: (page: number) => void }) {
  return (
    <Pagination size="sm">
      <Pagination.Prev onClick={() => {
        if (activePage > 1) {
          changePage(activePage - 1);
        }
      }} />
      {Array.from(Array(numberOfPages).keys()).map((page) => {
        return <Pagination.Item key={page} active={page === activePage - 1} onClick={() => {
          changePage(page + 1);
        }}
        >{page + 1}</Pagination.Item>;
      })}
      <Pagination.Next onClick={() => {
        if (activePage < numberOfPages) {
          changePage(activePage + 1);
        }
      }} />
    </Pagination>
  );
}


// Export
export default Admin;
