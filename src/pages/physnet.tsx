// ----------------------------
// PhysNet page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, Button, Modal, Alert } from 'react-bootstrap';
import { BigCircleLink, ButtonLink } from '../components/buttons';
import { H2 } from '../components/sections';
import { SimpleBlogCard } from '../components/cards';
import ScrollAnimation from 'react-animate-on-scroll';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import ImageLoader from '../components/imageLoader';

// Functions
import { useState, useEffect } from 'react';
import { getBlogPost } from '../functions/blog/getBlogPost';

// CSS
import styles from './css/multicompile.module.css';
import "@fontsource/dosis/400.css";

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)



function Physnet(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();


    }, []);

    // Get the blog post n°524
    const [blogPost1, setBlogPost1] = useState({ id:'', title: '', author: '', date: '', content: '', link: '' });
    const [blogPost2, setBlogPost2] = useState({  id:'', title: '', author: '', date: '', content: '', link: '' });
    useEffect(() => {
        getBlogPost(1129).then((response: any) => {
            setBlogPost1(response.blogPost);
        }).catch((error) => {
            console.error(error);
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Impossible de récupérer le dernier article du blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
        getBlogPost(335).then((response: any) => {
            setBlogPost2(response.blogPost);
        }).catch((error) => {
            console.error(error);
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Impossible de récupérer le dernier article du blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
    }, []);

    // Modal
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return (
        <>
            <ImageLoader />
            <Header title={''} subtitle={props.subtitle} color="#34495e">
                <h1 className={styles.title + ' mt-5'}><span className={styles.lightTitle}>Phys</span><span className={styles.boldTitle}>Net</span></h1>
                <h3 className={styles.subtitle}>Application de classe virtuelle pour le partage de ressources en temps réel</h3>
                <div className={styles.titleContainer}>
                    <div className={'my-4'}>
                        {/* See */}
                        <Button variant="success" className="m-1" href="https://physnet.ensciences.fr" target="_blank">
                            <FontAwesomeIcon icon={['fas', 'chalkboard-teacher']} />&nbsp;Accéder à PhysNet
                        </Button>
                        {/* Download */}
                        <Button variant="primary" className="m-1" onClick={handleShow}>
                            <FontAwesomeIcon icon={['fas', 'download']} />&nbsp;Télécharger
                        </Button>

                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Téléchargement</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Alert variant="info">
                                    Pour l'instant, seules les sources sont disponibles au téléchargement.
                                </Alert>

                                <Row className="text-center">
                                    <Col>

                                        <ButtonLink className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Windows"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'windows']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Linux"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'linux']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink className={styles.downloadButton + ' m-1'} target="_blank">
                                            <FontAwesomeIcon icon={['fab', 'apple']} />
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink href="https://forge.apps.education.fr/thibaultgiauffret/physnet" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger les sources"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'gitlab']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                </Row>


                            </Modal.Body>
                        </Modal>

                        {/* Forum */}
                        <Button variant="secondary" className="m-1" disabled>
                            <FontAwesomeIcon icon={['fas', 'comments']} />&nbsp;Forum
                        </Button>
                    </div>
                    <div>
                        Bientôt disponible sur <FontAwesomeIcon icon={['fab', 'windows']} />&nbsp;Windows et <FontAwesomeIcon icon={['fab', 'linux']} />&nbsp;Linux ! Peut-être un jour sur <FontAwesomeIcon icon={['fab', 'apple']} />&nbsp;MacOs...
                    </div>

                    <div className={'my-4'}>
                        <img alt="PhysNet" className={styles.headImage} data-path="physnet/gui.png" />
                    </div>
                </div>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        <Col xs={12} md={8}>
                            <H2>Présentation</H2>

                            <Alert variant="warning">
                                <Alert.Heading>
                                    <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Attention !</Alert.Heading>
                                    Le projet PhysNet est encore en développement et <b>son utilisation en classe est déconseillée</b>. En effet, l'application n'est pas encore optimisée (en terme de performances et de sécurité).
                            </Alert>

                            PhysNet est une <b>application de salle de classe virtuelle</b> basée sur NodeJS. Elle permet le partage de ressources (fichiers, liens, quiz...) en temps réel avec les élèves.

                            <div className="text-center">
                                <img data-path="physnet/anim.gif" alt="PhysNet Exemple" className={styles.image + ' my-3'} />
                            </div>

                            <p>Le logiciel tend à répondre à plusieurs problématiques :</p>
                            <ul>
                                <li>Être indépendant des problèmes logistiques (architecture réseau de l’établissement...)&nbsp;;</li>
                                <li>Promouvoir l'utilisation de supports numériques nouveaux et variés en classe (animations JavaScript, QCM interactifs, vidéos, audios...)&nbsp;;</li>
                                <li>Favoriser les évaluations formatrices/formatives régulières à l'aide de quiz&nbsp;;</li>
                                <li>Communiquer des ressources complémentaires (messages, liens, fichier...) en temps réel.</li>
                            </ul>

                            <p>Pour plus d'information sur le fonctionnement et la mise en route de PhysNet, veuillez consulter l'article : <a href="/blog/read?post=335">Projet PhysNet (2) : évolution du projet et perspectives</a> </p>


                        </Col>
                        <Col>
                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <H2>Code source</H2>
                            </ScrollAnimation>
                            <div className="d-flex justify-content-center align-items-center">
                                <span>
                                    <BigCircleLink href="https://forge.apps.education.fr/thibaultgiauffret/physnet" icon={'gitlab'} color="#34495e" />
                                </span>
                                <span className="ms-3 text-left">Voir les sources sur la forge</span>
                            </div>
                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <H2>Billets de blog</H2>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20} className='mb-3'>
                                <SimpleBlogCard title={blogPost1.title} author={blogPost1.author} date={blogPost1.date} href={"/blog/read?post=" + blogPost1.id}>
                                    {/* Safe ? */}
                                    {blogPost1.content}
                                </SimpleBlogCard>
                            </ScrollAnimation>

                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <SimpleBlogCard title={blogPost2.title} author={blogPost2.author} date={blogPost2.date} href={"/blog/read?post=" + blogPost2.id}>
                                    {/* Safe ? */}
                                    {blogPost2.content}
                                </SimpleBlogCard>
                            </ScrollAnimation>
                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}


export default Physnet;