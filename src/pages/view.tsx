// ----------------------------
// File viewer page
// ----------------------------

// Components
import { Alert, Button, Container, Spinner, Card, Modal, InputGroup, Form } from 'react-bootstrap';
import { BigCircleButton } from '../components/buttons';
import HTMLFlipBook from "react-pageflip"
import { Document, Page } from 'react-pdf';
import { pdfjs } from 'react-pdf';
import QRCode from '../components/qrcode';

const baseUrl = import.meta.env.BASE_URL;
const workerSrc = baseUrl.endsWith('/')
    ? `${baseUrl}pdf.worker.min.js`
    : `${baseUrl}/pdf.worker.min.js`;

pdfjs.GlobalWorkerOptions.workerSrc = workerSrc;
// CSS
import styles from '../pages/css/view.module.css';
import cardStyles from '../components/css/cards.module.css';
import 'react-pdf/dist/Page/TextLayer.css';
import 'react-pdf/dist/Page/AnnotationLayer.css';

// Functions
import { useState, useEffect, useRef } from 'react';
import { getFile, getLessonFile } from '../functions/files/getFile';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)

function FileViewer(props: { title: string }) {
    // Toasts
    const toasts = useToasts();

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
            const header = document.getElementById('pageHeader');
            if (header) {
                header.innerHTML = props.title;
            }
        };
        setTitle();
    }, []);

    // Get the previous location
    const [previousLocation, setPreviousLocation] = useState<string>('');
    useEffect(() => {
        // Get the previous location from the cookies
        const previousLocation = document.cookie.split(';').find(c => c.includes('currentLocation'));
        const previousLocationValue = previousLocation ? previousLocation.split('=').slice(1).join('=') : '/';
        // Decode the previous location
        const decodedLocation = decodeURIComponent(previousLocationValue);
        setPreviousLocation(decodedLocation);
    }, []);

    // File data type
    type FileData = {
        status: string,
        ext: string,
        file: string,
        name: string,
        size: number,
    }

    interface FlipBook {
        pageFlip: () => {
            flipPrev: () => void;
            flipNext: () => void;
        };
    }

    const flipBookRef = useRef<FlipBook>(null);

    // File data and status states
    const [fileData, setFileData] = useState<FileData>({
        status: 'loading',
        ext: '',
        file: '',
        name: '',
        size: 0
    });
    const [status, setStatus] = useState('loading');
    const [numPages, setNumPages] = useState<number>(0);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [rerenderKey, setRerenderKey] = useState(0);
    const [viewIcon, setViewIcon] = useState('book-open');
    const [showQRCode, setShowQRCode] = useState(false);

    // Get current view width and height and update the state on resize
    const [viewWidth, setViewWidth] = useState(document.getElementById('pageFlipView')?.clientWidth || document.documentElement.clientWidth);
    const [viewHeight, setViewHeight] = useState(document.getElementById('pageFlipView')?.clientHeight || document.documentElement.clientHeight);
    const [padding, setPadding] = useState(0);

    useEffect(() => {
        // Listen for window resize events
        window.addEventListener('resize', updateViewSize);
        // Update the view size on first load
        updateViewSize();
        return () => {
            window.removeEventListener('resize', updateViewSize);
        };
    }, []);

    const updateViewSize = () => {
        // Get the maximum width and height for the view
        let maxHeight = window.innerHeight * 0.9;
        let maxWidth = (maxHeight * Math.sqrt(2));

        // If the width is greater than the window width, fit the width
        if (maxWidth > window.innerWidth) {
            maxWidth = window.innerWidth;
            maxHeight = (maxWidth / Math.sqrt(2));
        }
        let newPadding = (window.innerHeight - maxHeight) / 2;

        // Update the view size
        setViewWidth(maxWidth);
        setViewHeight(maxHeight);
        setPadding(newPadding);

        // Update the rerender key to force the pageflip component to rerender
        setRerenderKey(rerenderKey + 1);
    };

    // Get the version from the url and get the lessons data
    const urlDirect = new URLSearchParams(window.location.search).get('direct');
    const urlLevel = new URLSearchParams(window.location.search).get('level');
    let urlVersion = new URLSearchParams(window.location.search).get('version') ?? '';
    const urlFile = new URLSearchParams(window.location.search).get('file');
    const directAccess = new URLSearchParams(window.location.search).get('directAccess');
    useEffect(() => {
        if (urlDirect) {
            getFile(urlDirect).then((data) => {
                if (data.status === 'success') {
                    if (directAccess === 'true') {
                        const a = document.createElement('a');
                        a.style.display = 'none';
                        a.href = data.file ?? '';
                        a.download = data.name ?? '';
                        document.body.appendChild(a);
                        a.click();
                    } else {
                        setFileData(data as FileData);
                        setStatus('success');
                    }
                } else if (data.status === 'not_found') {
                    setStatus('not_found');
                } else if (data.status === 'unauthorized') {
                    setStatus('unauthorized');
                } else {
                    setStatus('error');
                }
            });
        } else if (urlLevel && urlFile) {
            getLessonFile(urlLevel, urlVersion, urlFile).then((data) => {
                if (data.status === 'success') {
                    setFileData(data as FileData);
                    setStatus('success');
                } else if (data.status === 'not_found') {
                    setStatus('not_found');
                } else if (data.status === 'unauthorized') {
                    setStatus('unauthorized');
                } else {
                    setStatus('error');
                }
            });
        }
    }, []);

    // Handle page number
    function onDocumentLoadSuccess({ numPages }: { numPages: number }): void {
        setNumPages(numPages);
    }

    const pages = [];
    const totalPages = numPages ?? 0;
    for (let i = 1; i <= totalPages; i++) {
        pages.push(
            <div key={"page_" + i}>
                <Page pageNumber={i} height={viewHeight} />
            </div>
        );
    }

    if (directAccess === 'true') {
        return (
            <Container className={styles.mainView}>
                <div className='d-flex flex-column align-items-center mt-4'>
                    Fichier en cours de téléchargement... Vous pouvez fermer cette page une fois le téléchargement terminé.
                </div>
            </Container>
        )
    }

    return (
        <>
            {
                (fileData.status !== 'success' || (status === 'success' && fileData.ext !== 'pdf' &&
                    fileData.ext !== 'html')) &&
                <Container className={styles.mainView} style={{ overflow: 'hidden', height: '100vh' }}
                >
                    <Button variant="secondary" href={previousLocation !== '' ? previousLocation : '/'}>
                        <FontAwesomeIcon icon="arrow-left" /> Retourner sur EnSciences
                    </Button>
                    {/* Display error message if file not found */}
                    {status === 'not_found' &&
                        <Alert variant="danger" className="mt-4 d-flex justify-content-between align-items-center">
                            <span><FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Le fichier demandé n'existe pas...</span>
                            <Button variant="danger" size="sm" href={
                                urlDirect ?
                                    "/contact?request=bug_site&additionalInfo=" + urlDirect :
                                    "/contact?request=bug_site&additionalInfo=" + urlLevel + "/" + urlVersion + "/" + urlFile
                            }>
                                <FontAwesomeIcon icon={['fas', 'envelope']} />&nbsp;Signaler</Button>
                        </Alert>
                    }
                    {/* Display error message if user is unauthorized */}
                    {status === 'unauthorized' &&
                        <Alert variant="danger" className="mt-4 d-flex justify-content-between align-items-center">
                            <span><FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Vous n'êtes pas autorisé à accéder à ce fichier.</span>
                            <Button variant="danger" size="sm" href={
                                urlDirect ?
                                    "/contact?request=demande_acces&additionalInfo=" + urlDirect :
                                    "/contact?request=demande_acces&additionalInfo=Fichier : " + urlFile + ", Niveau : " + urlLevel + ", Version : " + urlVersion}>
                                <FontAwesomeIcon icon={['fas', 'envelope']} />&nbsp;Demander l'accès</Button>
                        </Alert>
                    }
                    {/* Display error message if an error occured */}
                    {status === 'error' &&
                        <Alert variant="danger" className="mt-4 d-flex justify-content-between align-items-center">
                            <span><FontAwesomeIcon icon={['fas', 'exclamation-triangle']} />&nbsp;Une erreur est survenue...</span>
                            <Button variant="danger" size="sm" href="/contact?request=bug_site">
                                <FontAwesomeIcon icon={['fas', 'envelope']} />&nbsp;Signaler</Button>
                        </Alert>
                    }
                    {/* Display download button if ext is not pdf */}
                    {status === 'success' && fileData.ext !== 'pdf' && fileData.ext !== 'html' &&
                        <div className={styles.downloadContainer}>
                            <Card className={cardStyles.card + " " + styles.downloadCard}>
                                <Card.Body>
                                    <Card.Title className="text-center">
                                        {/* If file is an image, display it */}
                                        {
                                            ['jpg', 'jpeg', 'png', 'gif'].includes(fileData.ext) &&
                                            <img src={fileData.file} alt={fileData.name} style={{
                                                width: '100%',
                                                height: 'auto',
                                                backgroundColor: 'var(--bs-light)',
                                            }} />
                                        }
                                        {['jpg', 'jpeg', 'png', 'gif'].includes(fileData.ext)}
                                        {/* If file is not an image, display the icon */}
                                        {
                                            !['jpg', 'jpeg', 'png', 'gif'].includes(fileData.ext) &&
                                            <>
                                                <FontAwesomeIcon icon={['fas', extToIcon(fileData.ext)]} className={styles.fileIcon} />
                                                <br />
                                            </>
                                        }

                                        <div className='mt-3'>
                                            {fileData.name}<br />
                                            <span className='text-muted small'> (
                                                {
                                                    (fileData.size / (1024 * 1024)).toFixed(3)
                                                }
                                                &nbsp;Mo)</span>
                                        </div>
                                    </Card.Title>
                                    <Card.Text className="mt-3">
                                        <a className="btn btn-primary btn-block w-100" href={fileData.file} download={fileData.name}>
                                            <FontAwesomeIcon icon={['fas', 'download']} />&nbsp;Télécharger le fichier
                                        </a>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                    }
                    {/* Display loading message */}
                    {status === 'loading' &&
                        <div className='d-flex flex-column align-items-center mt-4'>
                            <Spinner animation="border" style={{
                                width: '4rem',
                                height: '4rem'
                            }} />
                            <span className="mt-3">
                                Récupération du fichier...
                            </span>
                        </div>
                    }
                </Container>
            }

            {/* Display an iframe if the file is html */}
            {fileData.status === 'success' && fileData.ext === 'html' &&
                <iframe src={fileData.file} style={{
                    width: '100%',
                    height: '100vh',
                    border: 'none'
                }} />
            }

            {/* Display pdf is ext is pdf */}
            {
                fileData.status === 'success' && fileData.ext === 'pdf' &&
                <>
                    <div className={styles.toolBar}>
                        {/* Swap between standard view and pageflip view when the file is a pdf */}
                        <BigCircleButton className={styles.toolBtn} onClick={() => {
                            const viewContainer = document.getElementById('standardPdfViewContainer');
                            const pageFlipView = document.getElementById('pageFlipViewWrapper');
                            if (viewContainer && pageFlipView) {
                                if (viewContainer.style.display === 'none') {
                                    viewContainer.style.display = 'block';
                                    pageFlipView.style.display = 'none';
                                    // Change this icon to book-open
                                    setViewIcon('book-open');
                                } else {
                                    viewContainer.style.display = 'none';
                                    pageFlipView.style.display = 'block';
                                    // Change this icon to file-pdf
                                    setViewIcon('file-pdf');
                                    updateViewSize();
                                }
                            }
                        }} icon={viewIcon}
                            color='var(--bs-secondary)'
                        />
                        {/* Share button */}
                        <BigCircleButton icon='share-alt' className={styles.toolBtn}
                            color='var(--bs-primary)' onClick={() => {
                                // Show the modal
                                setShowQRCode(true);
                            }}
                        />
                        {/* Download button */}
                        <BigCircleButton icon='download' className={styles.toolBtn}
                            color='var(--bs-danger)' onClick={() => {
                                // Download the file
                                const a = document.createElement('a');
                                a.href = fileData.file;
                                if (urlFile) {
                                    a.download = urlFile;
                                    a.click();
                                } else {
                                    a.download = fileData.name;
                                    a.click();
                                }
                            }} />
                    </div>

                    {/* QRCode modal */}
                    <Modal show={showQRCode} onHide={() => setShowQRCode(false)} centered>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                <FontAwesomeIcon icon={['fas', 'share-alt']} />&nbsp;Partage du document
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="text-center">
                                <QRCode value={window.location.href} size={viewWidth * 0.4} />

                                <InputGroup className="mt-3">
                                    <Form.Control type="text" className="form-control" value={window.location.href} readOnly />
                                    <Button variant="primary" onClick={() => {
                                        navigator.clipboard.writeText(window.location.href);
                                        toasts.show({
                                            headerContent: 'Succès',
                                            bodyContent: "Lien copié dans le presse-papiers",
                                            toastProps: {
                                                bg: 'success',
                                                autohide: true,
                                                delay: 5000,
                                            },
                                        });
                                    }}><FontAwesomeIcon icon={['fas', 'copy']} /></Button>
                                </InputGroup>
                            </div>
                        </Modal.Body>
                    </Modal>

                    {/* Main view */}
                    <div style={{
                        width: '100vw',
                        height: '100vh',
                        overflow: 'hidden',
                        padding: '0',
                        margin: '0',
                    }}
                    id = "standardPdfViewContainer"
                    >
                        <embed src={fileData.file} style={{
                            width: '100vw',
                            height: '100vh',
                            padding: '0',
                            margin: '0',
                            overflow: 'hidden',
                            WebkitOverflowScrolling: 'touch',
                        }} id="standardPdfView" />
                    </div>

                    <div id="pageFlipViewWrapper" className={styles.pageFlipViewWrapper}>
                        <div id="pageFlipView" className={styles.pageFlipView} style={{
                            width: viewWidth,
                            height: viewHeight,
                            paddingTop: padding,
                            paddingBottom: padding,
                            WebkitOverflowScrolling: 'touch'
                        }} key={rerenderKey}>
                            <Document file={fileData.file} onLoadSuccess={onDocumentLoadSuccess}>
                                <HTMLFlipBook
                                    width={viewWidth}
                                    height={viewWidth * Math.sqrt(2)}
                                    size='stretch'
                                    maxShadowOpacity={0.5}
                                    showCover={true}
                                    mobileScrollSupport={true}
                                    onFlip={(e) => {
                                        setPageNumber(e.data + 1);
                                    }}
                                    style={{}}
                                    className={''}
                                    startPage={0}
                                    minWidth={0}
                                    maxWidth={0}
                                    minHeight={0}
                                    maxHeight={0}
                                    drawShadow={true}
                                    flippingTime={1000}
                                    usePortrait={false}
                                    startZIndex={0}
                                    autoSize={true}
                                    clickEventForward={false}
                                    useMouseEvents={true}
                                    swipeDistance={0}
                                    showPageCorners={true}
                                    disableFlipByClick={false}
                                    ref={flipBookRef}>
                                    {pages}
                                </HTMLFlipBook>
                            </Document>
                        </div>
                        {/* Page controls */}
                        <div className={styles.pageControls}>
                            <Card>
                                <Card.Body className='p-1 d-flex justify-content-between align-items-center'>
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        if (pageNumber > 1) {
                                            setPageNumber(pageNumber - 1);
                                            flipBookRef.current?.pageFlip().flipPrev();
                                        }
                                    }}><FontAwesomeIcon icon="arrow-left" /></Button>
                                    <span className="mx-3">{pageNumber}/{numPages}</span>
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        if (pageNumber < numPages) {
                                            setPageNumber(pageNumber + 1);
                                            flipBookRef.current?.pageFlip().flipNext();
                                        }
                                    }}><FontAwesomeIcon icon="arrow-right" /></Button>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </>
            }
        </>
    );
}

function extToIcon(ext: string) {
    switch (ext) {
        case 'pdf':
            return 'file-pdf';
        case 'doc':
        case 'docx':
            return 'file-word';
        case 'ppt':
        case 'pptx':
            return 'file-powerpoint';
        case 'xls':
        case 'xlsx':
            return 'file-excel';
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            return 'file-image';
        case 'mp4':
        case 'avi':
        case 'mov':
        case 'mkv':
            return 'file-video';
        case 'mp3':
        case 'wav':
        case 'flac':
            return 'file-audio';
        case 'odt':
        case 'ods':
        case 'odp':
            return 'file-alt';
        case 'zip':
        case 'rar':
        default:
            return 'file';
    }
}

export default FileViewer;