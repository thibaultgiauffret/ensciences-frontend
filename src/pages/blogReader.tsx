// ----------------------------
// Blog reader page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, InputGroup, Form, Button, Spinner, Badge, OverlayTrigger, Popover } from 'react-bootstrap';
import { TableOfContent } from '../components/blog/blog';
import { H2 } from '../components/sections';
import DOMPurify from "dompurify";

// Functions
import { useState, useEffect } from 'react';
import { getBlogPost } from '../functions/blog/getBlogPost';
import { getBlogPostComments, postComment } from '../functions/blog/getBlogPostComments';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)

// CSS
import './css/blogReader.css';
import "animate.css/animate.compat.css"

function BlogReader(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // Get the post id in the url
    const url = new URL(window.location.href);
    const postId = parseInt(url.searchParams.get('post')!);
    if (postId === null) {
        window.location.href = '/blog';
    }

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Set the different states
    const [userIsAdmin, setUserIsAdmin] = useState(false);
    const [userIsLoggedIn, setUserIsLoggedIn] = useState(false);
    const [blogPost, setBlogPost] = useState<BlogPostData>({} as BlogPostData);

    interface Comment {
        author_name: string;
        content: {
            rendered: string;
        };
        date: string;
    }

    const [comments, setComments] = useState<Comment[]>([]);

    // Get the contents
    useEffect(() => {
        // Get the blog post
        getBlogPost(postId).then((response: any) => {
            if (response.status === 'success') {
                setBlogPost(response.blogPost);
            }
        }).catch(() => {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la récupération du billet de blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        })

        // Get the comments
        getBlogPostComments(postId).then((response: any) => {
            if (response.status === 'success') {
                setComments(response.comments);
            }
        }).catch(() => {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Une erreur est survenue lors de la récupération des commentaires',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        })

        // Get user infos
        getUserInfos().then((response: any) => {
            if (response.data) {
                if (response.data.status === true) {
                    setUserIsLoggedIn(true);
                }
                if (response.data.role.includes('administrator')) {
                    setUserIsAdmin(true);
                }
            }
        });
    }, []);

    // Search bar
    const [search, setSearch] = useState('');
    const [originalContent, setOriginalContent] = useState<string | null>(null);

    useEffect(() => {
        const content = document.getElementById('blogContent');

        // Text highlighting function
        const highlightText = (node: any) => {
            if (node.nodeType === 3) { // Node.TEXT_NODE
                const text = node.nodeValue;
                const regex = new RegExp(search, 'gi');
                if (regex.test(text)) {
                    const newNode = document.createElement('span');
                    newNode.innerHTML = text.replace(regex, `<span class='bg-warning'>$&</span>`);
                    node.parentNode.replaceChild(newNode, node);
                }
            } else if (node.nodeType === 1) {
                node.childNodes.forEach((child: any) => highlightText(child));
            }
        };

        // If the original content is not set, set it
        if (content && originalContent === null) {
            setOriginalContent(content.innerHTML);
            return;
        }

        // If the search bar is empty, reset the content to the original
        if (!search) {
            if (content) {
                content.innerHTML = originalContent!;
            }
            return;
        }

        // Highlight the text
        if (content) {
            content.innerHTML = originalContent!;
        }
        highlightText(content);

    }, [search, originalContent]);

    // Wrap img in blogContent
    useEffect(() => {
        const blogContent = document.getElementById('blogContent');
        let images: HTMLCollectionOf<Element> = document.getElementsByClassName('figure-img');

        if (blogContent) {
            images = blogContent.getElementsByClassName('figure-img');
            Array.from(images).forEach((img) => {
                const wrapper = document.createElement('div');
                wrapper.className = 'image-container';
                img.parentNode?.insertBefore(wrapper, img);
                wrapper.appendChild(img);

                // Add event listener to open the modal
                img.addEventListener('click', () => openModal(img));
            });
        }

        // Open modal
        const openModal = (img: Element) => {
            const modal = document.createElement('div');
            modal.className = 'image-modal';
            modal.innerHTML = `
            <span class="close">&times;</span>
                <div class="modal-content">
                    
                    <img src="${(img as HTMLImageElement).src}" class="modal-image" />
                </div>
            `;
            document.body.appendChild(modal);

            // Event listeners to close the modal
            modal.querySelector('.close')?.addEventListener('click', () => {
                document.body.removeChild(modal);
            });
            modal.addEventListener('click', () => {
                document.body.removeChild(modal);
            });
        };

        // Remove event listeners
        return () => {
            Array.from(images).forEach((img) => {
                img.removeEventListener('click', () => openModal(img));
            });
        };
    }, [blogPost.content]);

    // Load fontawesome icons to work with <i> tags (temporary fix)
    useEffect(() => {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = '/fontawesome.min.css';
        link.id = 'font-awesome-stylesheet';
        document.head.appendChild(link);
        return () => {
            document.head.removeChild(link);
        };
    }, []);


    return (
        <>
            <Header title={props.title} subtitle={blogPost.title}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        {/* Blog content */}
                        <Col xs={12} md={8}>
                            {blogPost.content ? (
                                <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(blogPost.content) }}
                                    id='blogContent'></div>
                            ) : (
                                <div className='text-center'>
                                    <Spinner animation='border' className='my-3' />
                                    <p>Chargement du contenu...</p>
                                </div>
                            )}
                        </Col>
                        <Col>
                            {userIsAdmin && (
                                <Button variant='warning' href={'https://wordpress.ensciences.fr/wp-admin/post.php?post=' + postId + '&action=edit'} target='_blank' className='mb-3'>
                                    <FontAwesomeIcon icon='edit' />&nbsp;Modifier l'article
                                </Button>
                            )}

                            {/* Search bar to highlight text */}
                            <InputGroup className='mb-3'>
                                <Form.Control type='text'
                                    id="search"
                                    placeholder="Rechercher dans l'article" value={search} onChange={(e) => setSearch(e.target.value)} />
                                <Button variant='outline-secondary' onClick={() => {
                                    const searchElement = document.getElementById('search') as HTMLInputElement;
                                    setSearch(searchElement.value);
                                }}>
                                    <FontAwesomeIcon icon='search' />
                                </Button>
                            </InputGroup>

                            <H2>Sommaire</H2>
                            <TableOfContent content={blogPost.content} />

                            <H2>Informations</H2>
                            <div>
                                {/* Author */}
                                <FontAwesomeIcon icon='user' />&nbsp;<b>Auteur :</b> {blogPost.author}<br />
                                {/* Date */}
                                <FontAwesomeIcon icon='calendar-alt' />&nbsp;<b>Date de publication :</b>  {blogPost.date} <br />
                                {/* Update */}
                                {blogPost.modified && (
                                    <>
                                        <FontAwesomeIcon icon='sync-alt' />&nbsp;<b>Dernière modification:</b> {blogPost.modified}<br />
                                    </>
                                )}
                                {/* Categories */}
                                <FontAwesomeIcon icon='tags' />&nbsp;<b>Catégories :</b> {blogPost.post_categories && blogPost.post_categories.map((category: any, index: number) => {
                                    return (
                                        <Badge key={index} bg='secondary' className='me-1'>{category}</Badge>
                                    );
                                })}
                            </div>

                            {/* Comments */}
                            <H2>
                                Commentaires
                                <Badge bg='secondary' className="commentsCounter">{comments.length}</Badge>
                            </H2>
                            <div className="commentsList">
                                {comments.length > 0 ? (
                                    comments.map((comment, index) => {
                                        return (
                                            <div key={index} className='border border-secondary p-3 my-3 rounded'>
                                                <div>
                                                    <b>{comment.author_name}</b> a dit :
                                                </div>

                                                <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(comment.content.rendered) }}></div>

                                                <div className='small text-muted'>
                                                    {comment.date}
                                                </div>
                                            </div>
                                        );
                                    })
                                ) : (
                                    <div>Aucun commentaire pour le moment</div>
                                )}
                            </div>

                            {userIsLoggedIn ? (
                                <>
                                    {/* Textarea */}
                                    < Form.Control as='textarea' placeholder='Votre commentaire' className='mt-3' id='commentTextarea' />

                                    {/* Button */}
                                    <Button variant='primary' className='mt-3' onClick={() => {
                                        const textarea = document.getElementById('commentTextarea') as HTMLInputElement;
                                        if (textarea) {
                                            postComment(postId, textarea.value).then((response: any) => {
                                                if (response.status === 'success') {
                                                    toasts.show({
                                                        headerContent: 'Succès',
                                                        bodyContent: 'Votre commentaire a bien été posté.',
                                                        toastProps: {
                                                            bg: 'success',
                                                            autohide: true,
                                                            delay: 5000,
                                                        },
                                                    });
                                                } else {
                                                    toasts.show({
                                                        headerContent: 'Erreur',
                                                        bodyContent: 'Une erreur est survenue lors de la publication de votre commentaire : ' + response.status,
                                                        toastProps: {
                                                            bg: 'danger',
                                                            autohide: true,
                                                            delay: 5000,
                                                        },
                                                    });
                                                }
                                            });
                                        }
                                    }} >
                                        <FontAwesomeIcon icon='paper-plane' />&nbsp;Ajouter un commentaire
                                    </Button>
                                </>
                            ) : (
                                <div className='mt-3'>
                                    <OverlayTrigger overlay={<Popover>
                                        <Popover.Header as="h3">
                                            <FontAwesomeIcon icon="lock" style={{ color: "var(--bs-secondary)" }} /> Accès restreint
                                        </Popover.Header>
                                        <Popover.Body>
                                            <span>
                                                Veuillez cliquer pour vous connecter.
                                            </span>
                                        </Popover.Body>
                                    </Popover>}>
                                        <Button variant='primary' href='/login'>
                                            <FontAwesomeIcon icon='paper-plane' />&nbsp;Ajouter un commentaire
                                        </Button>
                                    </OverlayTrigger>
                                </div>
                            )}

                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}

export default BlogReader;