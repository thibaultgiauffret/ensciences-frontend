// ----------------------------
// Error page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import Topbar from "../components/topbar";
import { ToastsProvider as BootstrapToastsProvider } from 'react-bootstrap-toasts';
import Container from 'react-bootstrap/Container';
import Footer from "../components/footer";
import Notification from '../components/notification';
import { useRouteError } from 'react-router-dom';
import { Alert, Button } from 'react-bootstrap';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas)
library.add(fab)

// Functions
import { useEffect, useState } from 'react';

// CSS
import "animate.css/animate.compat.css"
import styles from './css/error.module.css';


function ErrorPage() {
    // Get the error code
    const error: { status?: number } = useRouteError() as { status?: number };
    const errorCode = error?.status || 'Unknown Error';

    // Switch case with the error message corresponding to the error code
    let errorMessage;
    let errorTitle;
    switch (errorCode) {
        case 404:
            errorMessage = "La page ou la ressources demandée n'existe pas.";
            errorTitle = "Not Found";
            break;
        case 403:
            errorMessage = "Vous n'avez pas les droits pour accéder à cette page.";
            errorTitle = "Forbidden";
            break;
        case 500:
            errorMessage = "Une erreur est survenue sur le serveur.";
            errorTitle = "Internal Server Error";
            break;
        default:
            errorMessage = "Une erreur est survenue.";
            errorTitle = "Error";
            break;
    }

    const url = window.location.href;

    // Get the previous location
    const [previousLocation, setPreviousLocation] = useState<string>('');
    useEffect(() => {
        // Get the previous location from the cookies
        const previousLocation = document.cookie.split(';').find(c => c.includes('previousLocation'));
        const previousLocationValue = previousLocation ? previousLocation.split('=').slice(1).join('=') : '/';
        // Decode the previous location
        const decodedLocation = decodeURIComponent(previousLocationValue);
        setPreviousLocation(decodedLocation);
    }, []);


    return (

        <BootstrapToastsProvider toastContainerProps={{ position: 'bottom-end', className: 'p-2' }} limit={5}>
            <>
                <div id="wrapper">
                    <Topbar />
                    <Container className="main" id="main">

                        <Header title="Erreur">
                        </Header>
                        <PageContainer>
                            <FullContainer>

                                <div className={styles.error_container}>

                                    <div className={styles.error_ghost}>
                                        <FontAwesomeIcon icon={['fas', 'ghost']} className={styles.animated + " " + styles.faaPulse} />
                                    </div>

                                    <div className={styles.error_text} data-text={errorCode}>
                                        <span className={styles.glitch1} data-text={errorCode} aria-hidden></span>
                                        <span className={styles.glitch2} data-text={errorCode} aria-hidden></span>
                                        {errorCode}
                                    </div>

                                </div>

                                <Alert variant="danger">
                                    <b>Code d'erreur : </b> {errorCode} <br />
                                    <b>Titre : </b> {errorTitle} <br />
                                    <b>Message : </b> {errorMessage} <br />
                                    <b>URL demandée : </b> {url} <br />
                                    <hr />
                                    {/* Contact button */}
                                    Vous pensez qu'il s'agit d'un bug sur le site ? N'hésitez pas à me contacter pour le signaler en cliquant sur le bouton ci-dessous :<br />
                                    <div className="d-flex justify-content-center align-items-center mt-3">
                                        <Button href={"/contact?request=bug_site&additionalInfo=Erreur " + errorCode + " : " + url} variant="danger">
                                            <FontAwesomeIcon icon={['fas', 'bug']} className="me-2" />Signaler un bug</Button>
                                        <span  className="mx-3"> ou </span>

                                        <Button href={previousLocation} variant="secondary">Retourner à la page précédente</Button>
                                    </div>

                                </Alert>

                            </FullContainer>
                        </PageContainer>


                        <Footer />
                    </Container>
                </div>
                <Notification />
            </>
        </BootstrapToastsProvider>

    );
}

export default ErrorPage;