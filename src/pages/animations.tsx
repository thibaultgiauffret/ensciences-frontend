// ----------------------------
// Animations page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, InputGroup, Form, Button, ButtonGroup } from 'react-bootstrap';
import { AnimationCard, AnimationRow } from '../components/animations/animations';
import { AddAnimation } from '../components/animations/animationEditor';

// Functions
import { useState, useEffect } from 'react';
import { getAnimations } from '../functions/animations/getAnimations';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
library.add(fas)
library.add(fab)



function Animations(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Set the different states
    const [userIsAdmin, setUserIsAdmin] = useState(false);
    const [animations, setAnimations] = useState<AnimationData[]>([]);
    const [filteredAnimations, setFilteredAnimations] = useState<AnimationData[]>([]);
    const [view, setView] = useState('card');

    // Get the animations list
    useEffect(() => {
        getAnimations().then((response: any) => {
            if (response.animations) {
                // For each animation.title, unescape the ' characters
                response.animations.forEach((animation: AnimationData) => {
                    animation.title = animation.title.replace("\\'", "'");
                });
                setAnimations(response.animations);
            }
        });

        // Get user infos
        getUserInfos().then((response: any) => {
            if (response.data) {
                if (response.data.role.includes('administrator')) {
                    setUserIsAdmin(true);
                }
            }
        });
    }, []);

    // Set the search state
    const [search, setSearch] = useState('');

    // Filter the animations
    useEffect(() => {
        const filtered = animations.filter((animation: any) => {
            return animation.title.toLowerCase().includes(search.toLowerCase()) || animation.keywords.toLowerCase().includes(search.toLowerCase());
        });
        setFilteredAnimations(filtered);
    }, [search, animations]);

    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>

                    {userIsAdmin &&
                        <AddAnimation />
                    }
                    {/* Search bar */}
                    <Row className="my-3">
                        <Col xs={12} sm={8} md={8} lg={10}>
                            <InputGroup>
                                <InputGroup.Text id="searchIcon">
                                    <FontAwesomeIcon icon="search" />
                                </InputGroup.Text>
                                <Form.Control
                                    id="searchAnimation"
                                    placeholder="Rechercher une animation"
                                    aria-label="Recherche"
                                    aria-describedby="searchIcon"
                                    onChange={(e) => {
                                        setSearch(e.target.value);
                                    }}
                                />
                            </InputGroup>
                        </Col>
                        <Col className='d-flex justify-content-end'>
                            <ButtonGroup className='d-inline'>
                                <Button variant="secondary" onClick={() => setView('card')}>
                                    <FontAwesomeIcon icon="th" />
                                </Button>
                                <Button variant="secondary" onClick={() => setView('table')}>
                                    <FontAwesomeIcon icon="list" />
                                </Button>
                            </ButtonGroup>
                        </Col>
                    </Row>

                    {/* Display the animations */}
                    {view === 'card' ? (
                        <Row className='my-3' xs={1} md={2} lg={4} xl={4}>
                            {filteredAnimations.length > 0
                                ? filteredAnimations.map((animation: AnimationData, index: number) => {
                                    return (
                                        <Col key={index}>
                                            <AnimationCard data={animation} admin={userIsAdmin} />
                                        </Col>
                                    )
                                }) : (
                                    <>
                                        {Array.from({ length: 8 }).map((_, index) => (
                                            <Col key={"placeholder"+index}>
                                                <AnimationCard data={{ uuid: 0, id: '', title: '', keywords: '', image: '', url: '', urlSource: '' }} admin={false} />
                                            </Col>
                                        ))}
                                    </>
                                )}
                        </Row>
                    ) : (
                        <Row>
                            <Col>
                                <table className="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Mots-clés</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {filteredAnimations.length > 0
                                            && filteredAnimations.map((animation: AnimationData, index: number) => {
                                                return (
                                                    <AnimationRow key={index} data={animation} admin={userIsAdmin} />
                                                );

                                            })}
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    )}
                </FullContainer>
            </PageContainer>
        </>
    );
}

export default Animations;