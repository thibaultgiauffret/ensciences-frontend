// ----------------------------
// Change password page
// ----------------------------

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container, Button, Card, Alert, InputGroup, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Functions
import { useEffect, useState } from 'react';
import { resetPassword, sendResetPasswordEmail } from '../functions/users/resetPassword';
import { useToasts } from 'react-bootstrap-toasts';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas)
library.add(fab)

// CSS
import "animate.css/animate.compat.css"
import styles from './css/login.module.css';


function ChangePassword(props: { title: string, subtitle?: string }) {

    // Set the title of the page
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title;
        };
        setTitle();
    }, []);

    // Get the previous location
    const [previousLocation, setPreviousLocation] = useState<string>('');
    useEffect(() => {
        // Get the previous location from the cookies
        const previousLocation = document.cookie.split(';').find(c => c.includes('currentLocation'));
        const previousLocationValue = previousLocation ? previousLocation.split('=').slice(1).join('=') : '/';
        // Decode the previous location
        const decodedLocation = decodeURIComponent(previousLocationValue);
        setPreviousLocation(decodedLocation);
    }, []);

    // Initialize the toasts
    const toasts = useToasts();

    // States
    const [email, setEmail] = useState('');
    const [code, setCode] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [stepTwo, setStepTwo] = useState(false);
    const [confirmValidity, setConfirmValidity] = useState(false);
    const [passwordStrength, setPasswordStrength] = useState<string[]>([]);


    const sendMail = () => {
        sendResetPasswordEmail(email).then((response: any) => {
            if (response.status === 'success') {
                setStepTwo(true);
                toasts.show({
                    headerContent: 'Succès',
                    bodyContent: "Le code de sécurité a été envoyé à l'adresse mail. Veuillez le renseigner pour continuer.",
                    toastProps: {
                        bg: 'success',
                        autohide: true,
                        delay: 5000,
                    },
                });
            } else {
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: 'Une erreur est survenue. Veuillez réessayer.',
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        });
    }

    const changePassword = () => {
        resetPassword(email, code, password, passwordConfirm).then((response: any) => {
            if (response.status === 'success') {
                // Go to the login page
                window.location.href = '/login?notification=success&message=Votre mot de passe a été modifié avec succès. Vous pouvez maintenant vous connecter.';
            } else {
                let errorMessage = "";

                // Clear all invalid fields
                clearInvalidFields();

                // Build the error message and highlight the invalid fields
                if (response.errors.includes('missing_fields')) {
                    errorMessage += " Veuillez remplir tous les champs.";
                    // Highlight the missing fields
                    if (!email) {
                        document.getElementById("email")?.classList.add("is-invalid");
                    }
                    if (!password) {
                        document.getElementById("password")?.classList.add("is-invalid");
                    }
                    if (!passwordConfirm) {
                        document.getElementById("passwordConfirm")?.classList.add("is-invalid");
                    }
                    if (!code) {
                        document.getElementById("code")?.classList.add("is-invalid");
                    }
                }
                if (response.errors.includes("passwords_do_not_match")) {
                    errorMessage += "Les mots de passe ne correspondent pas.";
                    document.getElementById("passwordConfirm")?.classList.add("is-invalid");
                }
                if (response.errors.includes('password_not_strong_enough')) {
                    errorMessage += " Le mot de passe n'est pas assez fort.";
                    document.getElementById("password")?.classList.add("is-invalid");
                }
                if (response.errors.includes('invalid_email')) {
                    errorMessage += " L'adresse email n'est pas valide.";
                    document.getElementById("email")?.classList.add("is-invalid");
                }

                // Show an error toast
                toasts.show({
                    headerContent: 'Erreur',
                    bodyContent: errorMessage,
                    toastProps: {
                        bg: 'danger',
                        autohide: true,
                        delay: 5000,
                    },
                });
            }
        });
    }


    return (
        <Container id="mainLogin" className={styles.mainLogin}>
            <Button variant="primary" href={previousLocation !== '' ? previousLocation : '/'} className={styles.backButton}>
                <FontAwesomeIcon icon="arrow-left" /> Retourner sur EnSciences
            </Button>

            {/* EnSciences logo centered */}
            <div className={styles.logoContainer}>
                <img src="/images/favicon.svg" alt="EnSciences" className={styles.logo} />
            </div>

            {/* Login form in a card with two tabs */}
            <Card className={styles.loginCard}>
                <Card.Body>
                    {!stepTwo &&
                        <Alert variant="warning">
                            Veuillez entrer l'<b>adresse mail associée à votre compte</b>. Vous recevrez un code de sécurité à cette adresse pour définir un nouveau mot de passe.
                        </Alert>
                    }

                    <InputGroup className="mb-3">
                        <InputGroup.Text><FontAwesomeIcon icon="envelope" /></InputGroup.Text>
                        <Form.Control type="email" placeholder="Adresse mail" aria-label="Adresse mail" name="email" id="email" onChange={(e) => setEmail(e.target.value)} />
                    </InputGroup>

                    {
                        stepTwo &&
                        <>
                            <InputGroup className="mb-3">
                                <InputGroup.Text><FontAwesomeIcon icon="shield" /></InputGroup.Text>
                                <Form.Control type="text" placeholder="Code de sécurité" aria-label="Code de sécurité" name="code" id="code" onChange={(e) => setCode(e.target.value)} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text><FontAwesomeIcon icon="key" /></InputGroup.Text>
                                <Form.Control type="password" placeholder="Nouveau mot de passe" aria-label="Nouveau mot de passe" name="password" id="password"
                                    className={password ? (passwordStrength.length < 5 ? "is-invalid" : "") : ""}
                                    onChange={(e) => {
                                        setPassword(e.target.value)

                                        if (e.target.value !== passwordConfirm && passwordConfirm !== "") {
                                            setConfirmValidity(false);
                                        } else {
                                            setConfirmValidity(true);
                                        }

                                        // Check password strength (at least 8 characters, 1 uppercase, 1 lowercase, 1 number and 1 special character)
                                        let strength = []
                                        if (e.target.value.length >= 8) {
                                            strength.push("length")
                                        }
                                        if (e.target.value.match(/[A-Z]/)) {
                                            strength.push("uppercase")
                                        }
                                        if (e.target.value.match(/[a-z]/)) {
                                            strength.push("lowercase")
                                        }
                                        if (e.target.value.match(/[0-9]/)) {
                                            strength.push("number")
                                        }
                                        if (e.target.value.match(/[!@#$%^&*]/)) {
                                            strength.push("special")
                                        }
                                        setPasswordStrength(strength);
                                    }} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <InputGroup.Text><FontAwesomeIcon icon="key" /></InputGroup.Text>
                                <Form.Control type="password" placeholder="Confirmer le mot de passe" aria-label="Confirmer le mot de passe" name="passwordConfirm" id="passwordConfirm" className={passwordConfirm ? (!confirmValidity ? "is-invalid" : "") : ""}
                                    onChange={(e) => {
                                        setPasswordConfirm(e.target.value)

                                        if (e.target.value !== password) {
                                            setConfirmValidity(false);
                                        } else {
                                            setConfirmValidity(true);
                                        }
                                    }} />
                            </InputGroup>

                            {/* Password strength */}
                            <Row className="mt-2">
                                <Col>
                                    <ul className="small">
                                        <li className={password && passwordStrength.includes("length") ? styles.validText : password ? styles.invalidText : ""}>8 caractères minimum</li>
                                        <li className={password && passwordStrength.includes("uppercase") ? styles.validText : password ? styles.invalidText : ""}>1 majuscule</li>
                                        <li className={password && passwordStrength.includes("lowercase") ? styles.validText : password ? styles.invalidText : ""}>1 minuscule</li>
                                    </ul>
                                </Col>
                                <Col>
                                    <ul className="small">
                                        <li className={password && passwordStrength.includes("number") ? styles.validText : password ? styles.invalidText : ""}>1 chiffre</li>
                                        <li className={password && passwordStrength.includes("special") ? styles.validText : password ? styles.invalidText : ""}>1 caractère spécial (!@#$%^&*)</li>
                                        <li className={passwordConfirm && confirmValidity ? styles.validText : passwordConfirm ? styles.invalidText : ""}>Les mots de passe correspondent</li>
                                    </ul>
                                </Col>
                            </Row>



                            <Button variant="primary" type="submit" className="w-100" onClick={() => {
                                changePassword()
                            }}>
                                <FontAwesomeIcon icon="check" /> Changer le mot de passe
                            </Button>
                        </>
                    }

                    {!stepTwo &&
                        <Button variant="primary" type="submit" className="w-100" onClick={() => {
                            sendMail();
                        }}>
                            <FontAwesomeIcon icon="paper-plane" /> Envoyer la demande de réinitialisation
                        </Button>
                    }
                </Card.Body>
            </Card>
        </Container>
    );
}


// Remove invalid fields classes
function clearInvalidFields() {
    document.getElementById("email")?.classList.remove("is-invalid");
    document.getElementById("password")?.classList.remove("is-invalid");
    document.getElementById("passwordConfirm")?.classList.remove("is-invalid");
    document.getElementById("passwordConfirm")?.classList.remove("is-valid");
    document.getElementById("password")?.classList.remove("is-valid");
}


export default ChangePassword;