// ----------------------------
// Contact page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, Form, Button, Card, Alert, InputGroup, Badge, FloatingLabel } from 'react-bootstrap';
import { H2 } from '../components/sections';
import { ButtonLink } from '../components/buttons';
import { loadCaptchaEnginge, LoadCanvasTemplateNoReload, validateCaptcha } from 'react-simple-captcha';

// Functions
import { useEffect, useState } from 'react';
import { sendMessage } from '../functions/messages/sendMessage';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)

// CSS
import styles from './css/contact.module.css';
import cardStyles from '../components/css/cards.module.css';

function Contact(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // States
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [subject, setSubject] = useState('');
    const [subjectText, setSubjectText] = useState('');
    const [captcha, setCaptcha] = useState('');

    const [selectedAppOption, setSelectedAppOption] = useState('');

    const [appVisible, setAppVisible] = useState(false);
    const [alert, setAlert] = useState('');

    const [additionalInfo, setAdditionalInfo] = useState('');

    const [isSent, setIsSent] = useState(false);

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Get the "request" query parameter and additional info
    useEffect(() => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const request = urlParams.get('request');
        const additionalInfoParam = urlParams.get('additionalInfo');

        if (additionalInfoParam) {
            setAdditionalInfo(additionalInfoParam);
        }

        if (request) {
            setSubject(request);
            const select = document.getElementById('subject') as HTMLSelectElement;
            select.value = request;
            setSubjectText(select.options[select.selectedIndex].text);
            showAlert(request);
        } else {
            setSubject('');
        }
    }, []);

    // Get the user infos (name, email) if logged in
    useEffect(() => {
        getUserInfos().then((response: any) => {
            if (response.data) {
                setName(response.data.name);
                setEmail(response.data.email);
            }
        });
    }, []);

    // Captcha
    useEffect(() => {
        loadCaptchaEnginge(6);
    }, []);

    const reloadCaptcha = () => {
        loadCaptchaEnginge(6);
    }

    // Submit the message
    const submitMessage = (e: any) => {
        e.preventDefault();
        const captchaValid = validateCaptcha(captcha);
        // Clear invalid
        for (let i = 0; i < e.target.length; i++) {
            e.target[i].classList.remove('is-invalid');
        }
        if (name === '' || email === '' || subject === '' || message === '') {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Veuillez remplir tous les champs du formulaire',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
            // Highlight the empty fields
            for (let i = 0; i < e.target.length; i++) {
                if (e.target[i].value === '') {
                    e.target[i].classList.add('is-invalid');
                } else {
                    e.target[i].classList.remove('is-invalid');
                }
            }
        } else if (!captchaValid) {
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Le code de sécurité est incorrect',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
            e.target[6].classList.add('is-invalid');
        } else {
            sendMessage(name, email, subject, subjectText, message, additionalInfo).then(response => {
                if (response.status === 'success') {
                    setIsSent(true);
                    // toasts.show({
                    //     headerContent: 'Succès',
                    //     bodyContent: 'Votre message a bien été envoyé',
                    //     toastProps: {
                    //         bg: 'success',
                    //         autohide: true,
                    //         delay: 5000,
                    //     }
                    // });
                    // Clear the form
                    e.target.reset();
                    setName('');
                    setEmail('');
                    setMessage('');
                    setSubject('');
                    setSubjectText('');
                    setCaptcha('');
                    setAdditionalInfo('');
                    setSelectedAppOption('');
                    setAppVisible(false);
                    setAlert('');
                    reloadCaptcha();
                } else {
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: 'Une erreur est survenue lors de l\'envoi du message. Veuillez réessayer plus tard.',
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                        }
                    });
                }
            });
        }
    }

    const showAlert = (value: string) => {
        if (value === 'demande_acces') {
            setAlert(`Je reçois énormément de demandes d\'accès au contenu restreint (documents "professeurs" majoritairement). Ces derniers sont destinés à mon usage personnel et aux collègues que je connais. Si vous souhaitez néanmoins faire la demande, merci d\'<b>utiliser votre adresse académique</b> dans le champ "email" ci-dessus.<br/>
            <p class="my-2">Merci de votre compréhension.</p>
            <small>P.S. : les messages impérieux du type "<em>merci de m\'accorder l\'accès, veuillez m'envoyer la correction</em>..." (et j\'en passe) seront ignorés.</small>`);
        } else {
            setAlert('');
        }
    }

    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        {/* Contact form */}
                        <Col xs={12} md={8}>
                            <H2 className='mb-3'>Formulaire de contact</H2>
                            <Alert variant='warning'>
                                Si le formulaire ne fonctionne pas, veuillez utiliser l'adresse suivante : <b>contact&nbsp;[at]&nbsp;ensciences.fr</b>
                            </Alert>
                            {isSent ?
                                <Alert variant='success'>
                                    <FontAwesomeIcon icon={['fas', 'check-circle']} className='me-2' />Votre message a bien été envoyé. Je ferai de mon mieux pour vous répondre dans les plus brefs délais&nbsp;!<br />
                                    {/* Send new message*/}
                                    <Button variant='success' className="mt-3" onClick={() => {
                                        setIsSent(false);
                                    }}>
                                        <FontAwesomeIcon icon={['fas', 'paper-plane']} className='me-2' />Envoyer un nouveau message
                                    </Button>
                                </Alert>
                                :
                                <Card className={cardStyles.card}>
                                    <Card.Body>
                                        <Form onSubmit={submitMessage}>
                                            <InputGroup className='mb-3'>
                                                {/* <Form.Label>Nom</Form.Label> */}
                                                <InputGroup.Text>
                                                    <FontAwesomeIcon icon="user" />
                                                </InputGroup.Text>
                                                <FloatingLabel
                                                    controlId="name"
                                                    label="Nom"
                                                >
                                                    <Form.Control type="text" placeholder="Votre nom" name="name" value={name} onChange={(e) => {
                                                        setName(e.target.value);
                                                    }} />
                                                </FloatingLabel>
                                            </InputGroup>
                                            <InputGroup className='mb-3'>
                                                {/* <Form.Label>Email</Form.Label> */}
                                                <InputGroup.Text>
                                                    <FontAwesomeIcon icon="envelope" />
                                                </InputGroup.Text>
                                                <FloatingLabel
                                                    controlId="email"
                                                    label="Adresse e-mail"
                                                >
                                                <Form.Control type="email" placeholder="Votre adresse e-mail" name="email" value={email} onChange={(e) => {
                                                    setEmail(e.target.value);
                                                }} />
                                                </FloatingLabel>
                                            </InputGroup>
                                            <InputGroup className='mb-3'>
                                                {/* <Form.Label>Objet</Form.Label> */}
                                                <InputGroup.Text>
                                                    <FontAwesomeIcon icon="tag" />
                                                </InputGroup.Text>
                                                <FloatingLabel
                                                    controlId="subject"
                                                    label="Objet"
                                                >
                                                <Form.Select value={subject} id="subject" onChange={(e) => {
                                                    setSubject(e.target.value)
                                                    setSubjectText(e.target.options[e.target.selectedIndex].text);
                                                    if (e.target.value === 'bug_app') {
                                                        // Show the second select
                                                        setAppVisible(true);
                                                    } else if (e.target.value === 'demande_acces') {
                                                        // Get the additional info in url
                                                        const queryString = window.location.search;
                                                        const urlParams = new URLSearchParams(queryString);
                                                        const additionalInfoParam = urlParams.get('additionalInfo');
                                                        if (additionalInfoParam) {
                                                            setAdditionalInfo(additionalInfoParam);
                                                        }
                                                    } else {
                                                        // Hide the second select
                                                        setAppVisible(false);
                                                        setSelectedAppOption('');
                                                        setAdditionalInfo('');
                                                    }

                                                    showAlert(e.target.value);
                                                }
                                                } name="subject">
                                                    <option value="">Choisissez un objet</option>
                                                    <option value="demande_acces"
                                                    >Demande d'accès au contenu restreint</option>
                                                    <option value="bug_site">Problème technique sur le site (erreur 404, bugs...)</option>
                                                    <option value="bug_app">Problème technique sur une application</option>
                                                    <option value="erreur_doc">Erreur dans un document</option>
                                                    <option value="contrib_banques">Contribution à la banque de ressources</option>
                                                    <option value="autre_demande">Autre demande ou remarque</option>
                                                </Form.Select>
                                                </FloatingLabel>
                                            </InputGroup>
                                            <Form.Control type="hidden" name="subjectText" value={subjectText} />
                                            {appVisible &&
                                                <InputGroup className='mb-3'>
                                                    {/* <Form.Label>Application</Form.Label> */}
                                                    <InputGroup.Text>
                                                        <FontAwesomeIcon icon="display" />
                                                    </InputGroup.Text>
                                                    <FloatingLabel
                                                    controlId="app"
                                                    label="Application"
                                                >
                                                    <Form.Select value={selectedAppOption} onChange={(e) => {
                                                        setSelectedAppOption(e.target.value)
                                                        setAdditionalInfo('Application concernée : ' + e.target.options[e.target.selectedIndex].text);
                                                    }
                                                    } name="subject">
                                                        <option value="">Choisissez une application</option>
                                                        <option value="chronophys">ChronoPhys</option>
                                                        <option value="physnet">PhysNet</option>
                                                        <option value="tableau">Tableau</option>
                                                        <option value="physalea">PhysAlea</option>
                                                        <option value="edutex">EduTeX</option>
                                                        <option value="blocalgo">BlocAlgo</option>
                                                        <option value="bacasable">Bac à sable Web</option>
                                                        <option value="autre_app">Autre application</option>
                                                    </Form.Select>
                                                    </FloatingLabel>
                                                </InputGroup>
                                            }
                                            {
                                                alert &&
                                                <Alert variant='danger' className='small'>
                                                    <FontAwesomeIcon icon={['fas', 'exclamation-triangle']} className='me-2' />{
                                                        // Set alert message as HTML
                                                        <span dangerouslySetInnerHTML={{ __html: alert }}></span>
                                                    }
                                                </Alert>
                                            }
                                            <Form.Group controlId="formBasicMessage" className='mb-3'>
                                                {/* <Form.Label>Message</Form.Label> */}
                                                <FloatingLabel
                                                    controlId="message"
                                                    label="Message"
                                                >
                                                <Form.Control as="textarea" rows={5} placeholder="Votre message" name="message" style={{ height: '170px' }} value={message} onChange={(e) => {
                                                    setMessage(e.target.value);
                                                }} />
                                                </FloatingLabel>
                                            </Form.Group>

                                            {/* <Form.Label>Captcha</Form.Label> */}
                                            <Row>
                                                <Col className='d-flex align-items-center' xs={12} md={6} lg={7} xl={8}>
                                                    <InputGroup style={{ marginBottom: '7.25px' }}>
                                                    <FloatingLabel
                                                    controlId="captcha"
                                                    label="Code de sécurité"
                                                >
                                                        <Form.Control type="text" placeholder="Entrer le code de sécurité" name="captcha" value={captcha} onChange={(e) => {
                                                            setCaptcha(e.target.value);
                                                        }} />
                                                        </FloatingLabel>
                                                        <Button id="reloadCaptcha" onClick={() => {
                                                            reloadCaptcha();
                                                        }} variant="outline-secondary">
                                                            <FontAwesomeIcon icon={['fas', 'redo-alt']} />
                                                        </Button>
                                                    </InputGroup>
                                                </Col>
                                                <Col className='d-flex align-items-center justify-content-end'>
                                                    <LoadCanvasTemplateNoReload />
                                                </Col>
                                            </Row>

                                            <Button variant="primary" type="submit" className='mt-3'>
                                                <FontAwesomeIcon icon={['fas', 'paper-plane']} className='me-2' />Envoyer le message
                                            </Button>
                                        </Form>
                                    </Card.Body>

                                </Card>
                            }

                        </Col>
                        <Col xs={12} md={4}>
                            <H2>À propos</H2>

                            <Card className={cardStyles.card + ' ' + styles.cvCard}>
                                <Card.Img variant="top" src="/images/avatar.png" />
                                <Card.Body>
                                    <Card.Title>Thibault GIAUFFRET</Card.Title>
                                    <ButtonLink href='/cv' className="stretched-link text-secondary">
                                        Consulter mon CV <Badge bg='secondary'>Bientôt disponible</Badge>
                                    </ButtonLink>
                                </Card.Body>
                            </Card>

                            <p className='mt-3'><span>
                                <FontAwesomeIcon icon={['fas', 'quote-left']} />
                            </span>
                                &nbsp;Actuellement professeur au lycée Guillaume Apollinaire de Nice (06), je porte un intérêt particulier aux outils numériques <u>libres</u> et à leur utilisation en Sciences-Physiques. Développeur en herbe et bricoleur, j’essaie de partager mes projets et idées à travers ce site&nbsp;!&nbsp;
                                <span>
                                    <FontAwesomeIcon icon={['fas', 'quote-right']} />
                                </span></p>
                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}

export default Contact;