// ----------------------------
// Applications page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, InputGroup, Form, Button, ButtonGroup } from 'react-bootstrap';
import { ApplicationCard, ApplicationRow } from '../components/applications/applications';
import { AddApplication } from '../components/applications/applicationEditor';

// Functions
import { useState, useEffect } from 'react';
import { getApplications } from '../functions/applications/getApplications';
import getUserInfos from '../functions/users/getUserInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { H2 } from '../components/sections';
library.add(fas)
library.add(fab)



function Applications(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Set the different states
    const [userIsAdmin, setUserIsAdmin] = useState(false);
    const [applications, setApplications] = useState<ApplicationData[]>([]);
    const [filteredApplications, setFilteredApplications] = useState<ApplicationData[]>([]);
    const [view, setView] = useState('card');

    // Get the applications list
    useEffect(() => {
        getApplications().then((response: any) => {
            if (response.applications) {
                setApplications(response.applications);
            }
        });

        // Get user infos
        getUserInfos().then((response: any) => {
            if (response.data) {
                if (response.data.role.includes('administrator')) {
                    setUserIsAdmin(true);
                }
            }
        });
    }, []);

    // Set the search state
    const [search, setSearch] = useState('');

    // Filter the applications
    useEffect(() => {
        const filtered = applications.filter((application: any) => {
            return application.title.toLowerCase().includes(search.toLowerCase()) || application.keywords.toLowerCase().includes(search.toLowerCase());
        });
        setFilteredApplications(filtered);
    }, [search, applications]);

    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>

                    {userIsAdmin &&
                        <AddApplication />
                    }
                    {/* Search bar */}
                    <Row className="my-3">
                        <Col xs={12} sm={8} md={8} lg={10}>
                            <InputGroup>
                                <InputGroup.Text id="searchIcon">
                                    <FontAwesomeIcon icon="search" />
                                </InputGroup.Text>
                                <Form.Control
                                    id="searchApplication"
                                    placeholder="Rechercher une application"
                                    aria-label="Recherche"
                                    aria-describedby="searchIcon"
                                    onChange={(e) => {
                                        setSearch(e.target.value);
                                    }}
                                />
                            </InputGroup>
                        </Col>
                        <Col className='d-flex justify-content-end'>
                            <ButtonGroup className='d-inline'>
                                <Button variant="secondary" onClick={() => setView('card')}>
                                    <FontAwesomeIcon icon="th" />
                                </Button>
                                <Button variant="secondary" onClick={() => setView('table')}>
                                    <FontAwesomeIcon icon="list" />
                                </Button>
                            </ButtonGroup>
                        </Col>
                    </Row>

                    <H2>Applications EnSciences</H2>

                    {/* Display the applications */}
                    {view === 'card' ? (
                        <Row className='my-3' xs={1} md={1} lg={2} xl={3}>
                            {filteredApplications.length > 0
                                ? filteredApplications.map((application: ApplicationData, index: number) => {
                                    return application.badgeThirdParty === 'false' && (
                                        <Col>
                                            <ApplicationCard key={index} data={application} admin={userIsAdmin} />
                                        </Col>
                                    );
                                }) : (
                                    <>
                                        {Array.from({ length: 8 }).map((_, index) => (
                                            <Col key={index}>
                                                <ApplicationCard data={{ uuid: 0, id: '', title: '', desc: '', logo: '', url: '', urlSource: '', badgeDev: '', badgeNew: '', badgeThirdParty: '' }} admin={false} />
                                            </Col>
                                        ))}
                                    </>
                                )}
                        </Row>
                    ) : (
                        <Row>
                            <Col>
                                <table className="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Description</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {filteredApplications.length > 0
                                            && filteredApplications.map((application: ApplicationData, index: number) => {
                                                return application.badgeThirdParty === 'false' &&
                                                    <ApplicationRow key={index} data={application} admin={userIsAdmin} />;
                                            })}
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    )}

                    <H2>Applications Tierces</H2>

                    {/* Display the applications */}
                    {view === 'card' ? (
                        <Row className='my-3' xs={1} md={1} lg={2} xl={3}>
                            {filteredApplications.length > 0
                                ? filteredApplications.map((application: ApplicationData, index: number) => {
                                    return application.badgeThirdParty === 'true' && (
                                        <Col>
                                            <ApplicationCard key={index} data={application} admin={userIsAdmin} />
                                        </Col>
                                    );
                                }) : (
                                    <>
                                        {Array.from({ length: 8 }).map((_, index) => (
                                            <Col key={index}>
                                                <ApplicationCard data={{ uuid: 0, id: '', title: '', desc: '', logo: '', url: '', urlSource: '', badgeDev: '', badgeNew: '', badgeThirdParty: '' }} admin={false} />
                                            </Col>
                                        ))}
                                    </>
                                )}
                        </Row>
                    ) : (
                        <Row>
                            <Col>
                                <table className="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Description</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {filteredApplications.length > 0
                                            && filteredApplications.map((application: ApplicationData, index: number) => {
                                                return application.badgeThirdParty === 'true' &&
                                                    <ApplicationRow key={index} data={application} admin={userIsAdmin} />;

                                            })}
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    )}
                </FullContainer>
            </PageContainer>
        </>
    );
}

export default Applications;