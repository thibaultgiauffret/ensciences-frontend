// ----------------------------
// Agreg page
// ----------------------------

// Components
import Header from '../components/header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { H2 } from '../components/sections'
import { PageContainer, FullContainer } from '../components/containers';
import { Alert, Button, Modal, Placeholder, Ratio } from 'react-bootstrap';
import { FileCardByPath } from '../components/cards';
import { OverlayRestrictedMessage } from '../components/overlays';

// Functions
import { useState, useEffect } from 'react';
import getUserInfos from '../functions/users/getUserInfos';
import { getLinks } from '../functions/links/getLinks';
import { getFileInfos } from '../functions/files/getFileInfos';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// CSS
import "animate.css/animate.compat.css"


function Agreg(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Get user infos
    const [connected, setConnected] = useState(false);
    useEffect(() => {
        getUserInfos().then((data) => {
            if (data.data !== undefined) {
                setConnected(true);
            }
        });
    }, []);

    // Set the different states
    const files: any = [
        {
            icon: "file-alt",
            path: "agreg/complet_chimie.pdf",
            title: "Fiches de cours de Chimie",
            description: "Ensemble de mes fiches de cours en Chimie réalisées pendant l'année 2018/2019.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "agreg/histoire_chimie.pdf",
            title: "Histoire de la chimie",
            description: "Quelques éléments historiques sur l'évolution de la chimie.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "agreg/pka.pdf",
            title: "Liste de pKa",
            description: "Liste de pKa communs pour la chimie organique.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "agreg/lecons_chimie.pdf",
            title: "Plans de leçons de Chimie",
            description: "Ensemble de mes plans de leçons de Chimie réalisées pendant l'année 2018/2019.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "agreg/lecons_physique.pdf",
            title: "Plans de leçons de Physique",
            description: "Ensemble de mes plans de leçons de Physique réalisées pendant l'année 2018/2019.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "agreg/montages_chimie.pdf",
            title: "Plans de montages de Chimie",
            description: "Ensemble de mes plans de montages de Chimie réalisées pendant l'année 2018/2019.",
            buttons: []
        },
    ];
    const [filesData, setFilesData] = useState<any[]>([]);

    // Get the filesData
    useEffect(() => {
        const fetchData = async () => {
            const updatedFiles = await Promise.all(files.map(async (file: any) => {
                const data = await getFileInfos(file.path);
                if (data.status === 'error') {
                    return file;
                } else {
                    file.buttons.push({
                        id: 1,
                        title: "Consulter",
                        name: "Consulter",
                        icon: "eye",
                        color: "primary",
                        path: "/view?direct=" + file.path,
                        date: data.data.date,
                        size: data.data.size,
                        enabled: data.data.enabled
                    });
                    return file;
                }
            }));
            setFilesData(updatedFiles);
        };

        fetchData();
    }, []);

    // Fetch /agreg/livres.json
    const [links, setLinks] = useState<any[]>([]);
    useEffect(() => {
        getLinks("agreg/livres.json").then((data) => {
            if (data.status === 'success') {
                setLinks(data.data);
            } else {
                console.error(data);
            }
        });
    }, []);

    // Modals
    const [showProtocoles, setShowProtocoles] = useState(false);
    const [protocoles, setProtocoles] = useState<any[]>([]);
    useEffect(() => {
        getFileInfos("agreg/protocoles").then((data) => {
            console.log(data);
            if (data.status === 'success') {
                setProtocoles(data.data);
            } else {
                console.error(data);
            }
        }
        );
    }, []);
    const [showRessources, setShowRessources] = useState(false);
    const [ressources, setRessources] = useState<any[]>([]);
    useEffect(() => {
        getFileInfos("agreg/ressources").then((data) => {
            if (data.status === 'success') {
                setRessources(data.data);
            } else {
                console.error(data);
            }
        }
        );
    }, []);
    const [showFichesManuscrites, setShowFichesManuscrites] = useState(false);
    const [fichesManuscrites, setFichesManuscrites] = useState<any[]>([]);
    useEffect(() => {
        getFileInfos("agreg/fiches_manuscrites").then((data) => {
            if (data.status === 'success') {
                setFichesManuscrites(data.data);
            } else {
                console.error(data);
            }
        }
        );
    }, []);

    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row>
                        <Col xs={12} md={8}>
                            <H2>Fiches de cours</H2>
                            <Alert variant="info" className='mt-3'>
                                <FontAwesomeIcon icon="exclamation-triangle" className="me-2" />Le recueil de fiches de cours est pour l'instant incomplet et contient des coquilles...
                            </Alert>
                            <FileCardByPath path="agreg/complet_chimie.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="agreg/histoire_chimie.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="agreg/pka.pdf" filesData={filesData} connected={connected} />

                            <H2>Plans de leçons et plans de montages</H2>

                            <FileCardByPath path="agreg/lecons_chimie.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="agreg/lecons_physique.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="agreg/montages_chimie.pdf" filesData={filesData} connected={connected} />

                            <H2>Ressources additionnelles</H2>
                            <Alert variant="info" className='mt-3'>
                                <FontAwesomeIcon icon="exclamation-triangle" className="me-2" />Des ressources pour compléter les plans ! C'est un peu le bazar mais il peut y avoir des choses exploitables... :)
                            </Alert>

                            {/* Modals buttons */}
                            <div className="d-flex flex-wrap mb-3">
                                <Button variant="primary" size="sm" className='m-1' onClick={() => setShowProtocoles(true)}>
                                    <FontAwesomeIcon icon="book" className="me-2" />Protocoles
                                </Button>
                                <Button variant="primary" size="sm" className='m-1' onClick={() => setShowRessources(true)}>
                                    <FontAwesomeIcon icon="puzzle-piece" className="me-2" />Ressources diverses
                                </Button>
                                <Button variant="primary" size="sm" className='m-1' onClick={() => setShowFichesManuscrites(true)}>
                                    <FontAwesomeIcon icon="pen" className="me-2" />Fiches de cours manuscrites
                                </Button>
                            </div>

                            {/* Modals */}
                            <Modal show={showProtocoles} onHide={() => setShowProtocoles(false)}>
                                <Modal.Header closeButton>
                                    <Modal.Title>
                                        <FontAwesomeIcon icon="book" className="me-2" />Protocoles
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <ul>
                                        {protocoles.map((protocole) => {
                                            return <li key={protocole.id}>
                                                <a href={"view?direct=" + protocole.relativePath} target="_blank">{protocole.name}</a>
                                            </li>
                                        }
                                        )}
                                    </ul>
                                </Modal.Body>
                            </Modal>

                            <Modal show={showRessources} onHide={() => setShowRessources(false)}>
                                <Modal.Header closeButton>
                                    <Modal.Title>
                                        <FontAwesomeIcon icon="puzzle-piece" className="me-2" />Ressources diverses
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <ul>
                                        {ressources.map((ressource) => {
                                            return <li key={ressource.id}>
                                                <a href={"view?direct=" + ressource.relativePath} target="_blank">{ressource.name}</a>
                                            </li>
                                        }
                                        )}
                                    </ul>
                                </Modal.Body>
                            </Modal>

                            <Modal show={showFichesManuscrites} onHide={() => setShowFichesManuscrites(false)}>
                                <Modal.Header closeButton>
                                    <Modal.Title>
                                        <FontAwesomeIcon icon="pen" className="me-2" />Fiches de cours manuscrites
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <ul>
                                        {fichesManuscrites.map((fiche) => {
                                            return <li key={fiche.id}>
                                                <a href={"view?direct=" + fiche.relativePath} target="_blank">{fiche.name}</a>
                                            </li>
                                        }
                                        )}
                                    </ul>
                                </Modal.Body>
                            </Modal>
                        </Col>
                        <Col>

                            <H2>Bibliothèque</H2>

                            <ul className="list-unstyled">
                                {links.length === 0 ?
                                    <>
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                        <BookLink book={{}} connected={connected} />
                                    </>

                                    :
                                    links.map((book) => {
                                        return <BookLink book={book} connected={connected} />
                                    })}
                            </ul>

                            <H2>Des liens utiles</H2>

                            <ul className="list-unstyled">
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Site officiel de l'Agrégation de Chimie (rapports de jury...)&nbsp;: <a href="http://agregation-chimie.fr/" target="_blank">ici</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Site de M. Vérot pour l'Agrégation de Chimie de l'ENS de Lyon (annales, cours...)&nbsp;: <a href="http://agregationchimie.free.fr/" target="_blank">ici</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Le site de l'UdPPC (avec les articles du BUP)&nbsp;: <a href="http://national.udppc.asso.fr/index.php/le-bup" target="_blank">ici</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Les articles du Journal of Chemical Education&nbsp;: <a href="https://pubs.acs.org/loi/jceda8" target="_blank">ici</a>.
                                    </span>
                                </li>
                            </ul>
                            <H2>Une private joke</H2>
                            <Ratio aspectRatio={"16x9"} className='mb-3'>
                                <iframe width="100%" src="https://www.youtube-nocookie.com/embed/lGQFA2LCecc" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </Ratio>

                            Pour les connaisseurs de la préparation à l'Agrégation de l'ENS de Lyon (2018-2019) :-)
                        </Col>
                    </Row>

                </FullContainer>
            </PageContainer>
        </>
    );
}

function BookLink({ book, connected }: { book: any, connected: boolean }) {
    return (
        book.name !== undefined ?
            <li className='d-flex align-items-top justify-content-between my-2'>
                <span className='d-flex align-items-top'>
                    <FontAwesomeIcon icon="book" className="me-3 pt-1" />
                    <span>
                        {book.name}
                        <small className='text-muted'><br />
                            {book.author} ({book.edition}, {book.year})
                        </small>
                    </span>
                </span>
                <span>

                    {book.url !== undefined ?
                        <Button variant="primary" size="sm" href={book.url} target="_blank" className='ms-3'>
                            <FontAwesomeIcon icon="eye" />
                        </Button>
                        :
                        <OverlayRestrictedMessage connected={connected}>

                            <a href={connected ? ("/contact?request=demande_acces&additionalInfo=Fichier : " + book.name + " (" + book.author + ", " + book.edition + ", " + book.year + ")"
                            ) : ("/login?notification=info&message=Veuillez vous connecter pour accéder au document.")} className="disabled">
                                <a className="btn btn-sm ms-3 btn-secondary disabled">
                                    <FontAwesomeIcon icon={"eye"} />
                                </a>
                            </a>
                        </OverlayRestrictedMessage>
                    }
                </span>
            </li>
            :
            <li className='d-flex justify-content-between my-2'>
                <FontAwesomeIcon icon="book" className="me-3 pt-1" />
                <span className='w-100'>
                    <Placeholder xs={10} />
                    <small className='text-muted'><br />
                        <Placeholder xs={6} />
                    </small>
                </span>
                <div>
                    <Placeholder.Button xs={2} className="btn-sm px-3" variant="primary" disabled />
                </div>
            </li>

    );
}

export default Agreg;