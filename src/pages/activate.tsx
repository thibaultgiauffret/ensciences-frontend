import { Spinner, Container, Button } from 'react-bootstrap';

// Functions
import { useEffect, useState } from 'react';
import { useToasts } from 'react-bootstrap-toasts';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

function Activate() {

    const toasts = useToasts();

    const [activated, setActivated] = useState(false);
    const [error, setError] = useState(false);

    // Get key and user parameters
    const params = new URLSearchParams(window.location.search);
    const key = params.get('key');
    const user = params.get('user');

    useEffect(() => {
        // Activate the account
        if (key && user) {
            activateAccount(key, user, setActivated).then(data => {
                if (data.status === 'success') {
                    // Display a success message
                    console.log(data.message);
                    // Show toast
                    toasts.show({
                        headerContent: 'Succès',
                        bodyContent: data.message,
                        toastProps: {
                            bg: 'success',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                    // Redirect to the login page
                    setTimeout(() => {
                        window.location.href = '/login?notification=success&message=Veuillez vous connecter avec vos identifiants';
                    }, 3000);
                } else {
                    // Display an error message
                    console.error(data.message);
                    setError(true);
                    // Show toast
                    toasts.show({
                        headerContent: 'Erreur',
                        bodyContent: data.message,
                        toastProps: {
                            bg: 'danger',
                            autohide: true,
                            delay: 5000,
                        },
                    });
                }
            });


        }
    }, []);

    return (
        <Container style={{ height: '100vh', width: '100%', padding: '40px' }}>
            {!error ?
                !activated ?
                    <div className='d-flex flex-column align-items-center mt-4'>
                        <Spinner animation="border" style={{
                            width: '4rem',
                            height: '4rem'
                        }} />
                        <span className="mt-3">
                            Activation du compte
                        </span>
                    </div>
                    :
                    <div className='d-flex flex-column align-items-center mt-4'>
                        <FontAwesomeIcon icon='check-circle' size='4x' className="my-4" />
                        <p>Votre compte a été activé avec succès. Vous allez être redirigé.</p>
                    </div>
                :
                <div className='d-flex flex-column align-items-center mt-4'>
                    <FontAwesomeIcon icon='exclamation-circle' size='4x' className="my-4" />
                    <p>Une erreur est survenue lors de l'activation de votre compte.</p>
                    <div><Button variant="danger" onClick={() => window.location.reload()} className="me-2">Réessayer</Button>ou<Button variant="secondary" onClick={() => window.location.href = '/'} className="ms-2">
                        Retour à l'accueil
                    </Button>
                    </div>
                </div>
            }
        </Container>
    );
}

async function activateAccount(key: string, user: string, setActivated: (value: boolean) => void) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'activateAccount.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ key, user }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            setActivated(true);
            return { "status": "success", "message": data.message };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

// Export
export default Activate;