// ----------------------------
// MultiCompile page
// ----------------------------

// Components
import Header from '../components/header';
import { PageContainer, FullContainer } from '../components/containers';
import { Row, Col, Button, Modal, Alert } from 'react-bootstrap';
import { ButtonLink } from '../components/buttons';
import { H2 } from '../components/sections';
import { SimpleBlogCard } from '../components/cards';
import ScrollAnimation from 'react-animate-on-scroll';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import ImageLoader from '../components/imageLoader';

// Functions
import { useState, useEffect } from 'react';
import { getBlogPost } from '../functions/blog/getBlogPost';

// CSS
import styles from './css/multicompile.module.css';
import "@fontsource/dosis/400.css";

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import "animate.css/animate.compat.css"
import { useToasts } from 'react-bootstrap-toasts';
library.add(fas)
library.add(fab)



function Multicompile(props: { title: string, subtitle?: string }) {

    const toasts = useToasts();

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();


    }, []);

    // Get the blog post n°524
    const [blogPost, setBlogPost] = useState({  id:'', title: '', author: '', date: '', content: '', link: '' });
    useEffect(() => {
        getBlogPost(524).then((response: any) => {
            setBlogPost(response.blogPost);
        }).catch((error) => {
            console.error(error);
            toasts.show({
                headerContent: 'Erreur',
                bodyContent: 'Impossible de récupérer le dernier article du blog',
                toastProps: {
                    bg: 'danger',
                    autohide: true,
                    delay: 5000,
                },
            });
        });
    }, []);

    // Modal
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    return (
        <>
            <ImageLoader />
            <Header title={''} subtitle={props.subtitle} color="#34495e">
                <h1 className={styles.title + ' mt-5'}><span className={styles.lightTitle}>Multi</span><span className={styles.boldTitle}>Compile</span></h1>
                <h3 className={styles.subtitle}>Générez plusieurs versions d'un document à partir d'une unique source LaTeX !</h3>
                <div className={styles.titleContainer}>
                    <div className={'my-4'}>
                        {/* Download */}
                        <Button variant="primary" className="m-1" onClick={handleShow}>
                            <FontAwesomeIcon icon={['fas', 'download']} />&nbsp;Télécharger
                        </Button>

                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Téléchargement</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Alert variant="info">
                                    Veuillez lire la documentation avant de télécharger le logiciel !
                                </Alert>

                                <Row className="text-center">
                                    <Col>

                                        <ButtonLink href="/view?direct=outils/multicompile/multicompile_0.6-alpha_windows.zip" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Windows"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'windows']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink href="/view?direct=outils/multicompile/multicompile_0.6-alpha_linux.zip" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger pour Linux"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fab', 'linux']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink className={styles.downloadButton + ' m-1'} target="_blank">
                                            <FontAwesomeIcon icon={['fab', 'apple']} />
                                        </ButtonLink>
                                    </Col>
                                    <Col>
                                        <ButtonLink href="/view?direct=outils/multicompile/multicompile_0.6-alpha_source.zip" className={styles.downloadButton + ' m-1'} target="_blank">
                                            <OverlayTrigger overlay={<Tooltip>{"Télécharger les sources"}</Tooltip>}>
                                                <FontAwesomeIcon icon={['fas', 'code']} />
                                            </OverlayTrigger>
                                        </ButtonLink>
                                    </Col>
                                </Row>


                            </Modal.Body>
                        </Modal>

                        {/* Documentation */}
                        <Button variant="secondary" className="m-1" href="view?direct=outils/multicompile/documentation_multicompile.pdf">
                            <FontAwesomeIcon icon={['fas', 'book']} />&nbsp;Documentation
                        </Button>

                        {/* Forum */}
                        <Button variant="secondary" className="m-1" disabled>
                            <FontAwesomeIcon icon={['fas', 'comments']} />&nbsp;Forum
                        </Button>
                    </div>
                    <div>
                        Disponible sur <FontAwesomeIcon icon={['fab', 'windows']} />&nbsp;Windows et bien évidemment <FontAwesomeIcon icon={['fab', 'linux']} />&nbsp;Linux ! Peut-être un jour sur <FontAwesomeIcon icon={['fab', 'apple']} />&nbsp;MacOs...
                    </div>

                    <div className={'my-4'}>
                        <img data-path="multicompile/gui.png" alt="MultiCompile" className={styles.headImage} />
                    </div>
                </div>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row className='my-3'>
                        <Col xs={12} md={8}>
                            <H2>Présentation</H2>

                            <p>Multi<b>Compile</b> est un logiciel libre développé en Python permettant, à partir d'une seule source LaTeX, de générer plusieurs variantes d'un même document.</p>

                            <div className="text-center">
                                <img data-path="multicompile/schema.png" alt="MultiCompile" className={styles.image + ' my-3'} />
                            </div>

                            <p>Développé à destination des enseignants, l'objectif est d'optimiser la production d'un même document pour des publics différents (par exemple, une variante "élève" à trous et une variante "professeur" complète). L'intérêt premier est l'utilisation d'une unique source : une seule modification est répercutée sur tous les documents ! Un véritable gain de temps est alors permis de paire avec une meilleure organisation !</p>

                            Parmi les fonctionnalités du logiciel :
                            <ul>
                                <li>Possibilité de créer plusieurs audiences (*) personnalisées dès la rédaction du document ;</li>
                                <li>Détection automatique des audiences dans le document TeX ;</li>
                                <li>Compilation de multiples sources TeX à la chaîne (avec audiences ou non).</li>
                            </ul>

                            <p>(*) Le logiciel se base, pour réaliser cela, sur le paquet LaTeX <b>multiaudience</b>. Pour plus d'informations, vous pouvez consulter la source CTAN ici.</p>
                        </Col>
                        <Col>
                            <ScrollAnimation animateIn="fadeIn" animatePreScroll={true} animateOnce={true} offset={20}>
                                <H2>Billet de blog</H2>
                                <SimpleBlogCard title={blogPost.title} author={blogPost.author} date={blogPost.date} href={"/blog/read?post=" + blogPost.id}>
                                    {/* Safe ? */}
                                    {blogPost.content}
                                </SimpleBlogCard>
                            </ScrollAnimation>
                        </Col>
                    </Row>
                </FullContainer>
            </PageContainer>
        </>
    );
}


export default Multicompile;