// ----------------------------
// CAPES page
// ----------------------------

// Components
import Header from '../components/header';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { H2 } from '../components/sections'
import { PageContainer, FullContainer } from '../components/containers';
import { Alert, Button, Placeholder } from 'react-bootstrap';
import { FileCardByPath } from '../components/cards';
import { OverlayRestrictedMessage } from '../components/overlays';

// Functions
import { useState, useEffect } from 'react';
import { getFileInfos } from '../functions/files/getFileInfos';
import getUserInfos from '../functions/users/getUserInfos';
import { getLinks } from '../functions/links/getLinks';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

// CSS
import "animate.css/animate.compat.css"

function Capes(props: { title: string, subtitle?: string }) {

    // Set the title
    useEffect(() => {
        const setTitle = () => {
            document.title = props.title + " | EnSciences";
        };
        setTitle();
    }, []);

    // Get user infos
    const [connected, setConnected] = useState(false);
    useEffect(() => {
        getUserInfos().then((data) => {
            if (data.data !== undefined) {
                setConnected(true);
            }
        });
    }, []);

    // Set the files
    const files: any = [
        {
            icon: "file-alt",
            path: "capes/complet.pdf",
            title: "Fiches de cours",
            description: "Fiches de cours pour le CAPES de Physique-Chimie.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "capes/significations.pdf",
            title: "Significations et liens en Sciences-Physiques",
            description: "Quelques définitions en Physique ainsi que des tableaux d'analogies.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "capes/incertitudes.pdf",
            title: "Que d'incertitudes !",
            description: "Quelques notions sur l'estimation d'incertitudes en Sciences-Physiques.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "capes/maths.pdf",
            title: "Mathématiques",
            description: "Quelques notions mathématiques pour le concours du CAPLP.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "capes/montages_physique.pdf",
            title: "Plans de montages en Physique",
            description: "Ensemble de mes plans de montages de physique réalisés pendant l'année 2017/2018.",
            buttons: []
        },
        {
            icon: "file-alt",
            path: "capes/montages_chimie.pdf",
            title: "Plans de montages en Chimie",
            description: "Ensemble de mes plans de montages de chimie réalisés pendant l'année 2017/2018.",
            buttons: []
        },
    ];
    const [filesData, setFilesData] = useState<any[]>([]);

    // Get the filesData
    useEffect(() => {
        const fetchData = async () => {
            const updatedFiles = await Promise.all(files.map(async (file: any) => {
                const data = await getFileInfos(file.path);
                if (data.status === 'error') {
                    console.error(data);
                    return file;
                } else {
                    file.buttons.push({
                        id: 1,
                        title: "Consulter",
                        name: "Consulter",
                        icon: "eye",
                        color: "primary",
                        path: "/view?direct=" + file.path,
                        date: data.data.date,
                        size: data.data.size,
                        enabled: data.data.enabled
                    });
                    return file;
                }
            }));
            setFilesData(updatedFiles);
        };

        fetchData();
    }, []);

    // Fetch /capes/livres.json
    const [links, setLinks] = useState<any[]>([]);
    useEffect(() => {
        getLinks("capes/livres.json").then((data) => {
            if (data.status === 'success') {
                setLinks(data.data);
            } else {
                console.error(data);
            }
        });
    }, []);


    return (
        <>
            <Header title={props.title} subtitle={props.subtitle}>
            </Header>
            <PageContainer>
                <FullContainer>
                    <Row>
                        <Col xs={12} md={8}>
                            <H2>Fiches de cours</H2>
                            <Alert variant="info" className='mt-3'>
                                <FontAwesomeIcon icon="lightbulb" className="me-2" />Le programme de M1 MEEF reprend celui de PCSI/PC. Ces quelques fiches de cours peuvent donc aussi être utiles en classes préparatoires !
                            </Alert>
                            <FileCardByPath path="capes/complet.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="capes/significations.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="capes/incertitudes.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="capes/maths.pdf" filesData={filesData} connected={connected} />

                            <H2>Préparation aux oraux </H2>
                            <Alert variant="info" className='mt-3'>
                                <FontAwesomeIcon icon="exclamation-triangle" className="me-2" />Les plans de montages proposés ici sont très loin d'être finalisés et ne sont en aucun cas représentatifs des attentes du jury du CAPES. Tout au plus, ils pourront fournir quelques idées de manipulations.
                            </Alert>

                            <FileCardByPath path="capes/montages_physique.pdf" filesData={filesData} connected={connected} />

                            <FileCardByPath path="capes/montages_chimie.pdf" filesData={filesData} connected={connected} />
                        </Col>
                        <Col>
                            <H2>Bibliothèque</H2>

                            <b>Livres complets de cours</b> pour le CAPES :
                            <ul className="list-unstyled mb-3">
                                {
                                    links.length > 0 &&
                                    <>
                                        <BookLink book={links[0]} connected={connected} />
                                        <BookLink book={links[1]} connected={connected} />
                                    </>
                                }
                            </ul>


                            <b>Quelques livres du professeur</b> (qui peuvent s'avérer utiles pour préparer des activités...) :

                            {
                                links.length > 0 &&
                                <>
                                    <ul className="list-unstyled">
                                        <BookLink book={links[2]} connected={connected} />
                                        <BookLink book={links[3]} connected={connected} />
                                        <BookLink book={links[4]} connected={connected} />
                                        <BookLink book={links[5]} connected={connected} />
                                        <BookLink book={links[6]} connected={connected} />
                                        <BookLink book={links[7]} connected={connected} />
                                        <BookLink book={links[8]} connected={connected} />
                                    </ul>
                                </>
                            }


                            <H2>Des liens utiles</H2>

                            <ul className="list-unstyled">
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Site d'Etienne Thibierge, professeur en CPGE&nbsp;: <a href="http://www.etienne-thibierge.fr/" target="_blank">ici</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Site de l'Agrégation de Chimie de l'ENS de Lyon (avec des sujets de CAPES et leurs corrections)&nbsp;: <a href="http://agregationchimie.free.fr/" target="_blank">ici</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Rapports de jury du CAPES&nbsp;: <a href="http://media.devenirenseignant.gouv.fr/file/externe/71/2/rj-2018-capes-cafep-externe-physique-chimie_989712.pdf" target="_blank">2018</a>, <a href="http://media.devenirenseignant.gouv.fr/file/ext/17/1/rjRJ-2017-capes-cafep-externe-physique-chimie_799171.pdf" target="_blank">2017</a>, <a href="http://media.devenirenseignant.gouv.fr/file/externe/56/1/rj_2016-capes-externe-physique-chimie_689561.pdf" target="_blank">2016</a>, <a href="http://media.devenirenseignant.gouv.fr/file/capes_ext/04/1/phychim_1_517041.pdf" target="_blank">2015</a>, <a href="http://media.devenirenseignant.gouv.fr/file/capes_ext/81/9/physiquechphy_374819.pdf" target="_blank">2014</a>.
                                    </span>
                                </li>
                                <li className='d-flex'>
                                    <FontAwesomeIcon icon="link" className="me-3 pt-1" />
                                    <span>Les programmes en Sciences-Physiques&nbsp;: <a href="http://eduscol.education.fr/physique-chimie/sinformer/textes-officiels/les-programmes.html" target="_blank">ici</a>.
                                    </span>
                                </li>
                            </ul>
                        </Col>
                    </Row>

                </FullContainer>
            </PageContainer>
        </>
    );
}

function BookLink({ book, connected }: { book: any, connected: boolean }) {
    return (
        book.name !== undefined ?
            <li className='d-flex align-items-top justify-content-between my-2'>
                <span className='d-flex align-items-top'>
                    <FontAwesomeIcon icon="book" className="me-3 pt-1" />
                    <span>
                        {book.name}
                        <small className='text-muted'>
                            {book.author} ({book.edition}, {book.year})
                        </small>
                    </span>
                </span>
                <span>

                    {book.url !== undefined ?
                        <Button variant="primary" size="sm" href={book.url} target="_blank" className='ms-3'>
                            <FontAwesomeIcon icon="eye" />
                        </Button>
                        :
                        <OverlayRestrictedMessage connected={connected}>

                            <a href={connected ? ("/contact?request=demande_acces&additionalInfo=Fichier : " + book.name + " (" + book.author + ", " + book.edition + ", " + book.year + ")"
                            ) : ("/login?notification=info&message=Veuillez vous connecter pour accéder au document.")} className="disabled">
                                <a className="btn btn-sm ms-3 btn-secondary disabled">
                                    <FontAwesomeIcon icon={"eye"} />
                                </a>
                            </a>
                        </OverlayRestrictedMessage>
                    }
                </span>
            </li>
            :
            <li className='d-flex justify-content-between my-2'>
                <FontAwesomeIcon icon="book" className="me-3 pt-1" />
                <span className='w-100'>
                    <Placeholder xs={10} />
                    <small className='text-muted'><br />
                        <Placeholder xs={6} />
                    </small>
                </span>
                <div>
                    <Placeholder.Button xs={2} className="btn-sm px-3" variant="primary" disabled />
                </div>
            </li>

    );
}


export default Capes;