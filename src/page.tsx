import ReactDOM from 'react-dom/client'

// Components
import Topbar from "./components/topbar";
import { ToastsProvider as BootstrapToastsProvider } from 'react-bootstrap-toasts';
import Container from 'react-bootstrap/Container';
import Footer from "./components/footer";
import Notification from './components/notification';
import { Loader } from './components/loader';
import { Outlet } from 'react-router-dom';
import Matomo from './components/cookies';
import { ScrollToTopButton } from './components/buttons';
import React from 'react';

// Functions
import { getTheme, setTheme } from './functions/theme/setTheme';
import { Suspense, lazy } from 'react';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import setPreviousLocation from './functions/cookies/previousLocation';

// Lazy loading
const Home = lazy(() => import('./pages/home'));
const Login = lazy(() => import('./pages/login'));
const Activate = lazy(() => import('./pages/activate'));
const FileViewer = lazy(() => import('./pages/view'));
const Animations = lazy(() => import('./pages/animations'));
const Applications = lazy(() => import('./pages/applications'));
const Blog = lazy(() => import('./pages/blog'));
const BlogReader = lazy(() => import('./pages/blogReader'));
const Contact = lazy(() => import('./pages/contact'));
const Multicompile = lazy(() => import('./pages/multicompile'));
const Physnet = lazy(() => import('./pages/physnet'));
const Chronophys = lazy(() => import('./pages/chronophys'));
const Admin = lazy(() => import('./pages/admin'));
const Capes = lazy(() => import('./pages/capes'));
const Agreg = lazy(() => import('./pages/agreg'));
const Ressources = lazy(() => import('./pages/ressources'));
const LaTeX = lazy(() => import('./pages/latex'));
const ErrorPage = lazy(() => import('./pages/error'));
const ChangePassword = lazy(() => import('./pages/changePassword'));

const SecondePC = lazy(() => import('./pages/seconde_pc'));
const SecondeSNT = lazy(() => import('./pages/seconde_snt'));
const PremiereES = lazy(() => import('./pages/premiere_es'));
const TerminaleES = lazy(() => import('./pages/terminale_es'));
const TerminaleSpe = lazy(() => import('./pages/terminale_spe'));
const TerminaleSTI2D = lazy(() => import('./pages/terminale_sti2d'));
const Bts1MsPC = lazy(() => import('./pages/bts_ms'));

// Router
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

// CSS
import './globals.scss';

const App: React.FC = () => {
  const location = useLocation();

  // Set the previous page
  useEffect(() => {
    setPreviousLocation();
  }, [location]);

  return (
    <BootstrapToastsProvider toastContainerProps={{ position: 'bottom-end', className: 'p-2' }} limit={5}>
      <>
        <div id="wrapper">
          <Topbar />
          <Suspense fallback={<Loader />}>
            <Container className="main" id="main">
              <Matomo />
              <Outlet />

              <Footer />
              <ScrollToTopButton />
            </Container>
          </Suspense>
        </div>
        <Notification />
      </>
    </BootstrapToastsProvider>
  );
};

const SimpleApp: React.FC = () => {
  // const location = useLocation();

  // // Set the previous page
  // useEffect(() => {
  //   setPreviousLocation();
  // }, [location]);

  return (
    <BootstrapToastsProvider toastContainerProps={{ position: 'bottom-end', className: 'p-2' }} limit={5}>
      <>
        <Suspense fallback={<Loader marginTop={false} />}>
          <div id="wrapper" className='pb-0'>
            <Matomo />
            <Outlet />
          </div>
        </Suspense>
        <Notification />
      </>
    </BootstrapToastsProvider>
  );
}

// Create the router
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home title="Accueil" />,
      },
      {
        path: "/animations",
        element: <Animations title="Animations" />
      },
      {
        path: "/applications",
        element: <Applications title="Applications en ligne" />
      },
      {
        path: "/blog",
        element: <Blog title="Blog" />
      },
      {
        path: "/blog/read",
        element: <BlogReader title="Billet de blog" />
      },
      {
        path: "/contact",
        element: <Contact title="Contact" />
      },
      {
        path: "/multicompile",
        element: <Multicompile title="MultiCompile" />
      },
      {
        path: "/physnet",
        element: <Physnet title="PhysNet" />
      },
      {
        path: "/chronophys",
        element: <Chronophys title="ChronoPhys" />
      },
      {
        path: "/capes",
        element: <Capes title="CAPES (2018)" />
      },
      {
        path: "/agreg",
        element: <Agreg title="Agrégation (2019)" />
      },
      {
        path: "/ressources",
        element: <Ressources title="Ressources complémentaires" />
      },
      {
        path: "/latex",
        element: <LaTeX title="LaTeX" />
      },
      {
        path: "/admin",
        element: <Admin title="Tableau de bord" />
      },
      {
        path: "/seconde_pc",
        element: <SecondePC title="Seconde" subtitle="Physique-Chimie" />
      },
      {
        path: "/seconde_snt",
        element: <SecondeSNT title="Seconde" subtitle="Sciences Numériques et Technologie" />
      },
      {
        path: "/premiere_es",
        element: <PremiereES title="Première" subtitle="Enseignement Scientifique" />
      },
      {
        path: "/terminale_es",
        element: <TerminaleES title="Terminale" subtitle="Enseignement Scientifique" />
      },
      {
        path: "/terminale_spe",
        element: <TerminaleSpe title="Terminale Spécialité" subtitle="Physique-Chimie" />
      },
      {
        path: "/terminale_sti2d",
        element: <TerminaleSTI2D title="Terminale STI2D" subtitle="Physique-Chimie" />
      },
      {
        path: "/bts1_systemes",
        element: <Bts1MsPC title="BTS1 Maintenance des systèmes" subtitle="Physique-Chimie" />
      }
    ]
  },
  {
    element: <SimpleApp />,
    errorElement:<ErrorPage />,
    children: [
      {
        path: '/login',
        element: <Login title="Connexion" />
      },
      {
        path: '/activate',
        element: <Activate />
      },
      {
        path: '/view',
        element: <FileViewer title="Visionneuse" />
      },
      {
        path: '/reset_password',
        element: <ChangePassword title="Changer de mot de passe" />
      }
    ]
  }
]);

const RouterComponent: React.FC = () => {
  return <RouterProvider router={router} />;
};

ReactDOM.createRoot(document.getElementById('root')!).render(
  <RouterComponent />
);

// Set the theme
setTheme(getTheme());

