async function getLessonFile(level: string, version: string, file: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    console.log('getLessonFile', level, version, file);

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'lessons/getFile.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ level, version, file }),
            credentials: 'include'
        });

        // Get the response
        const data = await response;
        // Check if the response is file or json
        if (data.headers.get('Content-Type')?.includes('application/json')) {
            const newdata = await data.json();
            console.error(newdata.message);
            return { 'status': newdata.status, 'message': newdata.message, ext: '', file: '', name: '', size: 0 };
        } else {
            // If the response is a file, get the file extension from filename
            const ext = file.split('.').pop();
            // Get the file as a blob
            const fileBlob = await data.blob();
            // Get the name of the file
            const name = file.split('/').pop();
            // Get the size of the file
            const size = data.headers.get('Content-Length');
            // Create a URL for the file
            const fileUrl = URL.createObjectURL(fileBlob);
            return { 'status': 'success', 'ext': ext, 'file': fileUrl, 'name': name, 'size': size };
        }

    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

async function getFile(path: string) {

    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/getFile.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ path }),
            credentials: 'include'
        });

        // Get the response
        const data = await response;
        // Check if the response is file or json
        if (data.headers.get('Content-Type')?.includes('application/json')) {
            const newdata = await data.json();
            return { 'status': newdata.status as string, 'message': newdata.message as string, ext: '', file: '', name: '', size: 0 };
        } else {
            // If the response is a file, get the file extension, name and size
            const ext = path.split('.').pop();
            const name = path.split('/').pop();
            const size = data.headers.get('Content-Length');
            // Get the file as a blob
            const fileBlob = await data.blob();
            // Create a URL for the file
            const fileUrl = URL.createObjectURL(fileBlob);
            return { 'status': 'success', 'ext': ext, 'file': fileUrl , 'name': name, 'size': size};
        }

    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getLessonFile, getFile };