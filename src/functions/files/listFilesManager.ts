async function listFilesManager(folder: string) {

    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/listFilesManager.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ folder }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            // Parse permissions
            for (let i = 0; i < data.data.length; i++) {
                if (data.data[i].permissions !== "") {
                    const cleanedPermissions = data.data[i].permissions.replace(/\\/g, '');
                    data.data[i].permissions = JSON.parse(cleanedPermissions);
                } else {
                    data.data[i].permissions = [];
                }
            }
            return { "status": "success", "data": data.data };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }

    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { listFilesManager };