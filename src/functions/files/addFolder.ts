async function addFolder(folder: string, name: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('folder', folder);
    formData.append('name', name);

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/addFolder.php', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "message": data.message };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { addFolder };