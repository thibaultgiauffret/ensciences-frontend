async function getImage(path: string) {

    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/getImage.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ path }),
            credentials: 'include'
        });

        // Get the response
        const data = await response;
        // Check if the response is file or json
        if (data.headers.get('Content-Type')?.includes('application/json')) {
            const newdata = await data.json();
            console.error(newdata.message);
            return { 'status': newdata.status, 'message': newdata.message };
        } else {
            // If the response is a file, get the file extension from filename
            const ext = path.split('.').pop();
            // Get the file as a blob
            const fileBlob = await data.blob();
            // Create a URL for the file
            const fileUrl = URL.createObjectURL(fileBlob);
            return { 'status': 'success', 'ext': ext, 'file': fileUrl };
        }

    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getImage };