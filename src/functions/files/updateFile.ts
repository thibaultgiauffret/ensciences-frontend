async function updateFile(id:string, folder:string, oldPath:string, name:string, permissions:any, size: string, file: File|undefined, inDB: boolean, password: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('id', id);
    formData.append('folder', folder);
    formData.append('oldPath', oldPath);
    formData.append('name', name);
    formData.append('size', size);
    if (file){
        formData.append('file', file);
    } else {
        formData.append('file', '');
    }
    
    formData.append('permissions', JSON.stringify(permissions));
    formData.append('inDB', inDB ? 'true' : 'false');
    formData.append('password', password);

    console.log(formData);

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/updateFile.php', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        console.log(data);
        if (data.status === 'success') {
            return { "status": "success", "message": data.message };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }
        // const data = await response.body?.getReader().read();
        // const decoder = new TextDecoder();
        // const decodedString = decoder.decode(data.value);
        // console.log(decodedString);
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateFile };