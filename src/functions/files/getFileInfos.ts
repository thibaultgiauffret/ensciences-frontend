async function getFileInfos(file: string) {

    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'files/getFileInfos.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ file }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        // Check if the response is file or json
        if (data.status === 'success') {
            return { 'status': 'success', 'data': data.data };
        } else {
            console.error(data.message);
            return { 'status': 'error', 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getFileInfos };