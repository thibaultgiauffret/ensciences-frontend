async function getBlogPosts(currentPage: number, search: string = '', category: string = '', year: string = '') {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_WORDPRESS_URL
        let postResponse = null;
        let posts = null;
        let postList = [];
        let totalPages = '';
        let totalPosts = '';
        if (search) {
            search = `&search=${search}`;
        }
        if (category) {
            category = `&categories=${category}`;
        }
        if (year) {
            year = `&after=${year}-01-01T00:00:00&before=${year}-12-31T23:59:59`;
        }
        try {
            postResponse = await fetch(backendUrl + '?rest_route=/wp/v2/posts&per_page=6&page=' + currentPage + search + category + year + '&_embed');
            posts = await postResponse.json();
            totalPosts = postResponse.headers.get('X-WP-Total') || '0';
            totalPages = postResponse.headers.get('X-WP-TotalPages') || '0';
        } catch (error) {
            console.error('Error fetching posts', error);
            reject({
                status: 'error',
                message: 'Error fetching posts'
            });
        }

        if (postResponse) {
            if (posts.length > 0) {
                const newDate = new Date(posts[0].date);
                const newDateString = newDate.toLocaleDateString("fr-FR", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                });
                for (let i = 0; i < posts.length; i++) {
                    postList.push({
                        date: newDateString,
                        id: posts[i].id,
                        link: posts[i].link,
                        title: posts[i].title.rendered,
                        content: posts[i].content.rendered,
                        excerpt: posts[i].excerpt.rendered,
                        post_categories: posts[i]._embedded['wp:term'][0].map((category: any) => category.name),
                        author: posts[i].author_meta.display_name,
                    });
                }
            }


            resolve({
                status: 'success',
                blogPosts: postList,
                totalPosts: parseInt(totalPosts, 10),
                totalPages: parseInt(totalPages, 10)
            })
        }
        reject({
            status: 'error',
            message: 'No posts found'
        });
    });
}

async function getBlogCategories() {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_WORDPRESS_URL
        let categoriesResponse = null;
        let categories = null;
        try {
            categoriesResponse = await fetch(backendUrl + '?rest_route=/wp/v2/categories');
            categories = await categoriesResponse.json();
        } catch (error) {
            console.error('Error fetching categories', error);
            reject({
                status: 'error',
                message: 'Error fetching categories'
            });
        }

        if (categoriesResponse) {
            if (categories.length > 0) {
                let categoriesList = [];
                for (let i = 0; i < categories.length; i++) {
                    categoriesList.push({
                        id: categories[i].id,
                        name: categories[i].name
                    })
                }
                resolve({
                    status: 'success',
                    categories: categoriesList
                });
            }
        }
        reject({
            status: 'error',
            message: 'No categories found'
        });
    });
}

// Get the years of the blog posts, counting the number of posts for each year
async function getBlogYears() {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_WORDPRESS_URL
        let postResponse = null;
        let posts = null;
        try {
            postResponse = await fetch(backendUrl + '?rest_route=/wp/v2/posts&per_page=100&page=1');
            posts = await postResponse.json();
        } catch (error) {
            console.error('Error fetching posts', error);
            reject({
                status: 'error',
                message: 'Error fetching posts'
            });
        }

        if (postResponse) {
            if (posts.length > 0) {
                let yearsList: { year: number, count: number }[] = [];
                for (let i = 0; i < posts.length; i++) {
                    const newDate = new Date(posts[i].date);
                    const year = newDate.getFullYear();
                    if (!yearsList.some((item) => item.year === year)) {
                        yearsList.push({
                            year: year,
                            count: 1
                        });
                    } else {
                        const index = yearsList.findIndex((item) => item.year === year);
                        yearsList[index].count++;
                    }
                }
                resolve({
                    status: 'success',
                    years: yearsList
                });
            }
        }
        reject({
            status: 'error',
            message: 'No posts found'
        });
    });
}

export { getBlogPosts, getBlogCategories, getBlogYears };