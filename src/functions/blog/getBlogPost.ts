async function getBlogPost(id: number) {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_WORDPRESS_URL
        let postResponse = null;
        let post = null;
        let postData = null;
        try {
            postResponse = await fetch(`${backendUrl}?rest_route=/wp/v2/posts/${id}&_embed`);
            post = await postResponse.json();


            if (post) {
                const newDate = new Date(post.date);
                const newDateString = newDate.toLocaleDateString("fr-FR", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                });
                const newModifiedDate = new Date(post.modified);
                const newModifiedDateString = newModifiedDate.toLocaleDateString("fr-FR", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                });

                let content = post.content.rendered;
                const parser = new DOMParser();
                const htmlDocument = parser.parseFromString(content, "text/html");
                const headings = htmlDocument.querySelectorAll("h2, h3, h4");
                let counter = 0;
                // Add an id to each heading
                headings.forEach((heading) => {
                    if (!(heading instanceof HTMLElement)) return;
                    if (!heading.id) {
                        heading.id = "section-" + counter;
                    }
                    counter++;
                });

                // Rebuild the content
                content = htmlDocument.body.innerHTML;

                postData = {
                    date: newDateString,
                    id: post.id,
                    link: post.link,
                    title: post.title.rendered,
                    content: content,
                    excerpt: post.excerpt.rendered,
                    post_categories: post._embedded['wp:term'][0].map((category: any) => category.name),
                    author: post.author_meta.display_name,
                    modified: post.modified ? newModifiedDateString : null
                }

                resolve({
                    status: 'success',
                    blogPost: postData
                })
            } else {
                reject({
                    status: 'error',
                    message: 'No posts found'
                });
            }
        } catch (error) {
            console.error('Error fetching posts', error);
            reject({
                status: 'error',
                message: 'Error fetching posts'
            });
        }
    });
}

export { getBlogPost };