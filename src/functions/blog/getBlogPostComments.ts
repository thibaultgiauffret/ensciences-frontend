async function getBlogPostComments(id: number) {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_WORDPRESS_URL
        let response = null;
        let comments = null;
        try {
            response = await fetch(`${backendUrl}?rest_route=/wp/v2/comments&post=${id}`);
            comments = await response.json();
            resolve({
                status: 'success',
                comments: comments
            });
        } catch (error) {
            console.error('Error fetching comments', error);
            reject({
                status: 'error',
                message: 'Error fetching comments'
            });
        }
    });
}

function postComment(postId: number, content: string) {
    return new Promise(async (resolve, reject) => {
        const backendUrl = import.meta.env.VITE_API_URL;
        try {
            const response = await fetch(`${backendUrl}/blog/postComment.php`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    postId: postId,
                    content: content
                }),
                credentials: 'include'
            });
            const data = await response.json();
            resolve({
                status: data.status,
            });
        } catch (error) {
            console.error('Error posting comment', error);
            reject({
                status: 'error',
                message: 'Error posting comment'
            });
        }
    })
}

export { getBlogPostComments, postComment };