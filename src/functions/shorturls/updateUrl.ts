async function updateUrl(id:string, file: string, url: string) {

    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'shorturls/updateUrl.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ id, file, url }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status !== 'success') {
            console.error(data.message);
        }
        return { 'status': data.status, 'message': data.message };
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateUrl };