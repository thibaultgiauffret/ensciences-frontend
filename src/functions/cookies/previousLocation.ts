function setPreviousLocation(): void {
    // Get the currentLocation from the cookies
    const currentLocation = document.cookie.split(';').find(c => c.includes('currentLocation'));
    const currentLocationValue = currentLocation ? currentLocation.split('=').slice(1).join('=') : '/';

    // Parse the current URL to get the full path including query parameters
    const url = new URL(window.location.href);
    const fullPath = url.pathname + url.search;

    // Store it to the previousLocation
    if (fullPath !== currentLocationValue) {
        // Get cookie domain
        const cookieDomain = import.meta.env.VITE_COOKIE_DOMAIN;
        setCookie('previousLocation', currentLocationValue, 1, cookieDomain);
        // Set the currentLocation
        setCookie('currentLocation', fullPath, 1, cookieDomain);
    }
}

// Cookies
function setCookie(name: string, value: string, days: number, domain?: string) {
    let expires = "";
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    const domainPart = domain ? `; domain=${domain}` : "";
    document.cookie = name + "=" + (value || "") + expires + "; path=/" + domainPart;
}

export default setPreviousLocation;