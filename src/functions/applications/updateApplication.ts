// Send the application data to the backend
async function updateApplication({ uuid, id, title, desc, logo, url, urlSource, badgeDev, badgeNew, badgeThirdParty } : ApplicationData) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('uuid', uuid.toString());
    formData.append('id', id);
    formData.append('title', title);
    formData.append('desc', desc);
    formData.append('logo', logo);
    formData.append('url', url);
    formData.append('urlSource', urlSource);
    formData.append('badgeDev', badgeDev);
    formData.append('badgeNew', badgeNew);
    formData.append('badgeThirdParty', badgeThirdParty);

    try {
        let route = '';
        if (uuid == -1) {
            route = 'applications/add.php';
        } else {
            route = 'applications/update.php';
        }
        // Send the POST request
        const response = await fetch(backendUrl + route, {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(responseData.message);
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateApplication };