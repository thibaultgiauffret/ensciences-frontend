async function getApplications() {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'applications/load.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({}),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "applications": data.applications, "connected": data.connected };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message, 'connected': data.connected };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getApplications };