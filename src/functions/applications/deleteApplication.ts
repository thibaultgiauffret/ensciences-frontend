async function deleteApplication(uuid:number) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'applications/delete.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ uuid }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "message": data.message };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { deleteApplication };