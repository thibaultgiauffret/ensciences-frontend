// Send the lessons data to the backend
async function updateJson(level: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'lessons/updateJson.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ level}),
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(responseData.message);
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateJson };