async function getLessons(lessonsId: string, raw: boolean = false) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;
  

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'lessons/load.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ lessonsId, raw }),
            credentials: 'include'
        });

        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success", "lessons": data.lessons, "connected": data.connected };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message, 'connected': data.connected};
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { getLessons };