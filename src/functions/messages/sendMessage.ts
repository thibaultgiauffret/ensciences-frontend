// Send message
async function sendMessage(name:string, email:string, subject:string, subjectText:string, message:string, additionalInfo:string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('subject', subject);
    formData.append('subjectText', subjectText);
    formData.append('message', message);
    formData.append('additionalInfo', additionalInfo);

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'messages/sendMessage.php', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(responseData.message);
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { sendMessage };