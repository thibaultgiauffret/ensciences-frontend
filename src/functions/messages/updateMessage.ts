// Mark a message as read
async function markAsRead(id:string, status:boolean) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'messages/markAsRead.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id, status }),
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(responseData.message);
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

// Delete a message
async function deleteMessage(id:string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'messages/delete.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id }),
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { markAsRead, deleteMessage };