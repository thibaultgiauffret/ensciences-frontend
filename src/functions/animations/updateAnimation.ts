// Send the animation data to the backend
async function updateAnimation(uuid: number, id: string, title: string, keywords: string, image: File, urlSource: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;

    // Create FormData
    const formData = new FormData();
    formData.append('uuid', uuid.toString());
    formData.append('id', id);
    formData.append('title', title);
    formData.append('keywords', keywords);
    formData.append('image', image);
    formData.append('urlSource', urlSource);

    try {
        let route = '';
        if (uuid == -1) {
            route = 'animations/add.php';
        } else {
            route = 'animations/update.php';
        }
        // Send the POST request
        const response = await fetch(backendUrl + route, {
            method: 'POST',
            body: formData,
            credentials: 'include'
        });
        // Get the response
        const responseData = await response.json();
        if (responseData.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(responseData.message);
            return { 'status': responseData.status, 'message': responseData.message };
        }
    } catch (error) {
        console.error(error);
        return { 'status': 'error' };
    }
}

export { updateAnimation };