async function sendResetPasswordEmail(email: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_WORDPRESS_URL;
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'wp-json/bdpwr/v1/reset-password', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email }),
            credentials: 'include'
        });
        // Get the response
        const data = await response.json();
        if (data.data.status === 200) {
            return { "status": "success" };
        } else {
            console.error(data.data.message);
            return { 'message': data.data.message, "status": "error" };
        }

    } catch (error) {
        console.error('Erreur lors de la connexion:', error);
        return { 'status': 'error' };
    }
}

async function resetPassword(email: string, code: string, password: string, passwordConfirm: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'changePassword.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, code, password, passwordConfirm }),
            credentials: 'include'
        });
        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success" };
        } else {
            return { 'errors': data.errors, "status": "error" };
        }

    } catch (error) {
        console.error('Erreur lors de la connexion:', error);
        return { 'status': 'error' };
    }
}

export { resetPassword, sendResetPasswordEmail };