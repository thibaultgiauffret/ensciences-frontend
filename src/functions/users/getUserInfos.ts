// Get the user infos from the backend

export async function getUserInfos() {
    const backendUrl = import.meta.env.VITE_API_URL;
    try {
        const response = await fetch(backendUrl + 'userCheck.php', {
            method: 'GET',
            credentials: 'include'
        });
        const data = await response.json();
        if (data.status === true) {
            return { 'data': data };
        } else {
            console.error('Erreur lors de la récupération des informations de l\'utilisateur:', data.error);
            return { 'error': data.error };
        }
    } catch (error) {
        console.error('Erreur lors de la récupération des informations de l\'utilisateur:', error);
        return { 'error': "Erreur lors de la récupération des informations de l'utilisateur." };
    }
}

export default getUserInfos;
