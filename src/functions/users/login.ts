// login function sends a POST request (with credentials) to the backend to log in the user.

async function login(email: string, password: string) {
    // Get the backend URL from the environment
    const backendUrl = import.meta.env.VITE_API_URL;
    try {
        // Send the POST request
        const response = await fetch(backendUrl + 'login.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password }),
            credentials: 'include'
        });
        // Get the response
        const data = await response.json();
        if (data.status === 'success') {
            return { "status": "success" };
        } else {
            console.error(data.message);
            return { 'status': data.status, 'message': data.message };
        }

    } catch (error) {
        console.error('Erreur lors de la connexion:', error);
        return { 'status': 'error' };
    }
}

export default login;