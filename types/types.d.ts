interface Lessons {
    id: string;
    title: string;
    description: string;
    versions: Version[]
}

interface Version {
    additional_files: AdditionalFile[];
    themes: Theme[];
    version: string,
    isNew?: boolean;
    isDeleted?: boolean;
    isModified?: boolean;
    oldVersion: string;
    id: number;
    availableAdditionalFiles: AvailableAdditionalFiles[];
}

interface Theme {
    id: number;
    name: string;
    title: string;
    icon: string;
    color: string;
    chapters: Chapter[];
}

interface Chapter {
    id: number;
    name: string;
    title: string;
    files: File[];
    additional_content: string;
    sources: File;
}

interface File {
    title: string;
    color: string;
    type: string;
    versions: FileVersion[];
}

interface FileVersion {
    access_level: string;
    audience: string;
    authorized: boolean;
    date: number;
    ext: string;
    name: string;
    path: string;
    size: number;
    title: string;
}

interface AdditionalFile {
    id: number;
    section: string;
    files: AdditionalFileFiles[];
}

interface Button {
    id: number;
    title: string;
    name: string;
    icon: string;
    color: string;
    path: string;
    date: number;
    size: number;
    enabled: boolean;
}

interface AdditionalFileFiles {
    id: number;
    icon: string;
    title: string;
    description: string;
    buttons: Button[];
}

interface AvailableAdditionalFiles {
    name: string;
    path: string;
    size: number;
    date: number;
}

interface FilesManagerFiles {
    id: string;
    name: string;
    dir: string;
    path: string;
    folder: string;
    permissions: any,
    size: number;
    date: number;
    ext: string;
    type: string;
    inDB: boolean;
    password:string;
}

interface AnimationData {
    uuid: number;
    id: string;
    title: string;
    keywords: string;
    image: string;
    url: string;
    urlSource: string;
}

interface ApplicationData {
    uuid: number;
    id: string;
    title: string;
    desc: string;
    logo: string;
    url: string;
    urlSource: string;
    badgeDev: string;
    badgeNew: string;
    badgeThirdParty: string;
}

interface BlogPostData {
    date: string;
    id: number;
    link: string;
    title: string;
    content: string;
    excerpt: string;
    post_categories: any;
    author: string;
    modified?: string;
}

type UrlManagerData = {
    id: string;
    file: string;
    url: string;
  };

declare module 'react-simple-captcha';