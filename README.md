# EnSciences Frontend

This is the next generation frontend of the EnSciences website. It relies on 4 main libraries : Vite, React, Bootstrap and Font Awesome.

**🚧 DISCLAIMER :** This project is still work in progress and is not ready for production use.

## 🖥 Local testing

- Clone the deposit : `git clone https://framagit.org/ThibGiauffret/ensciences-frontend.git`
- Install the dependencies : `npm install`
- Launch the dev server : `npm run dev`

## 🚀 Dev backend server

**❗ IMPORTANT NOTE :** When testing the EnSciences Frontend, you will be prompted with many errors. This is because the dev backend server (from which the frontend gets some content) is not available to public yet...

EnSciences relies on a headless Wordpress backend with a custom api. You can partially create your own dev backend server using docker with these images :
- php7.4-apache, expose port 8000 (containing a Wordpress installation in /var/www/html/wordpress folder)
- mysql:5.7

Full dev backend server (with api files) should be published one day...

## 📦 Build

To build the project, run `npm run build`. The build artifacts will be stored in the `dist/` directory.



